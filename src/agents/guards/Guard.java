package agents.guards;

import agents.Agent;
import core.Core;
import physics.Vector;

public class Guard extends Agent{
    public Vector at;
    final double GuardNormalVisionRange = 6.0;
    final int type = 1;

    public static Comunication com;

    public boolean inTower = false;
    public final int startingRangeInTower = 2;

    public Guard(Core core){
        super(core);
        this.coordinates = setRandomInitialCoordinates();
        this.com = new Comunication(core);
        initializeGuard();
    }

    public Guard(Core core, Vector vector){
        super(core);
        this.coordinates = vector;
        initializeGuard();
    }
    
    private void initializeGuard(){
        this.setVison(GuardNormalVisionRange);
        this.visionRange.setValue(normalVisionRange);
        this.visionAngle = 45;
    }

    public void move(){
        getRandomMove();
    }

    public int getType() {
        return type;
    }

    public boolean isInTower() {
        return inTower;
    }

    public void setInTower(boolean inTower) {
        this.inTower = inTower;
    }
    
    @Override
    public void setAngle(double value){
        if(inTower)
        {
            visionAngle = 30;
            angle.set(value - 30/2.0);
        }
        else{
            visionAngle = 45;
            angle.set(value - 45/2.0);
        }
    }
}
