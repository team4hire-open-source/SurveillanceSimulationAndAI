package agents.guards;

import agents.Agent;
import agents.AgentController;
import agents.AgentInterface;
import agents.guards.Guard;
import agents.intruders.Intruder;
import core.Core;
import javafx.geometry.Point2D;
import physics.PhysicsAgents;
import physics.Point;
import physics.Vector;
import physics.Vision;
import physics.algorithms.AssertEnviromentRefractored;
import physics.algorithms.SeenTerrain;

import java.util.ArrayList;

public class ExploreCatch extends Guard implements AgentInterface {
  
  int mapSize = core.map.length;


  public ExploreCatch(Core core){
    super(core);
  }

  @Override
  public void move(){

    com.resetSmellArray();
    com.resetPathArray();

    ArrayList<Vector> hearing = PhysicsAgents.iHear(this.core, this.coordinates);
    Vision vision = PhysicsAgents.iSee(this.core, this.coordinates, this.direction, this.normalVisionRange, this.visionAngle);
    com.upDateMap(vision.getSeenTerrain());

    /*
    com.updatePathArray();

    System.out.println("vision");
    printMatrixtint(vision.getSeenTerrain());
    System.out.println("seen map");
    printMatrixtint(com.seenMap);
    ArrayList<Agent> seenIntruders = vision.getIntruders();

    //to be able to update the position of already seen intruders
    for (Agent i: seenIntruders) {

      System.out.println("i saw an intruder");

      int agentCode = i.getAgentCode();
      boolean alreadySeen = false;

      for (Agent c_i: com.seenIntruders) {
        int com_agentCode = c_i.getAgentCode();

        if(agentCode == com_agentCode){
          com.seenIntruders.remove(c_i);
          com.seenIntruders.add(i);
          alreadySeen = true;
          com.updateSmell();
        }
      }

      if(!alreadySeen){
        com.seenIntruders.add(i);
        com.updateSmell();
      }
    }*/

    com.updateSmell();

    if(com.seenIntruders.size() > 0){
      _catch(hearing , vision);
    }else{
      explore();
    }
  }


  public void explore(){

    System.out.println("exploring");

    int j = Math.toIntExact(Math.round(this.coordinates.getxPos() - this.core.graphicDeviation));
    int i = Math.toIntExact(Math.round(this.coordinates.getyPos() - this.core.graphicDeviation));

    double agentValue = com.pathArray[i][j];

      double highestValue = 0;
      int direction = 0;

      //check square abow current
      if (i - 1 >= 0 && com.pathArray[i - 1][j] > highestValue) {
        highestValue = com.pathArray[i - 1][j];
        direction = 1;
      }
      //check square to the right of current
      if (j + 1 < mapSize && com.pathArray[i][j + 1] > highestValue) {
        highestValue = com.pathArray[i][j + 1];
        direction = 2;
      }
      //check square below current
      if (i + 1 < mapSize && com.pathArray[i + 1][j] > highestValue) {
        highestValue = com.pathArray[i + 1][j];
        direction = 3;
      }
      //check square to the left of current
      if (j - 1 >= 0 && com.pathArray[i][j - 1] > highestValue) {
        highestValue = com.pathArray[i][j - 1];
        direction = 4;
      }

      System.out.println("current value: " + agentValue);
      System.out.println("highest value: " + highestValue);
      System.out.println("direction: " + direction);

      if (direction == 1) { //go up
        //System.out.println("up");
        double goaly = (i - 1) + this.core.graphicDeviation;
        double goalx = j + this.core.graphicDeviation;

        Vector goalVec = new Vector();
        goalVec.setyPos(goaly);
        goalVec.setxPos(goalx);

        Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
        Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
        setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
        setDirection(newDirection);
        setCoordinates(newCoordinates);

      } else if (direction == 2) { //go right
        //System.out.println("right");
        double goaly = i + this.core.graphicDeviation;
        double goalx = (j + 1) + this.core.graphicDeviation;

        Vector goalVec = new Vector();
        goalVec.setyPos(goaly);
        goalVec.setxPos(goalx);

        Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
        Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
        setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
        setDirection(newDirection);
        setCoordinates(newCoordinates);

      } else if (direction == 3) {  //go down
        //System.out.println(" down");
        double goaly = (i + 1) + this.core.graphicDeviation;
        double goalx = j + this.core.graphicDeviation;

        Vector goalVec = new Vector();
        goalVec.setyPos(goaly);
        goalVec.setxPos(goalx);

        Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
        Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
        setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
        setDirection(newDirection);
        setCoordinates(PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection));

      } else { //go left
        //System.out.println("left");
        double goaly = i + this.core.graphicDeviation;
        double goalx = (j - 1) + this.core.graphicDeviation;

        Vector goalVec = new Vector();
        goalVec.setyPos(goaly);
        goalVec.setxPos(goalx);

        Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
        Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
        setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
        setDirection(newDirection);
        setCoordinates(PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection));

      }
  }

  public void _catch(ArrayList<Vector> hearing, Vision vision){

    System.out.println("catching");

    //CHECK FOR THE CLOSEST INTRUDER
    double closestIntruder = Double.MAX_VALUE;
    Vector closestPos = new Vector();

    for (Agent i: com.seenIntruders) {
      double distanceToIntruder = PhysicsAgents.getDistance(this.coordinates, i.getCoordinates());
      if(distanceToIntruder < closestIntruder){
        closestIntruder = distanceToIntruder;
        closestPos = i.getCoordinates();
      }
    }

    //IF GUARD HEAR SOMEONE AND CLOSEST INTRUDER SEEN DISTANCE IS > 10 THEN GO TO INTRUDER HEARD

      int j = Math.toIntExact(Math.round(this.coordinates.getxPos() - this.core.graphicDeviation));
      int i = Math.toIntExact(Math.round(this.coordinates.getyPos() - this.core.graphicDeviation));

      double agentValue = com.smellArray[i][j];

      if(agentValue == 100){

        Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, closestPos);
        setCoordinates(PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection));

      }else {

        double highestValue = 0;
        int direction = 0;

        //check square abow current
        if (i - 1 >= 0 && com.smellArray[i - 1][j] > highestValue) {
          highestValue = com.smellArray[i - 1][j];
          direction = 1;
        }
        //check square to the right of current
        if (j + 1 < mapSize && com.smellArray[i][j + 1] > highestValue) {
          highestValue = com.smellArray[i][j + 1];
          direction = 2;
        }
        //check square below current
        if (i + 1 < mapSize && com.smellArray[i + 1][j] > highestValue) {
          highestValue = com.smellArray[i + 1][j];
          direction = 3;
        }
        //check square to the left of current
        if (j - 1 >= 0 && com.smellArray[i][j - 1] > highestValue) {
          highestValue = com.smellArray[i][j - 1];
          direction = 4;
        }


        if (direction == 1) { //go up
          //System.out.println("up");
          double goaly = (i - 1) + this.core.graphicDeviation;
          double goalx = j + this.core.graphicDeviation;

          Vector goalVec = new Vector();
          goalVec.setyPos(goaly);
          goalVec.setxPos(goalx);

          Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
          Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
          setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
          setDirection(newDirection);
          setCoordinates(newCoordinates);

        } else if (direction == 2) { //go right
          //System.out.println("right");
          double goaly = i + this.core.graphicDeviation;
          double goalx = (j + 1) + this.core.graphicDeviation;

          Vector goalVec = new Vector();
          goalVec.setyPos(goaly);
          goalVec.setxPos(goalx);

          Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
          Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
          setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
          setDirection(newDirection);
          setCoordinates(newCoordinates);

        } else if (direction == 3) {  //go down
          //System.out.println(" down");
          double goaly = (i + 1) + this.core.graphicDeviation;
          double goalx = j + this.core.graphicDeviation;

          Vector goalVec = new Vector();
          goalVec.setyPos(goaly);
          goalVec.setxPos(goalx);

          Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
          Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
          setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
          setDirection(newDirection);
          setCoordinates(PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection));

        } else { //go left
          //System.out.println("left");
          double goaly = i + this.core.graphicDeviation;
          double goalx = (j - 1) + this.core.graphicDeviation;

          Vector goalVec = new Vector();
          goalVec.setyPos(goaly);
          goalVec.setxPos(goalx);

          Vector newDirection = PhysicsAgents.getDirectionVec(this.coordinates, goalVec);
          Vector newCoordinates = PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection);
          setAngle(180-Math.toDegrees(Math.atan2(this.coordinates.getyPos()-newCoordinates.getyPos(), this.coordinates.getxPos()-newCoordinates.getxPos())));
          setDirection(newDirection);
          setCoordinates(PhysicsAgents.move(this.core, this.radians, this.coordinates, this.velocity, newDirection));

        }
      }
    }


  public void printMatrixt(double[][] matrix){
    for (int i = 0; i < matrix.length ; i++) {
      for (int j = 0; j < matrix.length ; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
    System.out.println();
    System.out.println();
  }

  public void printMatrixtint(int[][] matrix){
    for (int i = 0; i < matrix.length ; i++) {
      for (int j = 0; j < matrix.length ; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
    System.out.println();
    System.out.println();
  }
}

