package agents.guards;

import agents.Agent;
import agents.intruders.Intruder;
import core.Core;

import java.util.ArrayList;

public class Comunication {
  public double[][] smellArray;
  public ArrayList<Intruder> seenIntruders;
  public int[][] seenMap;
  public double[][] pathArray;
  public ArrayList<Guard> guards;
  public double graphicDeviation;
  public int mapSize;
  boolean smellHasBeenReseted = false;
  int squaresToUPdate;

  public Comunication(Core core){
    this.mapSize = core.map.length;
    this.guards = core.guards;
    this.graphicDeviation = core.graphicDeviation;
    this.smellArray = new double[mapSize][mapSize];
    this.squaresToUPdate = mapSize * mapSize;
    this.pathArray = new double[mapSize][mapSize];
    this.seenIntruders = new ArrayList<>();
    //for testing:
    this.seenIntruders = core.intruders;


    this.seenMap = new int[mapSize][mapSize];

    for (int i = 0; i < seenMap.length ; i++) {
      for (int j = 0; j < seenMap.length ; j++) {
        seenMap[i][j] = -1;
      }
    }

   resetPathArray();

    //for testing:
    seenMap = core.map;

    resetSmellArray();
  }

  public void upDateMap(int[][] mapVision){

    for (int i = 0; i < seenMap.length ; i++) {
      for (int j = 0; j < seenMap.length ; j++) {
        if(seenMap[i][j] == -1 && mapVision[i][j] != -2){
          seenMap[i][j] = mapVision[i][j];
        }
      }
    }


    for (int i = 0; i < seenMap.length ; i++) {
      for (int j = 0; j < seenMap.length ; j++) {
        if(seenMap[i][j] == 10){
          smellArray[i][j] = -1;
        }
      }
    }

    for (int i = 0; i < pathArray.length ; i++) {
      for (int j = 0; j < pathArray.length ; j++) {
        if(seenMap[i][j] == 10){
          pathArray[i][j] = -1;
        }else if(seenMap[i][j] == -1){
          pathArray[i][j] = 100;
        }else{
          pathArray[i][j] = 0.0;
        }
      }
    }
  }

  public void updateSmell(){

    this.smellHasBeenReseted = false;

    for (Agent intruder: this.seenIntruders) {
      int x = Math.toIntExact(Math.round(intruder.getCoordinates().getxPos() - this.graphicDeviation));
      int y = Math.toIntExact(Math.round(intruder.getCoordinates().getyPos() - this.graphicDeviation));

      this.smellArray[y][x] = 100;
      double currentValue = 100;

      //adding smeel all over the map
      while(checkSmellNotFullyUpdated()) {

        double newValue = currentValue * 0.9;
        this.squaresToUPdate--;

        for (int i = 0; i < this.smellArray.length; i++) {
          for (int j = 0; j < this.smellArray.length; j++) {

            if (smellArray[i][j] == currentValue) {

              //update square below current
              if ( i+1 < mapSize && smellArray[i+1][j] < newValue && smellArray[i+1][j] != -1){
                smellArray[i+1][j] = newValue;
              }
              //update square abow current
              if ( i-1 >= 0 && smellArray[i-1][j] < newValue && smellArray[i-1][j] != -1){
                smellArray[i-1][j] = newValue;
              }
              //update square to the right of current
              if ( j+1 < mapSize && smellArray[i][j+1] < newValue && smellArray[i][j+1] != -1){
                smellArray[i][j+1] = newValue;
              }
              //update square to the left of current
              if ( j-1 >= 0 && smellArray[i][j-1] < newValue && smellArray[i][j-1] != -1){
                smellArray[i][j-1] = newValue;
              }
            }
          }
        }
        currentValue = newValue;
      }
      this.squaresToUPdate = mapSize * mapSize;
    }

    //decreasing smell next to guards
    for (Guard g: this.guards) {
      int j = Math.toIntExact(Math.round(g.getCoordinates().getxPos() - this.graphicDeviation));
      int i = Math.toIntExact(Math.round(g.getCoordinates().getyPos() - this.graphicDeviation));

      //update square below current
      if ( i+1 < mapSize && smellArray[i+1][j] != -1 && smellArray[i+1][j] != 100){
        smellArray[i+1][j] = smellArray[i+1][j] * 0.9;
      }
      //update square abow current
      if ( i-1 < mapSize && smellArray[i-1][j] != -1 &&  smellArray[i-1][j] != 100){
        smellArray[i-1][j] = smellArray[i-1][j];
      }
      //update square to the right of current
      if ( j+1 < mapSize && smellArray[i][j+1] != -1 && smellArray[i][j+1] != 100){
        smellArray[i][j+1] =  smellArray[i][j+1] * 0.9;
      }
      //update square to the left of current
      if ( j-1 < mapSize && smellArray[i][j-1] != -1 && smellArray[i][j-1] != 100){
        smellArray[i][j-1] = smellArray[i][j-1] * 0.9;
      }
    }
  }


  public void updatePathArray(){

    double currentValue = 100;

    while(checkPathNotFullyUpdated()) {
      double newValue = currentValue * 0.9;

      for (int i = 0; i < this.pathArray.length; i++) {
        for (int j = 0; j < this.pathArray.length; j++) {

          if (pathArray[i][j] == currentValue) {

            //update square below current
            if (i + 1 < mapSize && pathArray[i + 1][j] < newValue && pathArray[i + 1][j] != -1) {
              pathArray[i + 1][j] = newValue;
            }
            //update square abow current
            if (i - 1 >= 0 && pathArray[i - 1][j] < newValue && pathArray[i - 1][j] != -1) {
              pathArray[i - 1][j] = newValue;
            }
            //update square to the right of current
            if (j + 1 < mapSize && pathArray[i][j + 1] < newValue && pathArray[i][j + 1] != -1) {
              pathArray[i][j + 1] = newValue;
            }
            //update square to the left of current
            if (j - 1 >= 0 && pathArray[i][j - 1] < newValue && pathArray[i][j - 1] != -1) {
              pathArray[i][j - 1] = newValue;
            }
          }
        }
      }
      currentValue = newValue;
    }
  }


  public boolean checkPathNotFullyUpdated(){
    for (int i = 0; i < pathArray.length ; i++) {
      for (int j = 0; j < pathArray.length ; j++) {
        if(pathArray[i][j] == 0.0){
          return true;
        }
      }
    }
    return false;
  }

  public boolean checkSmellNotFullyUpdated(){

    if(squaresToUPdate == 0){
      return false;
    }
    return true;
  }


  public void resetSmellArray(){
    for (int i = 0; i < this.smellArray.length ; i++) {
      for (int j = 0; j < this.smellArray.length ; j++) {
        this.smellArray[i][j] = 0;
      }
    }
  }

  public void printMatrixt(double[][] matrix){
    for (int i = 0; i < matrix.length ; i++) {
      for (int j = 0; j < matrix.length ; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
    System.out.println();
    System.out.println();
  }


  public void resetPathArray(){
    for (int i = 0; i < pathArray.length ; i++) {
      for (int j = 0; j < pathArray.length; j++) {
        pathArray[i][j] = 100;
      }
    }
  }

  public void printMatrixtint(int[][] matrix){
    for (int i = 0; i < matrix.length ; i++) {
      for (int j = 0; j < matrix.length ; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
    System.out.println();
    System.out.println();
  }
}
