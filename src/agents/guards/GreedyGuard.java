package agents.guards;

import agents.Agent;
import agents.AgentInterface;
import core.Core;
import physics.PhysicsAgents;
import physics.Vector;
import physics.Vision;
import physics.algorithms.SeenTerrain;

import java.util.ArrayList;


public class GreedyGuard extends Guard implements AgentInterface {

    SeenTerrain floodFill = new SeenTerrain();

    public GreedyGuard(Core core){
        super(core);
        this.direction = new Vector(Math.random(), Math.random());
    }
    
    public GreedyGuard(Core core, Vector coordinates){
        super(core, coordinates);
        this.direction = new Vector(Math.random(), Math.random());
    }

    public void move(){
        Vector temp = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
        Vision vision = PhysicsAgents.iSee(core, coordinates, direction, visionRange.doubleValue(), angle.doubleValue());
        ArrayList<Agent> iSee = vision.getGuards();
        ArrayList<Vector> iHear = PhysicsAgents.iHear(core, coordinates);
        int[][] seenTerrain = floodFill.getSeenMap(core, (int)coordinates.getxPos(), (int)coordinates.getyPos(), angle.getValue(), visionRange.getValue(), normalVisionRange);
        boolean sawWall = false;
        if (seenTerrain != null) {
            int x = (int) coordinates.getxPos();
            int y = (int) coordinates.getyPos();
            for (int i = x -1; i < x+1;i++) {
                for (int j = y-1; j < y +1; j++ ){
                    int piece = seenTerrain[i][j];
                    if (piece == 10 && !sawWall) {
                        Vector tempDirection = new Vector();
                        if(Math.random() < 0.33) {
                            tempDirection.setxPos(i - x - Math.random());
                            tempDirection.setyPos(j - y - Math.random());
                        }
                        else{
                            if(Math.random() < 0.5) {
                                tempDirection.setxPos(i - x + Math.random());
                                tempDirection.setyPos(j - y - Math.random());
                            }
                            else{
                                tempDirection.setxPos(i - x - Math.random());
                                tempDirection.setyPos(j - y + Math.random());
                            }
                        }
                        temp = PhysicsAgents.move(core, radius, coordinates, velocity, tempDirection);
                        angle.set(180 - Math.toDegrees(Math.atan2(coordinates.getyPos() - temp.getyPos(), coordinates.getxPos() - temp.getxPos())));
                        sawWall = true;

                    }
                }
            }
        }
        if (sawWall) {

        }
        else if(iSee != null && iSee.size() >= 1){
            int closest = 0;
            double minDistance = Double.MAX_VALUE;
            for(int i = 0; i < iSee.size(); i++){
                double tmp = PhysicsAgents.getDistance(coordinates, iSee.get(i).coordinates);
                if(tmp < minDistance && tmp != 0) {
                    closest = i;
                    minDistance = tmp;
                }
            }
            temp = PhysicsAgents.move(core, radius, coordinates, velocity, new Vector((iSee.get(closest).coordinates.getxPos()-coordinates.getxPos()),(iSee.get(closest).coordinates.getyPos()-coordinates.getyPos())));
            angle.set(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-temp.getyPos(), coordinates.getxPos()-temp.getxPos())));
        }
        else if(iHear != null && iHear.size() >= 1){
            temp = PhysicsAgents.move(core, radius, coordinates, velocity, new Vector(iHear.get(0).getxPos(),iHear.get(0).getyPos()));
            angle.set(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-temp.getyPos(), coordinates.getxPos()-temp.getxPos())));

        }
        else if(coordinates.getxPos() == temp.getxPos() && coordinates.getyPos() == temp.getyPos()){
            getRandomMove();
        }
        else{
            getRandomMove();
        }
        coordinates.setxPos(temp.getxPos());
        coordinates.setyPos(temp.getyPos());
        direction.setxPos(coordinates.getxPos());
        direction.setyPos(coordinates.getyPos());
    }

}
