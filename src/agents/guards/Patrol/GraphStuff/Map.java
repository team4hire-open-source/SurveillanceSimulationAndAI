package agents.guards.Patrol.GraphStuff;

import core.Core;

import java.util.ArrayList;

public class Map {
    private int width, length;
    public Node[][] nodes;
    private static Core core;

    /*
    Constructor
     */
    public Map(int width, int length, Core core) {
        this.width = width;
        this.length = length;
        nodes = new Node[width][length];
        this.core = core;
        initEmptyNodes();
        setNodes();
        for(int i = 0; i < width; i++){
            for(int j =0 ; j < length; j++){
                addAdjacentNodes(nodes[i][j]);
            }
        }
    }

    /*
    Method that initializes the array of null nodes
     */

    public void initEmptyNodes() {
        for(int i =0; i < width; i++) {
            for(int j = 0; j < length; j++) {
                nodes[i][j] = new Node(core.map[i][j]);
                nodes[i][j].setxCoordinate(i);
                nodes[i][j].setyCoordinate(j);
            }
        }
    }

    public void setNodes() {
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < length; j++) {
                //Terrain ID numbers
                //0-Road
                //1-Floor
                //2-Low Visibility
                //10-Wall
                //11-Door
                //12-Window
                //13-Sentry Tower
                //20-Entry Point
                //21-Target Area
                switch(nodes[i][j].getNodeType()) {
                    case 0:
                    case 1:
                    case 2:
                    case 20:
                    case 21:
                    //case 11
                    //case 12
                    //case 13
                        nodes[i][j].setWalkable(true);
                        break;
                    case 10:
                        nodes[i][j].setWalkable(false);
                        break;
                    default:
                        nodes[i][j].setNodeIdleness(-1000);
                        nodes[i][j].setWalkable(false);
                        break;
                }

                nodes[i][j].setgCost(Integer.MAX_VALUE);

            }
        }
    }

    public void addAdjacentNodes(Node s){
        for(int ii = s.getxCoordinate()-1; ii <= s.getxCoordinate()+1; ii++) {
            for(int jj = s.getyCoordinate()-1; jj <= s.getyCoordinate()+1; jj++) {
                if(!(ii == s.getxCoordinate() && jj == s.getyCoordinate())){
                    if(withinGrid(ii,jj)) {
                        if(nodes[ii][jj].isWalkable()) {s.addAdjacentNodes(nodes[ii][jj]);
                        }
                    }
                }
            }
        }
    }

    /*
    Method that checks if a node is not out of bounds
     */
    public boolean withinGrid(int i, int j) {
        if((i < 0) || (j <0) ) {
            return false;    //false if negative out of bounds
        }
        if((i >= nodes.length) || (j >= nodes[0].length)) {
            return false;    //false if positive out of bounds
        }
        return true;
    }

    public void clearNodes()
    {
        for(int i=0;i<nodes.length;i++){
            for(int j=0;j<nodes[0].length;j++){
                nodes[i][j].setPrevious(nodes[i][j]);
                nodes[i][j].setgCost(0);
                nodes[i][j].setTotalCost();
            }
        }
    }
}
