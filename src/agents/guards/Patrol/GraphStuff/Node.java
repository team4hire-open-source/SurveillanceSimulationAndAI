package agents.guards.Patrol.GraphStuff;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private int xCoordinate, yCoordinate;
    private int nodeType;
    private boolean isWalkable;

    private List<Node> adjacentNodes = new ArrayList<>();
    private Node previous;
    private int nodeWeight;

    private double gCost = 0, hCost = 0, totalCost = 0;
    private int nodeIdleness = 0;
    private boolean isTargeted = false;

    public Node(int nodeType) {
        this.nodeType = nodeType;
    }

    public Node() { }


    public boolean isWalkable() {
        return this.isWalkable;
    }

    public void setWalkable(boolean walkable) {
        this.isWalkable = walkable;
    }

    public void addAdjacentNodes(Node adjacent)
    {
        this.adjacentNodes.add(adjacent);
    }

    public List<Node> getAdjacentNodes() { return this.adjacentNodes; }

    public void setNodeType(int nodeType) { this.nodeType =  nodeType;}

    public int getNodeType() { return this.nodeType;}

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }

    public double getgCost() {
        return gCost;
    }

    public void setgCost(double gCost) {
        this.gCost = gCost;
    }

    public double gethCost() {
        return hCost;
    }

    public void sethCost(double hCost) {
        this.hCost = hCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost() {
        this.totalCost = this.getgCost() + this.gethCost();
    }

    public void calculateHCostEuclidean(Node goal){
        this.sethCost(2*(double)Math.sqrt(Math.pow(this.getxCoordinate()-goal.getxCoordinate(),2)+Math.pow(this.getyCoordinate()-goal.getyCoordinate(),2)));
    }

    public void calculateHCostChebyshev(Node goal){
        this.sethCost(Math.max(Math.abs(this.getxCoordinate()-goal.getxCoordinate()),
                               Math.abs(this.getyCoordinate()-goal.getyCoordinate())));
    }


    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getNodeIdleness() {
        return nodeIdleness;
    }

    public void setNodeIdleness(int nodeIdleness) {
        this.nodeIdleness = nodeIdleness;
    }

    public void incrementNodeIdleness() {
        if(this.isWalkable){
            this.nodeIdleness = this.nodeIdleness + 1;
        }
    }

    public boolean isTargeted() {
        return isTargeted;
    }

    public void setTargeted(boolean targeted) {
        isTargeted = targeted;
    }

    public int getNodeWeight() {
        return nodeWeight;
    }

    public void setNodeWeight(int nodeWeight) {
        this.nodeWeight = nodeWeight;
    }
}
