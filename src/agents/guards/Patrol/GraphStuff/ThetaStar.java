package agents.guards.Patrol.GraphStuff;

import java.util.ArrayList;
import java.util.List;

public class ThetaStar {
    private List<Node> openList;
    private List<Node> closedList;
    ArrayList<Node> path;
    private Map mapper;
    private double gOld = 0;

    public ThetaStar(Map m) {
        this.mapper = m;
    }

    public List<Node> findPath(Node start, Node goal){
        openList = new ArrayList<>();
        closedList = new ArrayList<>();

        Node current;
        current = start;
        current.setPrevious(current);
        current.setgCost(0);
        openList.add(current);

        while(!openList.isEmpty()) {
            current = cheapestFInOpen();

            if(current.getxCoordinate() == goal.getxCoordinate() &&
               current.getyCoordinate() == goal.getyCoordinate()){
                return calcPath(start,current);
            }

            openList.remove(current);
            closedList.add(current);

            List<Node> currentAdjacent = current.getAdjacentNodes();
            for(Node s: currentAdjacent){
                if (!closedList.contains(s)) {
                    if(!openList.contains(s)){
                        s.setgCost(Integer.MAX_VALUE);
                        s.setPrevious(null);
                    }
                    updateVertex(current, s, goal);
                }
            }
        }
        return null;
    }

    private Node cheapestFInOpen() {
        Node cheapest = openList.get(0);
        for(int i = 0; i < openList.size(); i++) {
            if(openList.get(i).getTotalCost() < cheapest.getTotalCost()){
                cheapest = openList.get(i);
            }
        }
        return  cheapest;
    }

    private List<Node> calcPath(Node start, Node goal){
        path = new ArrayList<>();
        Node curr = goal;
        boolean done = false;
        while(!done){
            path.add(0,curr);
            curr = curr.getPrevious();
            if(curr.getxCoordinate() == start.getxCoordinate() &&
               curr.getyCoordinate() == start.getyCoordinate()) done = true;
        }
        return path;
    }

    private void updateVertex(Node s, Node s1, Node goal){
        gOld = s1.getgCost();
        computeCost(s,s1);
        if(s1.getgCost() < gOld){
            if(openList.contains(s1)){
                openList.remove(s1);
            }
            s1.calculateHCostChebyshev(goal);
            s1.setTotalCost();
            openList.add(s1);
        }
    }

    private void computeCost(Node s, Node s1){
        if(lineOfSight(s.getPrevious(), s1)){
            double temp = s.getPrevious().getgCost()+calculateChebyshev(s.getPrevious(),s1);
            if(temp < s1.getgCost()){
                s1.setPrevious(s.getPrevious());
                s1.setgCost(temp);
            }
        }
        else {
            double temp = s.getgCost()+calculateChebyshev(s,s1);
            if(temp < s1.getgCost()){
                s1.setPrevious(s);
                s1.setgCost(temp);
            }
        }
    }

    private boolean lineOfSight(Node s, Node s1){
        if(s.isWalkable() == false || s1.isWalkable() == false){return false;}
        int dx, dy;
        int sx, sy;
        int f = 0;
        int x1 =  s.getxCoordinate(),
            y1 =  s.getyCoordinate();
        int x2 =  s1.getxCoordinate(),
            y2 =  s1.getyCoordinate();
        dx = x2 - x1;
        dy = y2 - y1;
        if(dx < 0){
            dx = -dx;
            sx = -1;
        }
        else sx = 1;

        if(dy < 0){
            dy = -dy;
            sy = -1;
        }
        else sy = 1;

        if(dx > dy){
            while(x1 != x2){
                f += dy;
                if(f >= dx){
                    if(!mapper.nodes[x1+((sx-1)/2)][y1+((sy-1)/2)].isWalkable())return false;
                    y1 += sy;
                    f -= dx;
                }
                if(f!=0 && !mapper.nodes[x1+((sx-1)/2)][y1+((sy-1)/2)].isWalkable())return false;
                if(dy==0 && !mapper.nodes[x1+((sx-1)/2)][y1].isWalkable() && !mapper.nodes[x1+((sx-1)/2)][y1-1].isWalkable()) return false;
                x1 += sx;
            }
        }
        else {
            while(y1 != y2){
                f += dx;
                if(f >= dy){
                    if(!mapper.nodes[x1+((sx-1)/2)][y1+((sy-1)/2)].isWalkable())return false;
                    x1 += sx;
                    f -= dy;
                }
                if(f != 0 && !mapper.nodes[x1+((sx-1)/2)][y1+((sy-1)/2)].isWalkable())return false;
                if(dx == 0 && !mapper.nodes[x1][y1+((sy-1)/2)].isWalkable() && !mapper.nodes[x1-1][y1+((sy-1)/2)].isWalkable()) return false;
                y1 += sy;
            }
        }
        return true;
    }

    // Calculate distance between 2 nodes using the Euclidean distance
    // sqrt[(x1-x2)^2 + (y1-y2)^2]
    private int calculateStraight(Node s, Node s1){
        return (int)Math.sqrt(Math.pow(s1.getxCoordinate() - s.getxCoordinate() , 2) + Math.pow(s1.getyCoordinate() - s.getyCoordinate() , 2));
    }

    // Calculate distance between 2 nodes using the Chebyshev distance
    // max(|x1-x2|,|y1-y2|)
    private int calculateChebyshev(Node s, Node s1){
        return Math.max(Math.abs(s.getxCoordinate() - s1.getxCoordinate()),Math.abs(s.getyCoordinate()-s1.getyCoordinate()));
    }

}
