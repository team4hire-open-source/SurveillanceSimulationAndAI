package agents.guards.Patrol;

import agents.AgentInterface;
import agents.guards.Guard;
import core.Core;
import physics.PhysicsAgents;
import physics.Vector;
import java.util.Random;

public class PatrollingGuard extends Guard implements AgentInterface {
    private Random rand = new Random();
    public PatrollingGuard(Core core){
        super(core);
        this.direction = new Vector(rand.nextInt(), rand.nextInt());
    }

    public PatrollingGuard(Core core, Vector coordinates){
        super(core, coordinates);
        this.direction = new Vector(Math.random(), Math.random());
    }

    public void move() {

        Vector direction = PhysicsAgents.getDirectionVec(coordinates, this.at);
        setDirection(direction);
        Vector newPositionn = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
        if(newPositionn.getxPos() == coordinates.getxPos() &&
           newPositionn.getyPos() == coordinates.getyPos()){
            direction = PhysicsAgents.getDirectionVec(coordinates, new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation));
            newPositionn = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
            setDirection(direction);
        }
        setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newPositionn.getyPos(), coordinates.getxPos()-newPositionn.getxPos())));
        this.coordinates.setxPos(newPositionn.getxPos());
        this.coordinates.setyPos(newPositionn.getyPos());
    }
}
