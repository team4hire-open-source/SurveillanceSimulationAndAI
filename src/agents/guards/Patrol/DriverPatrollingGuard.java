package agents.guards.Patrol;

import agents.Agent;
import agents.guards.Guard;
import agents.guards.Patrol.GraphStuff.Map;
import agents.guards.Patrol.GraphStuff.Node;
import agents.guards.Patrol.GraphStuff.ThetaStar;
import core.Core;
import physics.Vector;

import java.util.ArrayList;
import java.util.List;

public class DriverPatrollingGuard {
    private Map mapper;
    private ThetaStar pathFinder;
    private Core core;
    private List<List<Node>> pathsOfGuards;
    private List<Node> tempPath;
    private ArrayList<Agent> seenGuards = new ArrayList<>();
    private ArrayList<Agent> seenIntruders = new ArrayList<>();
    private ArrayList<Vector> heardNoises = new ArrayList<>();

    private final int SEEN_GUARD_WEIGHT = 10;
    private final int SEEN_INTRUDER_WEIGHT = 15;
    private final int HEARD_NOISE_WEIGHT = 15;

    // Look up radius around the guard based on max idleness
    private final int IDLENESS_RANGE = 5;
    // Radius around the guard that the idleness is reset
        // Improvement includes changing from radius to cone, matching the guards' visibility cone
    private final int IDLENESS_RESET_RANGE = 3;

    public DriverPatrollingGuard(Core core) {
        this.core = core;
        mapper = new Map(core.map.length, core.map[0].length, core);
        pathFinder = new ThetaStar(mapper);
        pathsOfGuards = new ArrayList<>();
        System.out.println("Number of guards " + core.guardCount);
        for (int i = 0; i < core.guardCount; i++) {
            pathsOfGuards.add(new ArrayList<>());
        }
    }

    private void changeIdleness() {
        // Increase all nodes idleness
        for (int i = 0; i < mapper.nodes.length; i++) {
            for (int j = 0; j < mapper.nodes[0].length; j++) {
                mapper.nodes[i][j].incrementNodeIdleness();
            }
        }
        // Reset idleness of nodes around guards
        for (Guard g : core.guards) {
            int currentGuardX = (int) g.getCoordinates().getxPos(),
                currentGuardY = (int) g.getCoordinates().getyPos();

            for (int ii = currentGuardX - 3; ii <= currentGuardX + 3; ii++) {
                for (int jj = currentGuardY - 3; jj <= currentGuardY + 3; jj++) {
                        if (mapper.withinGrid(ii, jj) &&
                            mapper.nodes[ii][jj].isWalkable()) {
                                mapper.nodes[ii][jj].setNodeIdleness(0);
                        }
                }
            }

        }
    }

    // Driver method
    public void performMoves() {

        changeIdleness();

        // Go through guards and assign goals
        for (int i = 0; i < core.guardCount; i++) {
            Guard currentGuard = core.guards.get(i);
            Node currentGuardPos = mapper.nodes[(int)Math.round(currentGuard.getCoordinates().getxPos())][(int) Math.round(currentGuard.getCoordinates().getyPos())];

            // Update node weights based on seen guards and seen intruders
            // Issue with this is the fact that the weights have to be reset sosmewhere
//            Vision currentGVision = PhysicsAgents.iSee(core, currentGuard.coordinates, currentGuard.direction, currentGuard.visionRange.getValue(), currentGuard.angle.getValue());
//            seenGuards.clear();
//            seenIntruders.clear();
//            heardNoises.clear();
//            seenGuards = currentGVision.getGuards();
//            for(Agent gg: seenGuards){
//                Node seenGuardNode = mapper.nodes[(int)gg.getCoordinates().getxPos()][(int)gg.getCoordinates().getyPos()];
//                seenGuardNode.setNodeWeight(seenGuardNode.getNodeWeight() - SEEN_GUARD_WEIGHT);
//            }
//            for(Agent gg: seenIntruders){
//                Node seenIntruderNode = mapper.nodes[(int)gg.getCoordinates().getxPos()][(int)gg.getCoordinates().getyPos()];
//                seenIntruderNode.setNodeWeight(seenIntruderNode.getNodeWeight() + SEEN_INTRUDER_WEIGHT);
//            }
//
//            seenIntruders = currentGVision.getIntruders();
//            heardNoises = PhysicsAgents.iHear(core, currentGuard.coordinates);
//
            // Check if guard has finished task by looking at the size of the path it has to take
            if(pathsOfGuards.get(i).size() == 0) {
                    currentGuard.setHasFinishedTask(true);
            }

            // If guard has finished task assign a new one
            if (currentGuard.isHasFinishedTask()) {
                currentGuard.setHasFinishedTask(false);
                pathsOfGuards.get(i).clear();

                // Get new target based on the maximum idleness around the guard
                Node newTarget;
                int maxIdle = Integer.MIN_VALUE;
                for(int a = currentGuardPos.getxCoordinate() - IDLENESS_RANGE; a <= currentGuardPos.getxCoordinate() + IDLENESS_RANGE; a++){
                    for(int b = currentGuardPos.getyCoordinate() - IDLENESS_RANGE; b <= currentGuardPos.getyCoordinate() + IDLENESS_RANGE; b++){
                        if(mapper.withinGrid(a,b)){
                            if(maxIdle <= mapper.nodes[a][b].getNodeIdleness() &&
                               mapper.nodes[a][b].isWalkable()) {
                                    tempPath =  new ArrayList<>();
                                    tempPath = pathFinder.findPath(currentGuardPos, mapper.nodes[a][b]);
                                    if(tempPath != null){
                                        newTarget = mapper.nodes[a][b];
                                        maxIdle = newTarget.getNodeIdleness();
                                        pathsOfGuards.get(i).clear();
                                        pathsOfGuards.get(i).addAll(tempPath);
                                }
                            }
                        }
                    }
                }


                mapper.clearNodes();
                Node nextNode = pathsOfGuards.get(i).get(0);
                pathsOfGuards.get(i).remove(0);
                currentGuard.at = new Vector(nextNode.getxCoordinate(), nextNode.getyCoordinate());
            }

            // Guard has not finished task
            else {
                Node atNode = pathsOfGuards.get(i).get(0);
                for(Node g: pathsOfGuards.get(i)){
                    if(g.getNodeIdleness() < 10) currentGuard.setHasFinishedTask(true);
                }

                // Remove node only if it has been reached by the guard
                if(currentGuard.isHasFinishedTask() == false &&
                   currentGuardPos.getxCoordinate() == atNode.getxCoordinate() &&
                   currentGuardPos.getyCoordinate() == atNode.getyCoordinate()){
                        pathsOfGuards.get(i).remove(0);
                        if(pathsOfGuards.get(i).size() == 0){
                            currentGuard.setHasFinishedTask(true);
                        }
                        else {
                            atNode = pathsOfGuards.get(i).get(0);
                        }
                        currentGuard.at = new Vector(atNode.getxCoordinate(), atNode.getyCoordinate());
                }
            }
        }
    }


}

