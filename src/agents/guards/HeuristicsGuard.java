package agents.guards;

import agents.Agent;
import agents.AgentInterface;
import physics.Vector;
import core.Core;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import physics.algorithms.AStar;
import physics.Vision;
import physics.PhysicsAgents;
import physics.PhysicsWorld;
import physics.algorithms.SeenTerrain;

public class HeuristicsGuard extends Guard implements AgentInterface {
    private int[][] exploredWorld; //-1 visited //-2 unknown
    private int[] target;
    private ArrayList<Agent> seenGuards = new ArrayList<>();
    private ArrayList<Agent> seenIntruders = new ArrayList<>();
    private ArrayList<Vector> heardNoises = new ArrayList<>();
    
    public HeuristicsGuard(Core core){
        super(core);
        this.direction = new Vector(Math.random(), Math.random());
        setAngle(0);
        setInitialExploredWorld();
    }
    
    public void move() {
        updateExploredWorld();
    }
    
    private void updateExploredWorld(){
        Vision v = PhysicsAgents.iSee(core, coordinates, direction, visionRange.getValue(), angle.getValue());
        seenIntruders = v.getGuards();
        seenGuards = v.getIntruders();
        heardNoises = PhysicsAgents.iHear(core, coordinates);
        SeenTerrain f = new SeenTerrain();
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        int[][] seenMap = f.getSeenMap(core, x, y, angle.getValue(), visionAngle, visionRange.doubleValue());
        seenMap[x][y] = core.map[x][y];
        addNewFoundStructures(seenMap);
        newGoal();
    }
    
    private void newGoal(){
        if(!seenIntruders.isEmpty()){
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-seenIntruders.get(0).coordinates.getyPos(), coordinates.getxPos()-seenIntruders.get(0).coordinates.getxPos())));          
            direction = PhysicsAgents.getDirectionVec(coordinates, seenIntruders.get(0).coordinates);
            target = new int[2];
            target[0] = Math.toIntExact(Math.round(seenIntruders.get(0).coordinates.getyPos() - core.graphicDeviation));
            target[1] = Math.toIntExact(Math.round(seenIntruders.get(0).coordinates.getxPos() - core.graphicDeviation));
            generatePathToGoal();
        }
        else if(!heardNoises.isEmpty()){
            Vector noiseSource = new Vector(heardNoises.get(0).getyPos() + coordinates.getxPos(), heardNoises.get(0).getxPos() + coordinates.getyPos());
            //System.out.println("STH:" + coordinates.getxPos() + " " + coordinates.getyPos());
            //System.out.println(noiseSource.getxPos() + " " + noiseSource.getyPos());
            if(!isInRange(Math.toIntExact(Math.round(noiseSource.getyPos())), Math.toIntExact(Math.round(noiseSource.getxPos()))) && heardNoises.size() > 1){
                noiseSource = new Vector(heardNoises.get(1).getyPos() + coordinates.getxPos(), heardNoises.get(1).getxPos() + coordinates.getyPos());
                //System.out.println("Try2: " + noiseSource.getxPos() + " " + noiseSource.getyPos());
                setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-noiseSource.getyPos(), coordinates.getxPos()-noiseSource.getxPos())));          
                direction = PhysicsAgents.getDirectionVec(coordinates, noiseSource);
                target = new int[2];
                target[0] = Math.toIntExact(Math.round(noiseSource.getyPos() - core.graphicDeviation));
                target[1] = Math.toIntExact(Math.round(noiseSource.getxPos() - core.graphicDeviation));
                generatePathToGoal();
            }
        }
        else{
            generatePathToGoal();
        }
    }
    
    private void addNewFoundStructures(int[][] seenMap){
        for(int i = 0; i < seenMap.length; i++){
            for(int j = 0; j < seenMap[0].length; j++){
                if(seenMap[i][j] != -1 && seenMap[i][j] != -2){
                    exploredWorld[i][j] = seenMap[i][j];
                }
            }
        }
        //System.out.println("new");
        /*for(int i = 0; i < seenMap.length; i++){
            System.out.println();
            for(int j = 0; j < seenMap[0].length; j++){
                System.out.print(exploredWorld[i][j] + " ");
            }
        }*/
    }
    
    private void generatePathToGoal(){
        AStar a;
        if(target!=null){
            /*a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), 
                    Math.toIntExact(Math.round(goalSearchingTarget[1] - core.graphicDeviation)), 
                    Math.toIntExact(Math.round(goalSearchingTarget[0] - core.graphicDeviation)), generateWeightList());*/
            int tempo = exploredWorld[target[1]][target[0]];
            exploredWorld[target[1]][target[0]] = 21;
            a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), generateWeightList(), false, null);
            exploredWorld[target[1]][target[0]] = tempo;
        }
        else{
            findGoalSearchingTarget();
            int tempo = exploredWorld[target[1]][target[0]];
            exploredWorld[target[1]][target[0]] = 21;
            a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), generateWeightList(), false, null);
            exploredWorld[target[1]][target[0]] = tempo;
            /*a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), 
                    Math.toIntExact(Math.round(goalSearchingTarget[1] - core.graphicDeviation)), 
                    Math.toIntExact(Math.round(goalSearchingTarget[0] - core.graphicDeviation)), generateWeightList());*/
        }
        int i = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        int j = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        if(i == target[0] && j == target[1]){
            target = null;
        }
        ArrayList<Point2D> path = a.getSolution();
        if(path != null && !path.isEmpty()){
            Vector goal =  getGoal(path);
            Vector direction = PhysicsAgents.getDirectionVec(coordinates, goal);
            setDirection(direction);
            Vector newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
            if(newPosition.getxPos() == coordinates.getxPos() && newPosition.getyPos() == coordinates.getyPos()){
                direction = PhysicsAgents.getDirectionVec(coordinates, new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation));
                newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                setDirection(direction);
            }
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newPosition.getyPos(), coordinates.getxPos()-newPosition.getxPos())));
            this.coordinates.setxPos(newPosition.getxPos());
            this.coordinates.setyPos(newPosition.getyPos());
        }
        else{
            angle.setValue(Math.random()*360);
            findGoalSearchingTarget();
        }
    }
    
    private void findGoalSearchingTarget(){
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        ArrayList<Point2D> availablePoints = new ArrayList<>();
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                if(!(i == x && y == j)){
                    if(exploredWorld[i][j] == 0 || exploredWorld[i][j] == 1 || exploredWorld[i][j] == 2){
                        if(i - 1 >= 0 && exploredWorld[i-1][j] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(i + 1 < exploredWorld.length && exploredWorld[i+1][j] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(j - 1 >= 0 && exploredWorld[i][j-1] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(j + 1 < exploredWorld[0].length && exploredWorld[i][j+1] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                    }
                }
            }
        }
        Point2D randomPoint = availablePoints.get((int)(Math.random()*availablePoints.size()));
        target = new int[2];
        target[0] = Math.toIntExact(Math.round(randomPoint.getY()));
        target[1] = Math.toIntExact(Math.round(randomPoint.getX()));
    }
    
    private Vector getGoal(ArrayList<Point2D> path){
        int index;
        if(path.size() > 1 && !collision(coordinates, new Vector(path.get(1).getY() + core.graphicDeviation, path.get(1).getX() + core.graphicDeviation))){
            index  = 1;
        }
        else{
            index = 0;
        }
        return new Vector(path.get(index).getY() + core.graphicDeviation, path.get(index).getX() + core.graphicDeviation);
    }
    
    private boolean collision(Vector coordinates, Vector goal){
        Vector direction = PhysicsAgents.getDirectionVec(coordinates, goal);
        double iIncrease1 = direction.getyPos()/100.0;
        double iIncrease = iIncrease1 >= 0.01 ? iIncrease1 : 0.01;
        double jIncrease1 = direction.getxPos()/100.0;
        double jIncrease = jIncrease1 >= 0.01 ? jIncrease1 : 0.01;
        for(double i = coordinates.getyPos(); i <= goal.getyPos(); i+=iIncrease)
        {
            for(double j = coordinates.getxPos(); j <= goal.getxPos(); j+=jIncrease){
                if(PhysicsWorld.checkCollision(core, radius, j, i)){
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean isInRange(int y, int x){
        double adjustment = 0.2;
        if(x >= coordinates.getxPos() - adjustment && x <= coordinates.getxPos() + adjustment && y >= coordinates.getyPos() - adjustment && y <= coordinates.getyPos() + adjustment){
            return true;
        }
        return false;
    }
    
    private double[][] generateWeightList(){
        double[][] weightList = new double[exploredWorld.length][exploredWorld[0].length];
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                weightList[i][j] = 1;
            }
        }
        
        for(Vector v: heardNoises){
            Vector noiseSource = new Vector(v.getxPos() + coordinates.getxPos(), v.getyPos() + coordinates.getyPos());
            int distance = (int)PhysicsAgents.getDistance(coordinates, noiseSource);
            int startX = (noiseSource.getxPos() - distance >= 0 ? (int)(noiseSource.getxPos() - distance) : 0);
            int startY = (noiseSource.getyPos() - distance >= 0 ? (int)(noiseSource.getyPos() - distance) : 0);
            int endX = (noiseSource.getxPos() + distance <= exploredWorld.length ? (int)(noiseSource.getxPos() + distance) : exploredWorld.length);
            int endY = (noiseSource.getyPos() + distance <= exploredWorld[0].length ? (int)(noiseSource.getyPos() + distance) : exploredWorld[0].length);
            if(!isInRange(Math.toIntExact(Math.round(noiseSource.getyPos())), Math.toIntExact(Math.round(noiseSource.getxPos())))){
                for(int i = startX; i < endX; i++)
                {
                    for(int j = startY; j < endY; j++)
                    {
                        weightList[i][j] = 0.1;
                    }
                }
            }
        }
        
        for(Agent a: seenGuards){
            int startX = (a.coordinates.getyPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getyPos() - a.normalVisionRange) : 0);
            int startY = (a.coordinates.getxPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getxPos() - a.normalVisionRange) : 0);
            int endX = (a.coordinates.getyPos() + a.normalVisionRange <= exploredWorld.length ? (int)(a.coordinates.getyPos() + a.normalVisionRange) : exploredWorld.length);
            int endY = (a.coordinates.getxPos() + a.normalVisionRange <= exploredWorld[0].length ? (int)(a.coordinates.getxPos() + a.normalVisionRange) : exploredWorld[0].length);
            
            for(int i = startX; i < endX; i++)
            {
                for(int j = startY; j < endY; j++)
                {
                    weightList[i][j] = 1;
                }
            }
        }
        
        for(Agent a: seenIntruders){
            int startX = (a.coordinates.getyPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getyPos() - a.normalVisionRange) : 0);
            int startY = (a.coordinates.getxPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getxPos() - a.normalVisionRange) : 0);
            int endX = (a.coordinates.getyPos() + a.normalVisionRange <= exploredWorld.length ? (int)(a.coordinates.getyPos() + a.normalVisionRange) : exploredWorld.length);
            int endY = (a.coordinates.getxPos() + a.normalVisionRange <= exploredWorld[0].length ? (int)(a.coordinates.getxPos() + a.normalVisionRange) : exploredWorld[0].length);
            
            for(int i = startX; i < endX; i++)
            {
                for(int j = startY; j < endY; j++)
                {
                    weightList[i][j] = 0;
                }
            }
        }
        
        return weightList;
    }
    
    private void setInitialExploredWorld(){
        exploredWorld = new int[core.map.length][core.map[0].length];
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                exploredWorld[i][j] = -1;
            }
        }
        
        /*int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        if(x + 2 < Core.map.length){
            goalSearchingTarget = new int[2];
            goalSearchingTarget[0] = y;
            goalSearchingTarget[1] = x + 2;
        }
        else{
            goalSearchingTarget = new int[2];
            goalSearchingTarget[0] = y;
            goalSearchingTarget[1] = x - 2;
        }*/
    }
}
