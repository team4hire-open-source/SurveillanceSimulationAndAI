package agents;

import core.Core;
import java.awt.Point;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import physics.Vector;

public abstract class Agent {
    public Core core;
    public double normalVisionRange;
    public double radius;
    public Vector coordinates;
    public double radians;
    public double velocity = 1.4;
    public Vector direction = new Vector();
    public DoubleProperty angle = new SimpleDoubleProperty();
    public DoubleProperty visionRange = new SimpleDoubleProperty();
    public double visionAngle;
    public int agentCode;
    public static int codeCount = 0;
    private boolean hasFinishedTask = true;

    /**
     * all the times are end times except timeInDark! methods will check when timer is done
     */

    //shows if an agent can see or not and if it cannot for how long
    public boolean canHear = true;
    public boolean canSee = true;
    public boolean canMove = true;
    public long timeNotHear;
    public long timeNotSee;
    public long timeNotMove;

    //for entering doors and windows
    //for isEnteringDoor 0 = not entering, 1 = entering silent, 2 = entering loud
    public int isEnteringDoor = 0;
    public long timeEnteringDoor;
    public boolean isEnteringWindow = false;
    public long timeEnteringWindow;

    //for when an agent is in a dark area for more than 10 sec
    public boolean isHidden;
    public boolean inDarkArea = false;
    public long timeInDark;

    //for random move
    public Vector randomDir;
    public long ticks = Core.millisecondsCounter;
    public long startticks = 0;
    public int waitTime = 5000;

    
    public Agent(Core core){
        this.core = core;
        radius = core.squareSizeInMeters/3.0;
        codeCount++;
        this.agentCode = codeCount;
    }
    //index 0 is speed
    //index 1 is x
    //index 2 is y
    public void getRandomMove() {
        ticks = Core.millisecondsCounter;

        if (ticks - startticks >= waitTime) {

            waitTime = (int) ((Math.random()*5000) + 3000);
            startticks = ticks;
        }
        setDirection(new Vector(Math.random()*2-1,Math.random()*2-1));
        AgentController.move(core, this , Math.random()+0.4 , this.direction);
    }

    public double getRadiansDirection(){
        return this.radians;
    }

    public void move(){}

    public Vector getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Vector coordinates) {
        this.coordinates.setxPos(coordinates.getxPos());
        this.coordinates.setyPos(coordinates.getyPos());
    }
    
    public void setDirection(Vector direction){
        this.direction.setxPos(direction.getxPos());
        this.direction.setyPos(direction.getyPos());
    }
    
    public void setAngle(double value){
        angle.set(value - visionAngle/2.0);
    }

    public Vector getDirection() {
        return this.direction;
    }

    public Boolean getHidden() {
        return isHidden;
    }

    public void setHidden(Boolean hidden) {
        isHidden = hidden;
    }

    public DoubleProperty getAngle()
    {
        return this.angle;
    }
    
    public Vector setRandomInitialCoordinates(){
        ArrayList<Point> freePoints = new ArrayList<>();
        for(int i = 0; i < core.map.length; i++)
        {
            for(int j = 0; j < core.map[0].length; j++)
            {
                if(core.map[i][j] == 0 || core.map[i][j] == 2){
                    freePoints.add(new Point(j, i));
                }
            }
        }
        Point randomPoint = freePoints.get((int)(Math.random()*freePoints.size()));
        return new Vector(randomPoint.x + core.squareSizeInMeters/2.0, randomPoint.y + core.squareSizeInMeters/2.0);
    }
    
    public void setVison(double normalVisionRange){
        this.normalVisionRange = normalVisionRange;
    }

    public int getAgentCode(){
      return this.agentCode;
    }

    public boolean isHasFinishedTask() {
        return hasFinishedTask;
    }

    public void setHasFinishedTask(boolean hasFinishedTask) {
        this.hasFinishedTask = hasFinishedTask;
    }
}
