package agents.intruders;

import agents.Agent;
import agents.AgentController;
import core.Core;
import physics.Point;
import physics.Vector;

public class Intruder extends Agent {
    private final double IntruderNormalVisionRange = 7.5;
    final int type = 2;

    public boolean canRun = true;
    public boolean isRunning = false;
    public long timeStartRun = 0;
    public long timeStartRest = 0;
    // aka the intruder rested for at least 10 seconds

    public boolean wantDraw = false;
    public long timeDraw;
    public boolean inTarget = false;
    public long timeEnteredTarget;
    public int countInTarget = 0;
    public Point entryPoint;

    public boolean caught = false;

    public Intruder(Core core, boolean randomCoordinates){
        super(core);
        if(randomCoordinates){
            this.coordinates = setRandomInitialCoordinates();
        }
        else{
            entryPoint = AgentController.getEntryPoint(core, this);
            core.intruderEntryPoints.add(entryPoint);
            this.coordinates = new Vector(entryPoint.y + 0.5, entryPoint.x + 0.5);
        }
        initialize();
    }

    public Intruder(Core core, Vector vector){
        super(core);
        this.coordinates = vector;
        initialize();
    }
    
    private void initialize(){
        this.setVison(IntruderNormalVisionRange);
        this.visionRange.setValue(normalVisionRange);
        this.visionAngle = 45;
    }

    public void move(){
        getRandomMove();
    }

    public boolean Sprint(Vector direction) {
        if (AgentController.canRun(this)) {
            AgentController.startSprint(core, this, direction);
            return true;
        }
        else {
            return false;
        }
    }

    public int getType() {
        return type;
    }
    
}
