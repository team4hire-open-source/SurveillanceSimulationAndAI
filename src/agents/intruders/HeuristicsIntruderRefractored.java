package agents.intruders;

import agents.Agent;
import agents.AgentInterface;
import agents.guards.Guard;
import core.Core;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import physics.PhysicsAgents;
import physics.Point;
import physics.Vector;
import physics.algorithms.AssertEnviroment;
import physics.algorithms.AssertEnviromentRefractored;
import physics.algorithms.SeenTerrain;

public class HeuristicsIntruderRefractored extends Intruder implements AgentInterface{
    public static int[][] EXPLOREDWORLD;
    private int[][] exploredWorld; //-1 visited //-2 unknown
    private double[][] exploredWorldWeights;
    public double[] genes = new double[]{1, 20, 50, 30, 20, 1, 20, 20, 10, 5};
    private ArrayList<Point2D> path;
    private int pathIndex;
    private Point2D pathGoal;
    
    private Point2D finalDestination;
    
    private ArrayList<Point> foundTargets = new ArrayList<>();
    private ArrayList<Point> foundTowers = new ArrayList<>();
    private boolean angleRotationLeft;
    
    public HeuristicsIntruderRefractored(Core core, boolean randomCoordinates){
        super(core, randomCoordinates);
        initialize();
    }
    
    public HeuristicsIntruderRefractored(Core core, Vector coordinates){
        super(core, coordinates);
        initialize();
    }
    
    @Override
    public void move(){
        if(canSee){
            updateExploredWorld();
            executeMoveVersion2();
        }
        else{
            
        }
    }
    
    private void executeMoveVersion2(){
        AssertEnviromentRefractored a = new AssertEnviromentRefractored(false, exploredWorld, coordinates.getyPos(), coordinates.getxPos(), 
            exploredWorldWeights, genes, core.pathMap, finalDestination);
        finalDestination = null;
        if(a.stayAndObserve){
            double[] angleRotation = PhysicsAgents.getBlindMaxAngleRad();
            angleRotation[0] = Math.toDegrees(angleRotation[0]);
            if(angleRotationLeft){
                double angle = this.angle.doubleValue() + visionAngle/2.0 - angleRotation[0];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
            }
            else{
                double angle = this.angle.doubleValue() + visionAngle/2.0 + angleRotation[0];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
            }
        }
        else{
            Point2D goal = a.nextStep;
            finalDestination = a.finalDestination;
            goal = new Point2D(goal.getY(), goal.getX());
            double pointAngle = 180-Math.toDegrees(Math.atan2(coordinates.getyPos()-goal.getY(), coordinates.getxPos()-goal.getX()));
            double angle = (this.angle.doubleValue() + (visionAngle/2.0));
            if(angle > 360){
                angle = angle - 360;
            }
            double[] rotationMaximum = PhysicsAgents.getBlindMaxAngleRad();
            rotationMaximum[0] = Math.toDegrees(rotationMaximum[0]);
            rotationMaximum[1] = Math.toDegrees(rotationMaximum[1]);
            if(pointAngle < angle - rotationMaximum[0]){
                angle = angle - rotationMaximum[1];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
                angleRotationLeft = true;
            }
            else if(pointAngle > angle + rotationMaximum[0]){
                angle = angle + rotationMaximum[1];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
                angleRotationLeft = false;
            }
            else{
                Vector direction = new Vector(goal.getX(), goal.getY());
                setAngle(pointAngle);
                direction = PhysicsAgents.getDirectionVec(coordinates, direction);
                Vector newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                if(newPosition.getxPos() == coordinates.getxPos() && newPosition.getyPos() == coordinates.getyPos()){
                    Vector newGoal = new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation);
                    direction = PhysicsAgents.getDirectionVec(coordinates, newGoal);
                    setDirection(direction);
                    setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newGoal.getyPos(), coordinates.getxPos()-newGoal.getxPos())));
                    //AgentController.move(core, this, velocity, direction);
                    newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                    
                }
                else{
                    //AgentController.move(core, this, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                }
            }
        }
    }
    
    private void executeMoveRefractored(){
        AssertEnviromentRefractored a = new AssertEnviromentRefractored(false, exploredWorld, coordinates.getyPos(), coordinates.getxPos(), finalDestination, 
            exploredWorldWeights, genes, core.pathMap);
        if(a.stayAndObserve){
            double[] angleRotation = PhysicsAgents.getBlindMaxAngleRad();
            angleRotation[0] = Math.toDegrees(angleRotation[0]);
            if(angleRotationLeft){
                double angle = this.angle.doubleValue() + visionAngle/2.0 - angleRotation[0];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
            }
            else{
                double angle = this.angle.doubleValue() + visionAngle/2.0 + angleRotation[0];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
            }
        }
        else{
            Point2D goal = a.nextStep;
            finalDestination = a.finalDestination;
            goal = new Point2D(goal.getY(), goal.getX());
            double pointAngle = 180-Math.toDegrees(Math.atan2(coordinates.getyPos()-goal.getY(), coordinates.getxPos()-goal.getX()));
            double angle = (this.angle.doubleValue() + (visionAngle/2.0));
            if(angle > 360){
                angle = angle - 360;
            }
            double[] rotationMaximum = PhysicsAgents.getBlindMaxAngleRad();
            rotationMaximum[0] = Math.toDegrees(rotationMaximum[0]);
            rotationMaximum[1] = Math.toDegrees(rotationMaximum[1]);
            if(pointAngle < angle - rotationMaximum[0]){
                angle = angle - rotationMaximum[1];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
                angleRotationLeft = true;
            }
            else if(pointAngle > angle + rotationMaximum[0]){
                angle = angle + rotationMaximum[1];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
                angleRotationLeft = false;
            }
            else{
                Vector direction = new Vector(goal.getX(), goal.getY());
                setAngle(pointAngle);
                direction = PhysicsAgents.getDirectionVec(coordinates, direction);
                Vector newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                if(newPosition.getxPos() == coordinates.getxPos() && newPosition.getyPos() == coordinates.getyPos()){
                    Vector newGoal = new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation);
                    direction = PhysicsAgents.getDirectionVec(coordinates, newGoal);
                    setDirection(direction);
                    setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newGoal.getyPos(), coordinates.getxPos()-newGoal.getxPos())));
                    //AgentController.move(core, this, velocity, direction);
                    newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                    
                }
                else{
                    //AgentController.move(core, this, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                }
            }
        }
    }
    
    private void executeMove(){
        double x = coordinates.getyPos();
        double y = coordinates.getxPos();
        //System.out.println(pathIndex);
        double aa = angle.doubleValue()+visionAngle/2.0;
        if(aa > 360){
            aa = aa - 360;
        }
        AssertEnviroment a = new AssertEnviroment(core.pathMap, genes, x, y, pathGoal, (aa), exploredWorld, exploredWorldWeights);
        if(a.stayAndObserve){
            double[] angleRotation = PhysicsAgents.getBlindMaxAngleRad();
            angleRotation[0] = Math.toDegrees(angleRotation[0]);
            if(angleRotationLeft){
                double angle = this.angle.doubleValue() + visionAngle/2.0 - angleRotation[0];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
            }
            else{
                double angle = this.angle.doubleValue() + visionAngle/2.0 + angleRotation[0];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
            }
            setAngle(angle.doubleValue() + visionAngle/2.0 - angleRotation[0]);
            
            setDirection(new Vector(Math.cos(angleRotation[0]), Math.sin(angleRotation[0])));
        }
        else{
            Point2D goal = a.pointToExplore;
            pathGoal = a.goal;
            goal = new Point2D(goal.getY(), goal.getX());
            if(pathGoal != null){
                System.out.println("стх: " + coordinates.getyPos() + " " + coordinates.getxPos());
                System.out.println(pathGoal.getY() + " " + pathGoal.getX());
            }
            System.out.println(goal.getX() + " " + goal.getY());
            double pointAngle = 180-Math.toDegrees(Math.atan2(coordinates.getyPos()-goal.getY(), coordinates.getxPos()-goal.getX()));
            double angle = (this.angle.doubleValue() + (visionAngle/2.0));
            if(angle > 360){
                angle = angle - 360;
            }
            double[] rotationMaximum = PhysicsAgents.getBlindMaxAngleRad();
            rotationMaximum[0] = Math.toDegrees(rotationMaximum[0]);
            rotationMaximum[1] = Math.toDegrees(rotationMaximum[1]);
            System.out.println(pointAngle + " " + angle);
            System.out.println(rotationMaximum[0] + " " + rotationMaximum[1]);
            if(pointAngle < angle - rotationMaximum[0]){
                angle = angle - rotationMaximum[1];
                if(angle < 0){
                    angle = 360.0 + angle;
                }
                setAngle(angle);
                angleRotationLeft = true;
                setDirection(new Vector(Math.cos(rotationMaximum[1]), Math.sin(rotationMaximum[1])));
            }
            else if(pointAngle > angle + rotationMaximum[0]){
                angle = angle + rotationMaximum[1];
                if(angle > 360){
                    angle = angle - 360;
                }
                setAngle(angle);
                angleRotationLeft = true;
                setDirection(new Vector(Math.cos(rotationMaximum[1]), Math.sin(rotationMaximum[1])));
            }
            else{
                Vector direction = new Vector(goal.getX(), goal.getY());
                setAngle(pointAngle);
                direction = PhysicsAgents.getDirectionVec(coordinates, direction);
                Vector newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                System.out.println(direction.getxPos() + " " + direction.getyPos());
                System.out.println(pointAngle);
                System.out.println(newPosition.getxPos() + " " + newPosition.getyPos());
                System.out.println(coordinates.getxPos() + " " + coordinates.getyPos());
                if(newPosition.getxPos() == coordinates.getxPos() && newPosition.getyPos() == coordinates.getyPos()){
                    Vector newGoal = new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation);
                    direction = PhysicsAgents.getDirectionVec(coordinates, newGoal);
                    setDirection(direction);
                    setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newGoal.getyPos(), coordinates.getxPos()-newGoal.getxPos())));
                    //AgentController.move(core, this, velocity, direction);
                    newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                    
                }
                else{
                    //AgentController.move(core, this, velocity, direction);
                    this.coordinates.setxPos(newPosition.getxPos());
                    this.coordinates.setyPos(newPosition.getyPos());
                }
            }
            
        }
    }
    
    private void updateExploredWorld(){
        ArrayList<Vector> heardNoises = PhysicsAgents.iHear(core, coordinates);
        SeenTerrain s = new SeenTerrain();
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        int[][] seenMap = s.getSeenMap(core, x, y, angle.getValue(), visionAngle, visionRange.doubleValue());
        updateExploredWorld(seenMap, heardNoises, s.seenGuards, s.seenIntruders);
    }
    
    private void updateExploredWorld(int[][] seenMap, ArrayList<Vector> heardNoises, ArrayList<Guard> G, ArrayList<Intruder> I){
        for(int i = 0; i < seenMap.length; i++){
            for(int j = 0; j < seenMap[0].length; j++){
                if(seenMap[i][j] != -1 && seenMap[i][j] != -2){
                    if(seenMap[i][j] == 21){
                        if(!isTargetFound(new Point(i, j), foundTargets)){
                            foundTargets.add(new Point(i, j));
                        }
                    }
                    else if(seenMap[i][j] == 13){
                        if(!isTargetFound(new Point(i, j), foundTowers)){
                            foundTowers.add(new Point(i, j));
                        }
                    }
                    exploredWorld[i][j] = seenMap[i][j];
                }
            }
        }
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                exploredWorldWeights[i][j]=genes[0];
                for(Point t: foundTowers){
                    if(new Point2D(t.x, t.y).distance(new Point2D(i, j)) <= 15){
                        exploredWorldWeights[i][j]+=genes[4];
                    }
                }
            }
        }
        
        for(Vector v: heardNoises){
            Vector noiseSource = new Vector(v.getxPos() + coordinates.getxPos(), v.getyPos() + coordinates.getyPos());
            int distance = Math.toIntExact(Math.round(PhysicsAgents.getDistance(coordinates, noiseSource)));
            int startX = (noiseSource.getxPos() - distance >= 0 ? Math.toIntExact(Math.round(noiseSource.getxPos() - distance)) : 0);
            int startY = (noiseSource.getyPos() - distance >= 0 ? Math.toIntExact(Math.round(noiseSource.getyPos() - distance)) : 0);
            int endX = (noiseSource.getxPos() + distance <= exploredWorld.length ? Math.toIntExact(Math.round(noiseSource.getxPos() + distance)) : exploredWorld.length);
            int endY = (noiseSource.getyPos() + distance <= exploredWorld[0].length ? Math.toIntExact(Math.round(noiseSource.getyPos() + distance)) : exploredWorld[0].length);
            if(!isInRange(Math.toIntExact(Math.round(noiseSource.getyPos())), Math.toIntExact(Math.round(noiseSource.getxPos())))){
                for(int i = startX; i < endX; i++)
                {
                    for(int j = startY; j < endY; j++)
                    {
                        exploredWorldWeights[i][j]+=genes[1];
                    }
                }
            }
        }
        
        for(Agent a: G){
            int startX = (a.coordinates.getyPos() - a.normalVisionRange >= 0 ? Math.toIntExact(Math.round(a.coordinates.getyPos() - a.normalVisionRange)) : 0);
            int startY = (a.coordinates.getxPos() - a.normalVisionRange >= 0 ? Math.toIntExact(Math.round(a.coordinates.getxPos() - a.normalVisionRange)) : 0);
            int endX = (a.coordinates.getyPos() + a.normalVisionRange <= exploredWorld.length ? Math.toIntExact(Math.round(a.coordinates.getyPos() + a.normalVisionRange)) : exploredWorld.length);
            int endY = (a.coordinates.getxPos() + a.normalVisionRange <= exploredWorld[0].length ? Math.toIntExact(Math.round(a.coordinates.getxPos() + a.normalVisionRange)) : exploredWorld[0].length);
            
            for(int i = startX; i < endX; i++)
            {
                for(int j = startY; j < endY; j++)
                {
                    exploredWorldWeights[i][j]+=genes[2];
                }
            }
        }
        
        for(Agent a: I){
            if(a != this){
                int startX = (a.coordinates.getyPos() - a.normalVisionRange >= 0 ? Math.toIntExact(Math.round(a.coordinates.getyPos() - a.normalVisionRange)) : 0);
                int startY = (a.coordinates.getxPos() - a.normalVisionRange >= 0 ? Math.toIntExact(Math.round(a.coordinates.getxPos() - a.normalVisionRange)) : 0);
                int endX = (a.coordinates.getyPos() + a.normalVisionRange <= exploredWorld.length ? Math.toIntExact(Math.round(a.coordinates.getyPos() + a.normalVisionRange)) : exploredWorld.length);
                int endY = (a.coordinates.getxPos() + a.normalVisionRange <= exploredWorld[0].length ? Math.toIntExact(Math.round(a.coordinates.getxPos() + a.normalVisionRange)) : exploredWorld[0].length);

                for(int i = startX; i < endX; i++)
                {
                    for(int j = startY; j < endY; j++)
                    {
                        exploredWorldWeights[i][j]+=genes[3];
                    }
                }
            }
        }
        /*for(int i = 0; i < exploredWorld.length; i++){
            System.out.println();
            for(int j = 0; j < exploredWorld[0].length; j++){
                System.out.print(exploredWorld[i][j]);
            }
        }*/
    }
    
    private boolean isInRange(int y, int x){
        double adjustment = 0.2;
        if(x >= coordinates.getxPos() - adjustment && x <= coordinates.getxPos() + adjustment && y >= coordinates.getyPos() - adjustment && y <= coordinates.getyPos() + adjustment){
            return true;
        }
        return false;
    }
    
    private boolean isTargetFound(Point p, ArrayList<Point> points){
        for(Point pp: points){
            if(p.x == pp.x && p.y == pp.y)
            {
                return true;
            }
        }
        return false;
    }
    
    private void initialize(){
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        if(x == 0){
            setDirection(new Vector(coordinates.getxPos() + 1, coordinates.getyPos()));
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getxPos()- direction.getxPos(), coordinates.getyPos()-direction.getyPos())));
        }
        else if(x == core.map.length){
            setDirection(new Vector(coordinates.getxPos() - 1, coordinates.getyPos()));
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getxPos()- direction.getxPos(), coordinates.getyPos()-direction.getyPos())));
        }
        else if(y == 0){
            setDirection(new Vector(coordinates.getxPos(), coordinates.getyPos() + 1));
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getxPos()- direction.getxPos(), coordinates.getyPos()-direction.getyPos())));
        }
        else{
            setDirection(new Vector(coordinates.getxPos(), coordinates.getyPos() - 1));
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getxPos()- direction.getxPos(), coordinates.getyPos()-direction.getyPos())));
        }
        setInitialExploredWorld();
        updateExploredWorld();
    }
    
    private void setInitialExploredWorld(){
        exploredWorld = new int[core.map.length][core.map[0].length];
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                exploredWorld[i][j] = -1;
            }
        }
        exploredWorldWeights = new double[core.map.length][core.map[0].length];
        if(EXPLOREDWORLD == null){
            exploredWorld = new int[core.map.length][core.map[0].length];
            for(int i = 0; i < exploredWorld.length; i++){
                for(int j = 0; j < exploredWorld[0].length; j++){
                    exploredWorld[i][j] = -1;
                }
            }
        }
    }
}
