package agents.intruders;

import agents.Agent;
import agents.AgentInterface;
import physics.Vector;
import core.Core;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import physics.algorithms.AStar;
import physics.Vision;
import physics.PhysicsAgents;
import physics.PhysicsWorld;
import physics.algorithms.SeenTerrain;

public class HeuristicsIntruder extends Intruder implements AgentInterface {
    private int[][] exploredWorld; //-1 visited //-2 unknown
    private int[] foundTarget;
    private int[] goalSearchingTarget;
    private ArrayList<Agent> seenGuards = new ArrayList<>();
    private ArrayList<Vector> heardNoises = new ArrayList<>();

    public HeuristicsIntruder(Core core, boolean randomCoordinates){
        super(core, randomCoordinates);
        initialize();
    }
    
    public HeuristicsIntruder(Core core, Vector coordinates){
        super(core, coordinates);
        initialize();
    }
    
    private void initialize(){
        setAngle(0);
        setInitialExploredWorld();
    }
    
    public void move() {
        if(!PhysicsWorld.isAgentOnTarget(core, coordinates.getxPos(), coordinates.getyPos()))
        {
            updateExploredWorld();
            generatePathToGoal();
        }
    }
    
    private void updateExploredWorld(){
        Vision v = PhysicsAgents.iSee(core, coordinates, direction, visionRange.getValue(), angle.getValue());
        seenGuards = v.getGuards();
        heardNoises = PhysicsAgents.iHear(core, coordinates);
        SeenTerrain s = new SeenTerrain();
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        int[][] seenMap = s.getSeenMap(core, x, y, angle.getValue(), visionAngle, visionRange.doubleValue());
        seenMap[x][y] = core.map[x][y];
        addNewFoundStructures(seenMap);
    }
    
    private void addNewFoundStructures(int[][] seenMap){
        for(int i = 0; i < seenMap.length; i++){
            for(int j = 0; j < seenMap[0].length; j++){
                if(seenMap[i][j] != -1 && seenMap[i][j] != -2){
                    if(seenMap[i][j] == 21){
                        foundTarget = new int[2];
                        foundTarget[0] = i;
                        foundTarget[1] = j;
                    }
                    exploredWorld[i][j] = seenMap[i][j];
                }
            }
        }
        //System.out.println("new");
        /*for(int i = 0; i < seenMap.length; i++){
            System.out.println();
            for(int j = 0; j < seenMap[0].length; j++){
                System.out.print(exploredWorld[i][j] + " ");
            }
        }*/
    }
    
    private void generatePathToGoal(){
        AStar a;
        if(foundTarget != null){
            a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), generateWeightList(), false, null);
        }
        else{
            if(goalSearchingTarget!=null){
                /*a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - PhysicsWorld.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - PhysicsWorld.graphicDeviation)), 
                        Math.toIntExact(Math.round(goalSearchingTarget[1] - PhysicsWorld.graphicDeviation)), 
                        Math.toIntExact(Math.round(goalSearchingTarget[0] - PhysicsWorld.graphicDeviation)), generateWeightList());*/
                int tempo = exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]];
                exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]] = 21;
                a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), generateWeightList(), false, null);
                exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]] = tempo;
            }
            else{
                findGoalSearchingTarget();
                int tempo = exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]];
                exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]] = 21;
                a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)), generateWeightList(), false, null);
                exploredWorld[goalSearchingTarget[1]][goalSearchingTarget[0]] = tempo;
                /*a = new AStar(exploredWorld,  Math.toIntExact(Math.round(coordinates.getyPos() - PhysicsWorld.graphicDeviation)), Math.toIntExact(Math.round(coordinates.getxPos() - PhysicsWorld.graphicDeviation)), 
                        Math.toIntExact(Math.round(goalSearchingTarget[1] - PhysicsWorld.graphicDeviation)), 
                        Math.toIntExact(Math.round(goalSearchingTarget[0] - PhysicsWorld.graphicDeviation)), generateWeightList());*/
            }
            int i = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
            int j = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
            if(i == goalSearchingTarget[0] && j == goalSearchingTarget[1]){
                goalSearchingTarget = null;
            }
        }
        ArrayList<Point2D> path = a.getSolution();
        if(path != null && !path.isEmpty()){
            Vector goal =  getGoal(path);
            Vector direction = PhysicsAgents.getDirectionVec(coordinates, goal);
            setDirection(direction);
            Vector newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
            if(newPosition.getxPos() == coordinates.getxPos() && newPosition.getyPos() == coordinates.getyPos()){
                direction = PhysicsAgents.getDirectionVec(coordinates, new Vector(Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation)) + core.graphicDeviation, Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation)) + core.graphicDeviation));
                newPosition = PhysicsAgents.move(core, radius, coordinates, velocity, direction);
                setDirection(direction);
            }
            setAngle(180-Math.toDegrees(Math.atan2(coordinates.getyPos()-newPosition.getyPos(), coordinates.getxPos()-newPosition.getxPos())));
            this.coordinates.setxPos(newPosition.getxPos());
            this.coordinates.setyPos(newPosition.getyPos());
            PhysicsWorld.isAgentOnTarget(core, coordinates.getxPos(), coordinates.getyPos());
        }
        else{
            angle.setValue(Math.random()*360);
            findGoalSearchingTarget();
        }
    }
    
    private void findGoalSearchingTarget(){
        int x = Math.toIntExact(Math.round(coordinates.getyPos() - core.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - core.graphicDeviation));
        ArrayList<Point2D> availablePoints = new ArrayList<>();
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                if(!(i == x && y == j)){
                    if(exploredWorld[i][j] == 0 || exploredWorld[i][j] == 1 || exploredWorld[i][j] == 2){
                        if(i - 1 >= 0 && exploredWorld[i-1][j] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(i + 1 < exploredWorld.length && exploredWorld[i+1][j] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(j - 1 >= 0 && exploredWorld[i][j-1] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                        else if(j + 1 < exploredWorld[0].length && exploredWorld[i][j+1] == -1){
                            availablePoints.add(new Point2D(i, j));
                        }
                    }
                }
            }
        }
        Point2D randomPoint = availablePoints.get((int)(Math.random()*availablePoints.size()));
        goalSearchingTarget = new int[2];
        goalSearchingTarget[0] = Math.toIntExact(Math.round(randomPoint.getY()));
        goalSearchingTarget[1] = Math.toIntExact(Math.round(randomPoint.getX()));
    }
    
    private Vector getGoal(ArrayList<Point2D> path){
        int index;
        if(path.size() > 1 && !collision(coordinates, new Vector(path.get(1).getY() + core.graphicDeviation, path.get(1).getX() + core.graphicDeviation))){
            index  = 1;
        }
        else{
            index = 0;
        }
        return new Vector(path.get(index).getY() + core.graphicDeviation, path.get(index).getX() + core.graphicDeviation);
    }
    
    private boolean collision(Vector coordinates, Vector goal){
        Vector direction = PhysicsAgents.getDirectionVec(coordinates, goal);
        double iIncrease1 = direction.getyPos()/100.0;
        double iIncrease = iIncrease1 >= 0.01 ? iIncrease1 : 0.01;
        double jIncrease1 = direction.getxPos()/100.0;
        double jIncrease = jIncrease1 >= 0.01 ? jIncrease1 : 0.01;
        for(double i = coordinates.getyPos(); i <= goal.getyPos(); i+=iIncrease)
        {
            for(double j = coordinates.getxPos(); j <= goal.getxPos(); j+=jIncrease){
                if(PhysicsWorld.checkCollision(core, radius, j, i)){
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean isInRange(int y, int x){
        double adjustment = 0.2;
        if(x >= coordinates.getxPos() - adjustment && x <= coordinates.getxPos() + adjustment && y >= coordinates.getyPos() - adjustment && y <= coordinates.getyPos() + adjustment){
            return true;
        }
        return false;
    }
    
    private double[][] generateWeightList(){
        double[][] weightList = new double[exploredWorld.length][exploredWorld[0].length];
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                weightList[i][j] = 1;
            }
        }
        
        for(Vector v: heardNoises){
            Vector noiseSource = new Vector(v.getxPos() + coordinates.getxPos(), v.getyPos() + coordinates.getyPos());
            int distance = (int)PhysicsAgents.getDistance(coordinates, noiseSource);
            int startX = (noiseSource.getxPos() - distance >= 0 ? (int)(noiseSource.getxPos() - distance) : 0);
            int startY = (noiseSource.getyPos() - distance >= 0 ? (int)(noiseSource.getyPos() - distance) : 0);
            int endX = (noiseSource.getxPos() + distance <= exploredWorld.length ? (int)(noiseSource.getxPos() + distance) : exploredWorld.length);
            int endY = (noiseSource.getyPos() + distance <= exploredWorld[0].length ? (int)(noiseSource.getyPos() + distance) : exploredWorld[0].length);
            if(!isInRange(Math.toIntExact(Math.round(noiseSource.getyPos())), Math.toIntExact(Math.round(noiseSource.getxPos())))){
                for(int i = startX; i < endX; i++)
                {
                    for(int j = startY; j < endY; j++)
                    {
                        weightList[i][j] = 5;
                    }
                }
            }
        }
        
        for(Agent a: seenGuards){
            int startX = (a.coordinates.getyPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getyPos() - a.normalVisionRange) : 0);
            int startY = (a.coordinates.getxPos() - a.normalVisionRange >= 0 ? (int)(a.coordinates.getxPos() - a.normalVisionRange) : 0);
            int endX = (a.coordinates.getyPos() + a.normalVisionRange <= exploredWorld.length ? (int)(a.coordinates.getyPos() + a.normalVisionRange) : exploredWorld.length);
            int endY = (a.coordinates.getxPos() + a.normalVisionRange <= exploredWorld[0].length ? (int)(a.coordinates.getxPos() + a.normalVisionRange) : exploredWorld[0].length);
            
            for(int i = startX; i < endX; i++)
            {
                for(int j = startY; j < endY; j++)
                {
                    weightList[i][j] = 1000;
                }
            }
        }
        return weightList;
    }
    
    private void setInitialExploredWorld(){
        exploredWorld = new int[core.map.length][core.map[0].length];
        for(int i = 0; i < exploredWorld.length; i++){
            for(int j = 0; j < exploredWorld[0].length; j++){
                exploredWorld[i][j] = -1;
            }
        }
        
        /*int x = Math.toIntExact(Math.round(coordinates.getyPos() - PhysicsWorld.graphicDeviation));
        int y = Math.toIntExact(Math.round(coordinates.getxPos() - PhysicsWorld.graphicDeviation));
        if(x + 2 < Core.map.length){
            goalSearchingTarget = new int[2];
            goalSearchingTarget[0] = y;
            goalSearchingTarget[1] = x + 2;
        }
        else{
            goalSearchingTarget = new int[2];
            goalSearchingTarget[0] = y;
            goalSearchingTarget[1] = x - 2;
        }*/
    }
}
