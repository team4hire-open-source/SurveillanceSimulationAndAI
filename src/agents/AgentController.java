package agents;
import agents.guards.Guard;
import agents.intruders.Intruder;
import core.Core;
import physics.*;
import java.lang.Math;

import java.util.ArrayList;

import static core.Core.*;

public class AgentController {
    private static double maxDir = (Math.PI)*(gameTick/1000);

    //maybe return an array of all the sounds made by entering doors/windows
    public static int checkAllAgents(Core core){
        // 0 - no one won this game tick
        // 1 - intruders won
        // 2 - draw
        // 3 - guards won
        int whoWon = 0;

        //checks guards
        for(int i = 0; i < core.guards.size(); i++) {
            Guard g = core.guards.get(i);
            if (g.isEnteringDoor == 1 || g.isEnteringDoor == 2 || g.isEnteringWindow) {
                if (g.isEnteringWindow) {
                    if (g.timeEnteringWindow < secondsCounter) {
                        core.noise.noiseEnter(false, g.coordinates);
                        g.isEnteringWindow = false;
                        g.canMove = true;
                        moveThroughDW(g, false);
                    }
                }
                if (g.isEnteringDoor == 2) {
                    if (g.timeEnteringDoor < secondsCounter) {
                        core.noise.noiseEnter(true, g.coordinates);
                        g.isEnteringDoor = 0;
                        g.canMove = true;
                        moveThroughDW(g, true);
                    }
                }
                else if (g.isEnteringDoor == 1) {
                    if(g.timeEnteringDoor < secondsCounter){
                        g.isEnteringDoor = 0;
                        g.canMove = true;
                        moveThroughDW(g, true);
                    }
                }
            }
            else {
                if (!g.canHear) {
                    if (g.timeNotHear < secondsCounter) {
                        g.canHear = true;
                    }
                }
                if (!g.canSee) {
                    if (g.timeNotSee < secondsCounter) {
                        g.canHear = true;
                    }
                }
                if (!g.canMove) {
                    if (g.timeNotMove < secondsCounter) {
                        g.canHear = true;
                    }
                }
            }
            //We should probably check where agent is on the map, that sounds the easiest for checking if in dark area
            //check if in dark area
            if (PhysicsWorld.isAgentInDark(core, g.coordinates.getyPos(), g.coordinates.getxPos())) {
                if(g.isHidden){ }
                else if(g.inDarkArea){
                    if(g.timeInDark + 10 < secondsCounter){
                        g.isHidden = true;
                    }
                }
                else{
                    g.inDarkArea = true;
                    g.timeInDark = secondsCounter;
                }
            }
            else {
                g.isHidden = false;
                g.inDarkArea = false;
            }
        }
        //checks intruders
        for(int i = 0; i < core.intruders.size(); i++){
            Intruder in = core.intruders.get(i);
            if (in.isEnteringDoor == 1 || in.isEnteringDoor == 2 || in.isEnteringWindow) {
                if (in.isEnteringWindow) {
                    if (in.timeEnteringWindow < secondsCounter) {
                        core.noise.noiseEnter(false, in.coordinates);
                        in.isEnteringWindow = false;
                        in.canMove = true;
                        moveThroughDW(in, false);
                    }
                }
                if (in.isEnteringDoor == 2) {
                    if (in.timeEnteringDoor < secondsCounter) {
                        core.noise.noiseEnter(true, in.coordinates);
                        in.isEnteringDoor = 0;
                        in.canMove = true;
                        moveThroughDW(in, true);
                    }
                }
                else if (in.isEnteringDoor == 1) {
                    if(in.timeEnteringDoor < secondsCounter){
                        in.isEnteringDoor = 0;
                        in.canMove = true;
                        moveThroughDW(in, true);
                    }
                }
            }
            else {
                if (!in.canHear) {
                    if (in.timeNotHear < secondsCounter) {
                        in.canHear = true;
                    }
                }
                if (!in.canSee) {
                    if (in.timeNotSee < secondsCounter) {
                        in.canHear = true;
                    }
                }
                if (!in.canMove) {
                    if (in.timeNotMove < secondsCounter) {
                        in.canHear = true;
                    }
                }
            }
            //Timer reset resting sprint
            if(secondsCounter - in.timeStartRest >= 10 && in.timeStartRest != 0) {
                in.canRun = true;
                in.isRunning = false;
                in.timeStartRest = 0;
                in.timeStartRun = 0;
            }
            //checking if in target area
            if(PhysicsWorld.isAgentOnTarget(core, in.coordinates.getyPos(), in.coordinates.getxPos())){
                System.out.println("IN target area");
                if(in.inTarget){
                    if(in.timeEnteredTarget + 3 <= secondsCounter){
                        //have to add an actual ending screen
                        whoWon = 1;
                    }
                }
                else{
                    if(in.countInTarget == 0) {
                        in.inTarget = true;
                        in.timeEnteredTarget = secondsCounter;
                        in.countInTarget++;
                    }
                    else if(in.timeEnteredTarget + 3 < secondsCounter){
                        whoWon = 1;
                    }

                }
            }
            else{
                //System.out.println("NOT IN target area");
            }
            //check for draw
            if(in.wantDraw){
                if(in.timeDraw + 3 < secondsCounter){
                    whoWon = 2;
                }
            }
            //check if in dark area
            if (PhysicsWorld.isAgentInDark(core, in.coordinates.getyPos(), in.coordinates.getxPos())) {
                if(in.isHidden){ }
                else if(in.inDarkArea){
                    if(in.timeInDark + 10 < secondsCounter){
                        in.isHidden = true;
                    }
                }
                else{
                    in.inDarkArea = true;
                    in.timeInDark = secondsCounter;
                }
            }
            else {
                in.isHidden = false;
                in.inDarkArea = false;
            }
            //checking run timers can be put here
        }
        //checks if guards win
        for(Guard g : core.guards){
            for(Intruder i : core.intruders){
                double difference = Math.sqrt(Math.pow(g.coordinates.getxPos()-i.coordinates.getxPos(),2) + Math.pow(g.coordinates.getyPos()-i.coordinates.getyPos(),2));
                if(difference >= -0.5 && difference <= 0.5){
                    i.caught = true;
                }
            }
        }
        for(int i = 0; i < core.intruderCount; i++){
            if(!(i >= core.intruders.size())){
                if(core.intruders.get(i).caught){
                    core.intruders.remove(i);
                }
            }
        }
        if(core.intruders.size() == 0){
            whoWon = 3;
        }
        return whoWon;
    }
    public static Vector setVectorDirection(double radians, Vector direction){
        direction.setxPos(Math.cos(radians));
        direction.setyPos(Math.sin(radians));
        return direction;
    }

    public static double setRadiansDirection(Vector direction){
        return Math.atan2(direction.getyPos(), direction.getxPos());
    }

    public static void move(Core core, Intruder intruder, double speed, Vector direction){
        //if direction change is more than 45 degrees then agent does not see for 0.5 seconds and turing time
        double agentDir = setRadiansDirection(intruder.getCoordinates());
        double newDir = setRadiansDirection(direction);
        if (Math.abs(agentDir - newDir) > maxDir) {
            if (agentDir - newDir > 0) {
                newDir = agentDir + maxDir;
            }
            else {
                newDir = agentDir - maxDir;
            }
        }
        Vector newDirection = setVectorDirection(newDir, direction);
        if (intruder.isRunning) {
            if (speed != 3) {
                intruder.canRun = false;
                intruder.isRunning = false;
                intruder.timeStartRest = secondsCounter;
                intruder.timeStartRun = 0;
            }
        }
        Vector newCo = PhysicsAgents.move(core, intruder.radius, intruder.coordinates, speed, newDirection);
        intruder.angle.set(180-Math.toDegrees(Math.atan2(intruder.coordinates.getyPos()-newCo.getyPos(), intruder.coordinates.getxPos()-newCo.getxPos())));
        intruder.setCoordinates(newCo);
    }

    public static void move(Core core, Guard guard, double speed, Vector direction) {
        //if direction change is more than 45 degrees then agent does not see for 0.5 seconds and turing time
        double agentDir = setRadiansDirection(guard.getCoordinates());
        double newDir = setRadiansDirection(direction);
        if (Math.abs(agentDir - newDir) > maxDir) {
            if (agentDir - newDir > 0) {
                newDir = agentDir + maxDir;
            }
            else {
                newDir = agentDir - maxDir;
            }
        }
        Vector newDirection = setVectorDirection(newDir, direction);
        Vector newCo = PhysicsAgents.move(core, guard.radians, guard.coordinates, speed, newDirection);
        guard.angle.set(180-Math.toDegrees(Math.atan2(guard.coordinates.getyPos()-newCo.getyPos(), guard.coordinates.getxPos()-newCo.getxPos())));
        guard.setCoordinates(newCo);

    }

    public static void move(Core core, Agent agent, double speed, Vector direction) {
        //if direction change is more than 45 degrees then agent does not see for 0.5 seconds and turing time
        double agentDir = setRadiansDirection(agent.getCoordinates());
        double newDir = setRadiansDirection(direction);
        if (Math.abs(agentDir - newDir) > maxDir) {
            if (agentDir - newDir > 0) {
                newDir = agentDir + maxDir;
            }
            else {
                newDir = agentDir - maxDir;
            }
        }
        Vector newDirection = setVectorDirection(newDir, direction);
        Vector newCo = PhysicsAgents.move(core, agent.radius, agent.coordinates, speed, newDirection);
        agent.angle.set(180-Math.toDegrees(Math.atan2(agent.coordinates.getyPos()-newCo.getyPos(), agent.coordinates.getxPos()-newCo.getxPos())));
        agent.setCoordinates(newCo);

    }


    public static boolean canRun(Intruder intruder) {
        if (intruder.isRunning) {
            if (secondsCounter - intruder.timeStartRun >= 5) {
                intruder.canRun = false;
                intruder.isRunning = false;
                intruder.timeStartRest = secondsCounter;
                intruder.timeStartRun = 0;
                return false;
            }
            else {
                return true;
            }
        }
        else if (intruder.canRun) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void startSprint(Core core, Intruder intruder, Vector direction) {
        if (intruder.isRunning) {
            if (secondsCounter - intruder.timeStartRun >= 5) {
                intruder.canRun = false;
                intruder.isRunning = false;
                intruder.timeStartRest = secondsCounter;
                intruder.timeStartRun = 0;
            }
            else {
                move(core, intruder, 3, direction);
            }
        }
        else {
            intruder.isRunning = true;
            intruder.timeStartRun = secondsCounter;
            move(core, intruder, 3, direction);
        }
         // fix after move class
    }

    public static boolean draw(Intruder intruder){
        boolean canDraw = false;
        int xP = (int) intruder.coordinates.getxPos();
        int yP = (int) intruder.coordinates.getyPos();
        for(int i = xP-1; i < xP+1; i++){
            for(int j = yP-1; j < yP+1; j++){
                if (i == intruder.core.intruderEntryPoints.get(0).x && j == intruder.core.intruderEntryPoints.get(0).y){
                    canDraw = true;
                }
            }
        }
        if(canDraw){
            intruder.timeDraw = secondsCounter;
            intruder.wantDraw = true;
        }
        return canDraw;
    }

    private static void moveThroughDW(Agent agent, boolean door){
        int[][] tmpMap = agent.core.map;
        int xP = (int) agent.coordinates.getxPos();
        int yP = (int) agent.coordinates.getyPos();
        for(int i = xP-1; i < xP+1; i++){
            for(int j = yP-1; j < yP+1; j++){
                if(door){
                    if (tmpMap[i][j] == 11){
                        if(i == xP && j == yP-1){
                            agent.setCoordinates(new Vector(i, j - 1));
                        }
                        if(i == xP && j == yP+1){
                            agent.setCoordinates(new Vector(i, j + 1));
                        }
                        if(i == xP-1 && j == yP){
                            agent.setCoordinates(new Vector(i - 1, j));
                        }
                        if(i == xP+1 && j == yP){
                            agent.setCoordinates(new Vector(i + 1, j));
                        }
                    }
                }
                else {
                    if (tmpMap[i][j] == 12) {
                        if (i == xP && j == yP - 1) {
                            agent.setCoordinates(new Vector(i, j - 1));
                        }
                        if (i == xP && j == yP + 1) {
                            agent.setCoordinates(new Vector(i, j + 1));
                        }
                        if (i == xP - 1 && j == yP) {
                            agent.setCoordinates(new Vector(i - 1, j));
                        }
                        if (i == xP + 1 && j == yP) {
                            agent.setCoordinates(new Vector(i + 1, j));
                        }
                    }
                }
            }
        }

    }

    //1 is silent entering, 2 is loud entering
    public static void enteringDoor(Agent agent, int type){
        //silent entering
        if(type == 1){
            agent.isEnteringDoor = 1;
            agent.timeEnteringDoor = getTime(NormalDist.NormalDist(12, 2));
            agent.canMove = false;
        }
        //fast and loud entering
        else if(type == 2){
            agent.isEnteringDoor = 2;
            agent.timeEnteringDoor = getTime(5);
            agent.canMove = false;
        }
    }

    public static void enteringWindow(Agent agent){
        agent.isEnteringWindow = true;
        agent.timeEnteringWindow = getTime(3);
        agent.canMove = false;
    }

    public static void leavingTower(Guard guard){
        guard.visionRange.setValue(6);
        guard.inTower = false;
        //set timers
        setHearingTo(guard, false, 3);
        setVisionTo(guard, false, 3);
        setMovingTo(guard, false, 3);
    }
    public static void enterTower(Guard guard){
        guard.visionRange.setValue(15);
        guard.inTower = true;
        //set timers
        setHearingTo(guard, false, 3);
        setVisionTo(guard, false, 3);
        setMovingTo(guard, false, 3);
    }

    //you can set vision and etc. to false or true,
    //if setting to true then the time, in seconds, matters but does not for setting to false.
    public static void setVisionTo(Agent agent, boolean to, double time){
        if(to){
            agent.canSee = to;
        }
        else{
            agent.canSee = to;
            agent.timeNotSee = getTime(time);
        }
    }
    public static void setHearingTo(Agent agent, boolean to, double time){
        if(to){
            agent.canHear = to;
        }
        else{
            agent.canHear = to;
            agent.timeNotHear = getTime(time);
        }
    }
    public static void setMovingTo(Agent agent, boolean to, double time){
        if(to){
            agent.canMove = to;
        }
        else{
            agent.canMove = to;
            agent.timeNotMove = getTime(time);
        }
    }
    
    public static Point getEntryPoint(Core core, Intruder intruder){
        ArrayList<Point> holes = new ArrayList<>();
        for(int i = 1; i < core.map.length - 1; i++){
            holes.add(new Point(0, i));
            for(Point p: core.intruderEntryPoints){
                if(p.x == 0 && p.y == i){
                    holes.remove(holes.size()-1);
                }
            }
            holes.add(new Point(core.map.length - 1, i));
            for(Point p: core.intruderEntryPoints){
                if(p.x == core.map.length-1 && p.y == i){
                    holes.remove(holes.size()-1);
                }
            }
        }
        for(int i = 1; i < core.map.length - 1; i++){
            holes.add(new Point(i, 0));
            for(Point p: core.intruderEntryPoints){
                if(p.x == i && p.y == 0){
                    holes.remove(holes.size()-1);
                }
            }
            holes.add(new Point(i, core.map.length - 1));
            for(Point p: core.intruderEntryPoints){
                if(p.x == i && p.y == core.map.length-1){
                    holes.remove(holes.size()-1);
                }
            }
        }
        Point entry = holes.get(Math.toIntExact(Math.round(Math.random()*holes.size())));
        core.displayEntryPoint(entry);
        return entry;
    }

    //gets the time and adds the times given to the function, in seconds
    public static long getTime(){
        return secondsCounter;
    }
    public static long getTime(long extraTime){
        return secondsCounter + extraTime;
    }
    public static long getTime(double extraTime){
        return secondsCounter + (long) extraTime;
    }
    public static long getTime(int extraTime){
        return secondsCounter + extraTime;
    }
}
