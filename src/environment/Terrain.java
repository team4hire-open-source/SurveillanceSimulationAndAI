package environment;

import agents.Agent;
import physics.NoiseModel;

import java.util.ArrayList;

public class Terrain {
    //Terrain ID numbers
    //0-Road
    //1-Floor
    //2-Low Visibility
    //10-Wall
    //11-Door
    //12-Window
    //13-Sentry Tower
    //20-Entry Point
    //21-Target Area

    int[][] Terrain;
    ArrayList<Agent> Agents;
    ArrayList<NoiseModel> Noises;

    public Terrain(int[][] Terrain) {
        this.Terrain = Terrain;
    }

    public int getTerInf(int x, int y) {
        return Terrain[x][y];
    }

    public ArrayList<Agent> getAgents() {
        return Agents;
    }

    public ArrayList<NoiseModel> getNoises() {
        return Noises;
    }
}
