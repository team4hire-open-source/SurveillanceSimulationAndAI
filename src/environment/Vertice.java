package environment;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import physics.Point;
import physics.Vector;

public class Vertice {

  private Vector position;
  private int type;
  public Point2D coordinates;
  public Point origin;
  public boolean isTrueTarget = false;
  public ArrayList<Vertice> links = new ArrayList<>();

  public Vertice(Vector position, int type) {
    this.position = position;
    this.type = type;
  }
  
  public Vertice(Point2D coordinates, Point origin){
      this.coordinates = coordinates;
      this.origin = origin;
  }

  public Vector getPosition() {
    return position;
  }

  public void setPosition(Vector position) {
    this.position = position;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }
}
