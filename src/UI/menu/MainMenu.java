package UI.menu;

import core.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Screen;

/**
 * Class creating the main menu
 */
public class MainMenu extends VBox{
    //menu style and size variables
    private Background MenuBackground, ButtonBackground;
    private String ButtonStyle;
    private int ButtonWidth, ButtonHeight, MenuWidth, MenuHeight, MenuSpacing;
    
    public MainMenu(){
        loadVariables();
        loadMenu();
    }

    private void loadVariables(){
        MenuBackground = new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonBackground = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonStyle = "-fx-font: 22 arial;";
        ButtonWidth = 220;
        ButtonHeight = 50;
        MenuSpacing = 20;
        MenuWidth = ButtonWidth + 100;
        MenuHeight = ButtonHeight*3 + MenuSpacing + 100;
    }

    public void loadMenu() {
        //TERRAIN SELECTION
        Button selectTerrain = new Button("Select terrain");
        selectTerrain.setMinSize(ButtonWidth, ButtonHeight);
        selectTerrain.setStyle(ButtonStyle);
        selectTerrain.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.loadTerrainSelectionMenu();
            }
        });

        // TERRAIN DESIGN
        Button createTerrain = new Button("Create terrain");
        createTerrain.setMinSize(ButtonWidth, ButtonHeight);
        createTerrain.setStyle(ButtonStyle);
        //Change stage to terrain design
        createTerrain.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.loadTerrainDesignMenu();
            }
        });

        // EXIT BUTTON
        Button exit = new Button("Exit");
        exit.setMinSize(ButtonWidth, ButtonHeight);
        exit.setStyle(ButtonStyle);
        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Platform.exit();
                System.exit(0);
            }
        });

        selectTerrain.setBackground(ButtonBackground);
        createTerrain.setBackground(ButtonBackground);
        exit.setBackground(ButtonBackground);

        getChildren().addAll(selectTerrain, createTerrain, exit);
        setAlignment(Pos.CENTER);
        setSpacing(MenuSpacing);
        setBackground(MenuBackground);
        setMinWidth(MenuWidth);
        setMinHeight(MenuHeight);

        Scene scene = new Scene(this);
        Main.primaryStage.setTitle("Multi Agent Surveillance");
        Main.primaryStage.setScene(scene);
        Main.primaryStage.show();
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        Main.primaryStage.setX((primScreenBounds.getWidth() - Main.primaryStage.getWidth()) / 2);
        Main.primaryStage.setY((primScreenBounds.getHeight() - Main.primaryStage.getHeight()) / 2);
    }
}
