package UI.menu;

import UI.data.Reader;
import UI.objects.UITerrain;
import core.Core;
import core.GuardAI;
import core.IntruderAI;
import core.Main;
import java.util.Arrays;
import java.util.Optional;
import javafx.animation.Animation.Status;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * Create a menu used for managing simulations
 */
public class SimulationMenu extends Pane{
    //menu style and size variables
    private Background MenuBackground, ButtonBackground;
    private String ButtonStyle;
    private int ButtonWidth, ButtonHeight, MenuWidth, MenuSpacing;
    //value used for creating an initial menu from a set size
    private final int PaneWidth = 900;
    private final int heightAdjustment = 47;
    private final int widthAdjustment = 18;
    private ChangeListener heightListener, widthListener;
    
    //variables used in simulation menu
    private UITerrain terrain;
    private VBox options;
    private Label status, guards, intruders, intrudersUncaptured;
    private TextField intruderText, guardText;
    private ChoiceBox<GuardAI> guardAI;
    private ChoiceBox<IntruderAI> intruderAI;
    private Button run;
    
    private Core core;
    
    /**
     * Constructor
     * @param map 2D matrix representing world
     */
    public SimulationMenu(int[][] map, boolean t){
        core = new Core(map, this);
        loadVariables();
        loadMenu(t);
    }
    
    /**
     * Load variables used for creating the menu
     */
    private void loadVariables(){
        MenuBackground = new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonBackground = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonStyle = "-fx-font: 22 arial;";
        ButtonWidth = 220;
        ButtonHeight = 50;
        MenuSpacing = 20;
        MenuWidth = ButtonWidth + 30;
    }
    
    /**
     * Method creates the pane with the simulation options and graphic representation of the world
     */
    private void loadMenu(boolean t){
        setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        
        terrain = core.createTerrain(PaneWidth, t);
        getChildren().add(terrain);
        options = new VBox();
        
        resizeOptions();
           
        //START
        Button create = new Button("Create");
        create.setMinSize(ButtonWidth, ButtonHeight);
        create.setStyle(ButtonStyle);
        create.setBackground(ButtonBackground);
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                create();
            }
        });

        //PAUSE
        run = new Button("Start");
        run.setMinSize(ButtonWidth, ButtonHeight);
        run.setStyle(ButtonStyle);
        run.setBackground(ButtonBackground);
        run.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                run();
            }
        });
        
        //RESTART
        Button restart = new Button("Restart");
        restart.setMinSize(ButtonWidth, ButtonHeight);
        restart.setStyle(ButtonStyle);
        restart.setBackground(ButtonBackground);
        restart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                restart();
            }
        });

        //BACK
        Button back = new Button("Back");
        back.setMinSize(ButtonWidth,ButtonHeight);
        back.setStyle(ButtonStyle);
        back.setBackground(ButtonBackground);
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                back();
            }
        });
        
        CheckBox sound = new CheckBox("Sound");
        sound.setFont(new Font("Arial", 17));
        sound.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sound(sound.isSelected());
            }
        });

        options.getChildren().addAll(setLegend(), run, restart, create, setSimulationOptions(), back, sound);
        options.setSpacing(MenuSpacing);
        options.setPadding(new Insets(30, 15, 30, 15));
        options.setBackground(MenuBackground);
        options.setMaxWidth(MenuWidth);
        options.setAlignment(Pos.CENTER);
        getChildren().add(options);
        
        Scene scene = new Scene(this, PaneWidth + MenuWidth, PaneWidth);
        setControlls(scene, terrain, options);
        Main.primaryStage.setScene(scene);
        Main.primaryStage.sizeToScene();
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        Main.primaryStage.setX((primScreenBounds.getWidth() - Main.primaryStage.getWidth()) / 2);
        Main.primaryStage.setY((primScreenBounds.getHeight() - Main.primaryStage.getHeight()) / 2);
    }
    
    /**
     * Control sound display
     * @param sound boolean display sound, don't display sound
     */
    private void sound(boolean sound){
        terrain.setDisplayingNoise(sound);
    }
    
    /**
     * Return to levelSelectionMenu
     */
    private void back(){
        Main.primaryStage.heightProperty().removeListener(heightListener);
        Main.primaryStage.widthProperty().removeListener(widthListener);
        core.clearWorld();
        Main.loadTerrainSelectionMenu();
    }
    
    /**
     * Start simulation
     */
    private void create(){
        core.startSimulation(guardAI.getValue(), intruderAI.getValue(), Integer.parseInt(guardText.getText()), Integer.parseInt(intruderText.getText()));
    }
    
    /**
     * Pause simulation
     */
    private void run(){
        core.pauseSimulation();
    }
    
    /**
     * Restart simulation
     */
    private void restart(){
        core.restartSimulation();
    }
    
    /**
     * If UI is resized, resizes world too
     */
    private void resizeOptions(){
        options.translateXProperty().bind(terrain.getXPosition().add(terrain.getWidth()));
        
        heightListener = (ChangeListener<Number>) (obs, oldVal, newVal) -> 
        {
            terrain.resizeHeight(newVal.intValue() - heightAdjustment);
            options.setTranslateY(newVal.doubleValue()/2.0 - options.getHeight()/2.0);
        };
        widthListener = (ChangeListener<Number>) (obs, oldVal, newVal) -> 
        {
            terrain.resizeWidth(newVal.intValue() - MenuWidth - widthAdjustment);
        };
        
        
        Main.primaryStage.heightProperty().addListener(heightListener);
        Main.primaryStage.widthProperty().addListener(widthListener);
    }
    
    /**
     * Sets legend displaying simulation details
     * @return VBox containing legend
     */
    private VBox setLegend(){
        VBox box = new VBox();
        int spacing = 10;
        Background background = new Background(new BackgroundFill(Color.rgb(220, 220, 220), CornerRadii.EMPTY, Insets.EMPTY));
        Insets padding = new Insets(15, 15, 15, 15);
        
        status = new Label("Simulation: ");
        status.setFont(new Font("Arial", 18));
        status.setTextFill(Color.DARKBLUE);
        
        guards = new Label("Guards: ");
        guards.setFont(new Font("Arial", 17));
        
        intruders = new Label("Intruders: ");
        intruders.setFont(new Font("Arial", 17));
        
        intrudersUncaptured = new Label("Uncaptured intruders: ");
        intrudersUncaptured.setFont(new Font("Arial", 17));
        
        box.getChildren().addAll(status, guards, intruders, intrudersUncaptured);
        box.setSpacing(spacing);
        box.setBackground(background);
        box.setPadding(padding);
        box.setAlignment(Pos.CENTER);
        
        return box;
    }
    
    public void updateLegend(int guardsCount, int intrudersCount, int intrudersUncapturedCount){
        guards.setText("Guards: " + guardsCount);
        intruders.setText("Intruders: " + intrudersCount);
        intrudersUncaptured.setText("Uncaptured intruders: " + intrudersUncapturedCount);
    }
    
    public void updateTimelineStatus(Status timelineStatus){
        switch (timelineStatus) {
            case PAUSED:
                run.setText("Start");
                break;
            case RUNNING:
                run.setText("Pause");
                break;
            case STOPPED:
                create();
                run.setText("Start");
                break;
        }
        status.setText("Simulation: " + timelineStatus.toString());
    }
    
    public void setAI(GuardAI guard, IntruderAI intruder, int guardCount, int intruderCount){
        guardAI.setValue(guard);
        intruderAI.setValue(intruder);
        guardText.setText(guardCount + "");
        intruderText.setText(intruderCount  + "");
    }
    
    public void simulationFinished(String winner){
        core.clearWorld();
        //System.out.println(core.timeline.getStatus());
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Simulation has finished!");
        alert.setHeaderText(winner);

        ButtonType restart = new ButtonType("Restart");
        ButtonType terrrainSelection = new ButtonType("Back to terrain selection");
        ButtonType exit = new ButtonType("Exit");

        alert.getButtonTypes().setAll(restart, terrrainSelection, exit);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == restart){
            restart();
            run();
        } 
        else if (result.get() == terrrainSelection) {
            back();
        }
        else{
            Platform.exit();
            System.exit(0);
        }
    }
    
    /**
     * Adds options for simulation control
     * @return VBox containing the options
     */
    private VBox setSimulationOptions(){
        VBox box = new VBox();
        int spacing = 10;
        Background background = new Background(new BackgroundFill(Color.rgb(220, 220, 220), CornerRadii.EMPTY, Insets.EMPTY));
        Insets padding = new Insets(15, 15, 15, 15);
        int choiceBoxSizeAdjustment = Math.toIntExact(Math.round(padding.getLeft() + padding.getRight()));
        
        Label guardsLabel = new Label("Set number of guards:");
        guardsLabel.setFont(new Font("Arial", 17));
        
        guardText = new TextField(Core.defaultGuardCount + "");
        
        Label intruderLabel = new Label("Set number of intruders:");
        intruderLabel.setFont(new Font("Arial", 17));
        
        intruderText = new TextField(Core.defaultIntruderCount + "");
        
        Label guardsChoiceBox = new Label("Guards AI:");
        guardsChoiceBox.setFont(new Font("Arial", 17));
        
        guardAI = new ChoiceBox();
        guardAI.getItems().setAll(Arrays.asList(GuardAI.values()));
        guardAI.setMinSize(ButtonWidth - choiceBoxSizeAdjustment, ButtonHeight - choiceBoxSizeAdjustment/2);
        /*guardAI.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
            public void changed(ObservableValue ov, Number value, Number new_value){
                Core.Guard = Core.GuardAI[new_value.intValue()];
            }
        });*/
        guardAI.setValue(Core.defaultGuard);
        
        Label intruderChoiceBox = new Label("Intruders AI:");
        intruderChoiceBox.setFont(new Font("Arial", 17));
        
        intruderAI = new ChoiceBox<IntruderAI>();
        intruderAI.getItems().setAll(Arrays.asList(IntruderAI.values()));
        intruderAI.setMinSize(ButtonWidth - choiceBoxSizeAdjustment, ButtonHeight - choiceBoxSizeAdjustment/2);
        /*intruderAI.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
            public void changed(ObservableValue ov, Number value, Number new_value){
                Core.Intruder = Core.IntruderAI[new_value.intValue()];
            }
        });*/
        intruderAI.setValue(Core.defaultIntruder);
        
        box.getChildren().addAll(guardsLabel, guardText, intruderLabel, intruderText, guardsChoiceBox, guardAI, intruderChoiceBox, intruderAI);
        box.setSpacing(spacing);
        box.setBackground(background);
        box.setPadding(padding);
        box.setAlignment(Pos.CENTER);
        
        return box;
    }
    
    private static void setControlls(Scene scene, UITerrain t, VBox options){
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case UP: t.moveUp(); t.requestFocus(); break;
                    case DOWN: t.moveDown(); t.requestFocus(); break;
                    case LEFT: t.moveLeft(); t.requestFocus(); break;
                    case RIGHT: t.moveRight(); t.requestFocus(); break;
                    case Z: t.getChildren().clear(); t.squareSize++; t.drawTerrain(); break;
                    case X: t.getChildren().clear(); t.squareSize--; t.drawTerrain(); break;
                    case D: t.returnToDefault(); break;
                    case P: System.out.println(Main.primaryStage.getWidth() + " " + Main.primaryStage.getHeight()); break;
                    case O: System.out.println(options.getTranslateX()); System.out.println(t.getXPosition()); System.out.println(t.getWidth()); System.out.println(t.getTranslateX()); break;
                }
            }
        });
    }
}
