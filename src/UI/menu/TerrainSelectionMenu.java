package UI.menu;

import UI.data.TerrainData;
import UI.data.Reader;
import UI.data.CellType;
import core.Main;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * Create a menu used to view and select from existing terrains
 */
public class TerrainSelectionMenu extends VBox{
    //menu style and size variables
    private Background MenuBackground, ButtonBackground;
    private String ButtonStyle, BorderStyle, ScrollPaneStyle;
    private int ButtonWidth, ButtonHeight, MenuWidth, MenuHeight, MenuSpacing;
    private RadioButton textures;
    
    //available terrains to choose from
    private ArrayList<TerrainData> terrains;
    
    /**
     * Constructor
     */
    public TerrainSelectionMenu(){
        loadVariables();
        loadMenu();
    }
    
    /**
     * Load variables used for creating the menu
     */
    private void loadVariables(){
        MenuBackground = new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonBackground = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
        ButtonStyle = "-fx-font: 22 arial;";
        BorderStyle = "-fx-border-color: black;\n" +
                      "-fx-border-style: solid inside;" +
                   "-fx-border-insets: 5;\n" +
                   "-fx-border-width: 3;\n";
        ScrollPaneStyle = "-fx-background: black; -fx-background-color: black;";
        ButtonWidth = 220;
        ButtonHeight = 50;
        MenuSpacing = 10;
    }
    
    private void loadMenu(){
        
        VBox box = setDataForScrollPane();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(box);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setBackground(MenuBackground);
        scrollPane.setStyle(ScrollPaneStyle);
        
        HBox radioButtons = new HBox();
        RadioButton simpleColors = new RadioButton("Load with colors");
        simpleColors.setFont(new Font("Arial", 17));
        textures = new RadioButton("Load with textures");
        textures.setFont(new Font("Arial", 17));
        textures.setSelected(true);
        radioButtons.setAlignment(Pos.CENTER);
        
        ToggleGroup t = new ToggleGroup();
        simpleColors.setToggleGroup(t);
        textures.setToggleGroup(t);
        radioButtons.getChildren().addAll(simpleColors, textures);
        radioButtons.setSpacing(20);
        
        //BACK
        Button back = new Button("Back to main menu");
        back.setMinSize(ButtonWidth,ButtonHeight);
        back.setStyle(ButtonStyle);
        back.setBackground(ButtonBackground);
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Main.loadMainMenu();
            }
        });
        
        getChildren().add(scrollPane);
        getChildren().add(radioButtons);
        getChildren().add(back);
        
        Insets padding = new Insets(20, 20, 20, 20);
        setSpacing(MenuSpacing);
        setAlignment(Pos.CENTER);
        setBackground(MenuBackground);
        setPadding(padding);
        
        Scene scene = new Scene(this);
        Main.primaryStage.setScene(scene);
        Main.primaryStage.sizeToScene();
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        Main.primaryStage.setX((primScreenBounds.getWidth() - Main.primaryStage.getWidth()) / 2);
        Main.primaryStage.setY((primScreenBounds.getHeight() - Main.primaryStage.getHeight()) / 2);
    }
    
    private VBox setDataForScrollPane(){
        VBox pane = new VBox();
        int spacing = 10;
        int minWidth = 350;
        int prefHeight = 400;
        
        terrains = Reader.readTerrains();
        for(int i = 0; i < terrains.size(); i++)
        {
            //BUTTON
            Button b = new Button(terrains.get(i).name);
            b.setStyle(ButtonStyle);
            b.setMinSize(ButtonWidth, ButtonHeight);
            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    for(TerrainData m: terrains){
                        if(m.name.equals(b.getText())){
                            Main.loadSimulationMenu(m.map, textures.isSelected());
                            break;
                        }
                    }
                }
            });
            
            pane.getChildren().add(b);
            
            HBox box = new HBox();
            box.setSpacing(5);
            
            Label size = new Label(terrains.get(i).map.length + " X " + terrains.get(i).map[0].length + " meters");
            size.setFont(new Font("Arial", 17));
            size.setStyle(BorderStyle);
            box.getChildren().add(size);
            
            Label guardTowers = new Label("Towers: " + countObjects(terrains.get(i).map, CellType.getCellType(CellType.CELLTYPE.Tower)));
            guardTowers.setFont(new Font("Arial", 17));
            guardTowers.setStyle(BorderStyle);
            box.getChildren().add(guardTowers);
            
            Label walls = new Label("Wall count: " + countObjects(terrains.get(i).map, CellType.getCellType(CellType.CELLTYPE.Wall)));
            walls.setFont(new Font("Arial", 17));
            walls.setStyle(BorderStyle);
            box.getChildren().add(walls);
            
            pane.getChildren().add(box);
            box.setAlignment(Pos.CENTER);
        }
        
        pane.setAlignment(Pos.CENTER);
        pane.setSpacing(spacing);
        pane.setMinWidth(minWidth);
        pane.setPrefHeight(prefHeight);
        pane.setBackground(MenuBackground);
        return pane;
    }
    
    private int countObjects(int[][] matrix, int index){
        int objects = 0;
        for(int x = 0; x < matrix.length; x++)
        {
            for(int y = 0; y < matrix[0].length; y++)
            {
                if(matrix[x][y] == index)
                {
                    objects++;
                }
            }
        }
        return objects;
    }
}
