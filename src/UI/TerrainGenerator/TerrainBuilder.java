package UI.TerrainGenerator;

import UI.data.Reader;
import UI.data.CellType;
import core.Main;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Stack;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * UI used for terrain creation
 */
public class TerrainBuilder{
    //view variables
    private Pane terrainBuilding;
    private Group squaresBetweenLines;
    private int terrainSize;
    
    private HBox main;
    private ScrollPane shapeScrollPane;
    private boolean creatingShape;
    private TextField size, precision;
    private Background Background, MenuBackground, ButtonBackground;
    private static String ButtonStyle;
    private static int ButtonWidth, ButtonHeight;
    
    private Label Selected;
    private CellType selectedType;
    private CellType Door, Window, Wall, Road, Tower, LowVisibility, Target;
    private int[] types;
    
    private int SIZE;
    private CellType[][] map;
    private Stack<Change> changes = new Stack<>();
    private CellType[][] MAP;
    private Stack<Change> MapChanges = new Stack<>();
    private CellType[][] SHAPE;
    private Stack<Change> ShapeChanges = new Stack<>();
    private boolean saved;
    
    private Shape selectedShape;
    private boolean drawingShapeInMap;
    private boolean randomMenu = false;

    /**
     * Constructor
     */
    public TerrainBuilder(){
        setVariables();
        terrainSize = 900;
        Rectangle clip = new Rectangle(terrainSize, terrainSize);
        terrainBuilding = new Pane();
        terrainBuilding.setPrefWidth(terrainSize);
        terrainBuilding.setPrefHeight(terrainSize);
        terrainBuilding.setClip(clip);
        
        main = new HBox();
        int spacing = 10;
        Insets padding = new Insets(20, 40, 20, 40);
        
        VBox options = getOptions();
        main.getChildren().addAll(terrainBuilding, options);
        
        main.setAlignment(Pos.TOP_CENTER);
        main.setSpacing(spacing);
        main.setPadding(padding);
        main.setBackground(MenuBackground);
        
        Scene scene = new Scene(main);
        setControls(scene);
        Main.primaryStage.setScene(scene);
        Main.primaryStage.sizeToScene();
        
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        Main.primaryStage.setX((primScreenBounds.getWidth() - Main.primaryStage.getWidth()) / 2);
        Main.primaryStage.setY((primScreenBounds.getHeight() - Main.primaryStage.getHeight()) / 2);
    }
    
    
    /**
     * Adds controls to the scene
     * @param scene scene to which the controls should be added
     */
    private void setControls(Scene scene){
        KeyCombination keyCombination = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(keyCombination.match(event))
                {
                    if(!changes.isEmpty())
                    {
                        saved = false;
                        Change c = changes.pop();
                        terrainBuilding.getChildren().remove(map[c.Y][c.X].rec);
                        map[c.Y][c.X] = c.previousType;
                        c.previousType.createType(SIZE);
                        c.previousType.rec.setTranslateX(c.X*SIZE);
                        c.previousType.rec.setTranslateY(c.Y*SIZE);
                        terrainBuilding.getChildren().add(c.previousType.rec);
                        c.previousType.rec.toBack();
                    }
                }
                else{
                    if(drawingShapeInMap){
                        drawingShapeInMap = false;
                        int XCor = (int)(selectedShape.g.getTranslateX()/SIZE);
                        int YCor = (int)(selectedShape.g.getTranslateY()/SIZE);
                        terrainBuilding.getChildren().remove(selectedShape.g);
                        for(int x = 0; x < selectedShape.map.length; x++)
                        {
                            for(int y = 0; y < selectedShape.map[0].length; y++)
                            {
                                if(selectedShape.map[x][y] != map[YCor + x][XCor + y].index){
                                    CellType t = new CellType(selectedShape.map[x][y]);
                                    terrainBuilding.getChildren().remove(map[YCor + x][XCor + y].rec);
                                    changes.add(new Change(XCor + y, YCor + x, map[YCor + x][XCor + y]));
                                    map[YCor + x][XCor + y] = t;
                                    t.createType(SIZE);
                                    t.rec.setTranslateX((XCor + y)*SIZE);
                                    t.rec.setTranslateY((YCor + x)*SIZE);
                                    terrainBuilding.getChildren().add(t.rec);
                                    t.rec.toBack();
                                }
                            }
                        }
                    }
                    switch (event.getCode()) {
                        case Q: selectedType = Door.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case W: selectedType = Window.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case E: selectedType = Wall.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case R: selectedType = Road.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case T: selectedType = Tower.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case Y: selectedType = LowVisibility.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case U: selectedType = Target.cloneType(); Selected.setText(selectedType.TYPE + " is selected"); Selected.setTextFill(selectedType.c); break;
                        case UP: terrainBuilding.setTranslateY(terrainBuilding.getTranslateY() - getAdjustment()); terrainBuilding.getClip().setTranslateY(terrainBuilding.getClip().getTranslateY() + getAdjustment()); 
                                terrainBuilding.requestFocus(); size.setFocusTraversable(false); precision.setFocusTraversable(false); break;
                        case DOWN: terrainBuilding.setTranslateY(terrainBuilding.getTranslateY() + getAdjustment()); terrainBuilding.getClip().setTranslateY(terrainBuilding.getClip().getTranslateY() - getAdjustment()); 
                                terrainBuilding.requestFocus(); size.setFocusTraversable(false); precision.setFocusTraversable(false); break;
                        case LEFT: terrainBuilding.setTranslateX(terrainBuilding.getTranslateX() - getAdjustment()); terrainBuilding.getClip().setTranslateX(terrainBuilding.getClip().getTranslateX() + getAdjustment()); 
                                terrainBuilding.requestFocus(); size.setFocusTraversable(false); precision.setFocusTraversable(false); break;
                        case RIGHT: terrainBuilding.setTranslateX(terrainBuilding.getTranslateX() + getAdjustment()); terrainBuilding.getClip().setTranslateX(terrainBuilding.getClip().getTranslateX() - getAdjustment()); 
                                terrainBuilding.requestFocus(); size.setFocusTraversable(false); precision.setFocusTraversable(false); break;
                        case Z: terrainBuilding.getChildren().clear(); SIZE++; drawSquares(); drawMap(); break;
                        case X: terrainBuilding.getChildren().clear(); SIZE--; drawSquares(); drawMap(); break;
                    }
                }
            }
        });
        terrainBuilding.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saved = false;
                terrainBuilding.requestFocus();
                if(SIZE != 0){
                    int XCor = (int)(event.getX())/SIZE;
                    int YCor = (int)(event.getY())/SIZE;
                    if(YCor < map.length && XCor < map[0].length)
                    {
                        if(drawingShapeInMap)
                        {
                            if(!terrainBuilding.getChildren().contains(selectedShape.g))
                            {
                                terrainBuilding.getChildren().add(selectedShape.g);
                                squaresBetweenLines.toFront();
                            }
                            selectedShape.g.setTranslateX(XCor*SIZE - (selectedShape.map[0].length/2)*SIZE);
                            selectedShape.g.setTranslateY(YCor*SIZE - (selectedShape.map.length/2)*SIZE);
                        }
                        else if(map[YCor][XCor].index != selectedType.index){
                            CellType t = selectedType.cloneType();
                            terrainBuilding.getChildren().remove(map[YCor][XCor].rec);
                            changes.add(new Change(XCor, YCor, map[YCor][XCor]));
                            map[YCor][XCor] = t;
                            t.createType(SIZE);
                            t.rec.setTranslateX(XCor*SIZE);
                            t.rec.setTranslateY(YCor*SIZE);
                            terrainBuilding.getChildren().add(t.rec);
                            t.rec.toBack();
                        }
                    }
                }
            }
        });
        terrainBuilding.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                saved = false;
                terrainBuilding.requestFocus();
                if(SIZE != 0){
                    int XCor = (int)(event.getX())/SIZE;
                    int YCor = (int)(event.getY())/SIZE;
                    if(YCor < map.length && XCor < map[0].length)
                    {
                        if(drawingShapeInMap)
                        {
                            if(!terrainBuilding.getChildren().contains(selectedShape.g))
                            {
                                terrainBuilding.getChildren().add(selectedShape.g);
                                squaresBetweenLines.toFront();
                            }
                            selectedShape.g.setTranslateX(XCor*SIZE - (selectedShape.map[0].length/2)*SIZE);
                            selectedShape.g.setTranslateY(YCor*SIZE - (selectedShape.map.length/2)*SIZE);
                        }
                        else if(map[YCor][XCor].index != selectedType.index){
                            CellType t = selectedType.cloneType();
                            terrainBuilding.getChildren().remove(map[YCor][XCor].rec);
                            changes.add(new Change(XCor, YCor, map[YCor][XCor]));
                            map[YCor][XCor] = t;
                            t.createType(SIZE);
                            t.rec.setTranslateX(XCor*SIZE);
                            t.rec.setTranslateY(YCor*SIZE);
                            terrainBuilding.getChildren().add(t.rec);
                            t.rec.toBack();
                        }
                    }
                }
            }
        });
    }
   
    /**
     * Load terrain
     * @param Squares square size
     * @param Meters size of the whole terrain in meters
     */
    private void loadGrid(double Squares ,int Meters){
        if(creatingShape && (SHAPE == null || SHAPE[0][0] == null)){
            map = new CellType[Math.toIntExact(Math.round(Squares*Meters))][Math.toIntExact(Math.round(Squares*Meters))];

            drawSquares();
            drawDefault();
        }
        else if(!creatingShape && (MAP == null || MAP[0][0] == null)){
            map = new CellType[Math.toIntExact(Math.round(Squares*Meters))][Math.toIntExact(Math.round(Squares*Meters))];
            
            drawSquares();
            drawDefault();
        }
        else if(creatingShape){
            map = cloneMap(SHAPE);
            changes = cloneChanges(ShapeChanges);
            SHAPE = null;
            ShapeChanges = null;
            drawSquares();
            drawMap();
        }
        else if(!creatingShape){
            map = cloneMap(MAP);
            changes = cloneChanges(MapChanges);
            MAP = null;
            MapChanges = null;
            drawSquares();
            drawMap();
        }
    }
    
    /**
     * Create options view
     * @return VBox containing options
     */
    private VBox getOptions(){
        VBox box = new VBox();
        
        Selected = new Label(selectedType.TYPE + " is selected");
        Selected.setFont(new Font("Arial", 18));
        Selected.setTextFill(selectedType.c);
        
        Label instructions = new Label("Legend:");
        instructions.setFont(new Font("Arial", 18));
        
        Label setSize = new Label("Set size in meters:");
        setSize.setFont(new Font("Arial", 18));
        size = new TextField();
        size.setText("20");
        
        Label setPrecision1 = new Label("Set precision:");
        setPrecision1.setFont(new Font("Arial", 18));
        Label setPrecision2 = new Label("1 meter = ? squares");
        setPrecision2.setFont(new Font("Arial", 18));
        
        precision = new TextField();
        precision.setText("1");
        
        Button save = new Button("Save");
        save.setBackground(ButtonBackground);
        save.setStyle(ButtonStyle);
        save.setMinSize(ButtonWidth, ButtonHeight);
        Button back = new Button("Back");
        back.setBackground(ButtonBackground);
        back.setStyle(ButtonStyle);
        back.setMinSize(ButtonWidth, ButtonHeight);
        Button create = new Button("Create shape");
        create.setBackground(ButtonBackground);
        create.setStyle(ButtonStyle);
        create.setMinSize(ButtonWidth, ButtonHeight);
        creatingShape = false;
        Button open = new Button("Shape list");
        open.setBackground(ButtonBackground);
        open.setStyle(ButtonStyle);
        open.setMinSize(ButtonWidth, ButtonHeight);
        Button load = new Button("Load grid");
        load.setBackground(ButtonBackground);
        load.setStyle(ButtonStyle);
        load.setMinSize(ButtonWidth, ButtonHeight);
        Button random = new Button("Random");
        random.setBackground(ButtonBackground);
        random.setStyle(ButtonStyle);
        random.setMinSize(ButtonWidth, ButtonHeight);
        
        GridPane legend = createLegend();
        
        save.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if(map!=null){
                    if(creatingShape){
                        saved = true;
                        terrainBuilding.getChildren().clear();
                        Reader.saveShape(convertMap());
                        size.setText("20");
                        precision.setText("1");
                        creatingShape = false;
                        if(map != null && map[0][0] != null)
                        {
                            SHAPE = cloneMap(map);
                            ShapeChanges = cloneChanges(changes);
                        }
                        load.fire();
                    }
                    else{
                        TextInputDialog dialog = new TextInputDialog("name");
                        dialog.setTitle("Terrain name");
                        dialog.setContentText("Please enter name for your terrain:");

                        Optional<String> name = dialog.showAndWait();
                        if (name.isPresent()){
                            saved = true;
                            Reader.saveTerrain(convertMap(), name.get());
                            Main.loadMainMenu();
                        }
                    }
                }
            }
        });
        back.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if(!saved){
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("You are about to exit without saving!");

                    ButtonType buttonSave = new ButtonType("Save and exit");
                    ButtonType buttonExit = new ButtonType("Exit without saving");
                    ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

                    alert.getButtonTypes().setAll(buttonSave, buttonExit, buttonCancel);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == buttonSave){
                        if(creatingShape){
                            terrainBuilding.getChildren().clear();
                            if(map!=null){
                                Reader.saveShape(convertMap());
                            }
                            size.setText("20");
                            precision.setText("1");
                            creatingShape = false;
                            if(map != null && map[0][0] != null)
                            {
                                SHAPE = cloneMap(map);
                                ShapeChanges = cloneChanges(changes);
                            }
                            load.fire();
                        }
                        else{
                            TextInputDialog dialog = new TextInputDialog("name");
                            dialog.setTitle("Terrain name");
                            dialog.setContentText("Please enter name for your terrain:");

                            Optional<String> name = dialog.showAndWait();
                            if (name.isPresent()){
                                if(map!=null){
                                    Reader.saveTerrain(convertMap(), name.get());
                                }
                                Main.loadMainMenu();
                            }
                        }
                    } 
                    else if (result.get() == buttonExit) {
                        if(creatingShape){
                            terrainBuilding.getChildren().clear();
                            size.setText("20");
                            precision.setText("1");
                            creatingShape = false;
                            if(map != null && map[0][0] != null)
                            {
                                SHAPE = cloneMap(map);
                                ShapeChanges = cloneChanges(changes);
                            }
                            load.fire();
                        }
                        else{
                            Main.loadMainMenu();
                        }
                    }
                }
                else{
                    if(creatingShape){
                        terrainBuilding.getChildren().clear();
                        size.setText("20");
                        precision.setText("1");
                        creatingShape = false;
                        if(map != null && map[0][0] != null)
                        {
                            SHAPE = cloneMap(map);
                            ShapeChanges = cloneChanges(changes);
                        }
                        load.fire();
                    }
                    else{
                        Main.loadMainMenu();
                    }
                }
            }
        });
        create.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                terrainBuilding.getChildren().clear();
                size.setText("5");
                precision.setText("1");
                creatingShape = true;
                if(map != null && map[0][0] != null)
                {
                    MAP = cloneMap(map);
                    MapChanges = cloneChanges(changes);
                }
                load.fire();
                box.getChildren().remove(create);
                box.getChildren().remove(open);
                if(box.getChildren().contains(shapeScrollPane))
                {
                    box.getChildren().remove(shapeScrollPane);
                }
                if(box.getChildren().contains(random))
                {
                    box.getChildren().remove(random);
                }
            }
        });
        shapeScrollPane = null;
        open.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if(shapeScrollPane == null)
                {
                    shapeScrollPane = getShapes();
                    box.getChildren().add(shapeScrollPane);
                }
                else if(!box.getChildren().contains(shapeScrollPane))
                {
                    shapeScrollPane = getShapes();
                    box.getChildren().add(shapeScrollPane);
                }
                else{
                    shapeScrollPane = getShapes();
                }
            }
        });
        load.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                terrainBuilding.getChildren().clear();
                int Precision = Integer.parseInt(precision.getText());
                int Meters = Integer.parseInt(size.getText());
                SIZE = (int)(terrainSize)/(Meters*Precision);
                if(SIZE < 15)
                {
                    SIZE = 15;
                }
                if(SIZE > 45)
                {
                    SIZE = 45;
                }
                loadGrid(Precision, Meters);
                if(!creatingShape){
                    if(!box.getChildren().contains(create))
                    {
                        box.getChildren().add(box.getChildren().indexOf(save), create);
                    }
                    if(!box.getChildren().contains(open))
                    {
                        box.getChildren().add(box.getChildren().indexOf(save), open);
                    }
                    if(!box.getChildren().contains(random))
                    {
                        box.getChildren().add(box.getChildren().indexOf(load) + 1, random);
                    }
                }
            }
        });
        random.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                if(randomMenu){
                    random.setText("Random");
                    box.getChildren().clear();
                    box.getChildren().addAll(Selected, instructions, legend, setSize, size, setPrecision1, setPrecision2, precision, load, random, create, save, back);
                }
                else{
                    random.setText("Back");
                    box.getChildren().clear();
                    box.getChildren().add(createRandomMenu());
                    box.getChildren().add(random);
                }
                randomMenu = !randomMenu;
            }
        });
        
        box.getChildren().addAll(Selected, instructions, legend, setSize, size, setPrecision1, setPrecision2, precision, load, create, save, back);
        box.setAlignment(Pos.TOP_CENTER);
        box.setSpacing(5);
        box.setPadding(new Insets(20, 40, 20, 40));
        //box.setBackground(MenuBackground);
        //root.setMinWidth(250);
        
        return box;
    }
    
    /**
     * Menu for random terrain creation
     * @return VBox with menu
     */
    private VBox createRandomMenu(){
        VBox box = new VBox();
        
        Label Tower = new Label("Generate towers:");
        Tower.setFont(new Font("Arial", 17));
        
        CheckBox tower = new CheckBox();
        tower.setSelected(false);
        
        HBox box1 = new HBox();
        box1.setSpacing(10);
        box1.setAlignment(Pos.CENTER);
        box1.getChildren().addAll(Tower, tower);
        
        Label Target = new Label("Generate targets:");
        Target.setFont(new Font("Arial", 18));
        
        CheckBox target = new CheckBox();
        tower.setSelected(false);

        HBox box2 = new HBox();
        box2.setSpacing(10);
        box2.setAlignment(Pos.CENTER);
        box2.getChildren().addAll(Target, target);
        
        Label street = new Label("Space between walls:");
        street.setFont(new Font("Arial", 18));
        
        TextField Street = new TextField();
        Street.setText("1");
        
        Button house = new Button("Create houses");
        house.setBackground(ButtonBackground);
        house.setStyle(ButtonStyle);
        house.setMinSize(ButtonWidth, ButtonHeight);
        
        house.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                drawDefault();
                boolean to = tower.isSelected();
                boolean ta = target.isSelected();
                String s = Street.getText();
                RandomMap r = new RandomMap(false, to, ta, Integer.parseInt(s), map.length, map.length);
                drawMap(r.map);
            }
        });
        
        Button maze = new Button("Create maze");
        maze.setBackground(ButtonBackground);
        maze.setStyle(ButtonStyle);
        maze.setMinSize(ButtonWidth, ButtonHeight);
        
        maze.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                drawDefault();
                boolean to = tower.isSelected();
                boolean ta = target.isSelected();
                String s = Street.getText();
                RandomMap r = new RandomMap(true, to, ta, Integer.parseInt(s), map.length, map.length);
                drawMap(r.map);
            }
        });
        
        box.getChildren().addAll(box1, box2, street, Street, house, maze);
        box.setAlignment(Pos.TOP_CENTER);
        box.setSpacing(15);
        return box;
    }
    
    /**
     * Create legend with cell types
     * @return GridPane with legend
     */
    private GridPane createLegend(){
        GridPane pane = new GridPane();
        for(int i = 0; i < types.length; i++)
        {
            CellType t = new CellType(types[i]);
            t.createType(15);
            Label l = new Label(t.TYPE + " - " + t.keyCode);
            l.setFont(new Font("Arial", 15));
            l.setTextFill(t.c);
            pane.add(t.rec, 0, i);
            pane.add(l, 1, i);
            pane.setHalignment(t.rec, HPos.CENTER);
            pane.setHalignment(l, HPos.LEFT);
        }
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setAlignment(Pos.CENTER);
        return pane;
    }
    
    /**
     * Draw a default terrain
     */
    private void drawDefault(){
        try{
            if(this.map[0][0].rec != null){
                for(int x = 0; x < map.length; x++)
                {
                    for(int y = 0; y < map[0].length; y++)
                    {
                        terrainBuilding.getChildren().remove(this.map[x][y].rec);
                    }
                }
            }
        }
        catch(NullPointerException e)
        {
        }
        for(int x = 0; x < map.length; x++)
        {
            CellType t1 = Wall.cloneType();
            map[0][x] = t1;
            t1.createType(SIZE);
            t1.rec.setTranslateX(x*SIZE);
            t1.rec.setTranslateY(0*SIZE);
            terrainBuilding.getChildren().add(t1.rec);
            t1.rec.toBack();
            
            CellType t2 = Wall.cloneType();
            map[map[0].length - 1][x] = t2;
            t2.createType(SIZE);
            t2.rec.setTranslateX(x*SIZE);
            t2.rec.setTranslateY((map[0].length - 1)*SIZE);
            terrainBuilding.getChildren().add(t2.rec);
            t2.rec.toBack();
        }
        for(int y = 1; y < map.length - 1; y++)
        {
            CellType t1 = Wall.cloneType();
            map[y][0] = t1;
            t1.createType(SIZE);
            t1.rec.setTranslateX(0*SIZE);
            t1.rec.setTranslateY(y*SIZE);
            terrainBuilding.getChildren().add(t1.rec);
            t1.rec.toBack();
            
            CellType t2 = Wall.cloneType();
            map[y][map.length - 1] = t2;
            t2.createType(SIZE);
            t2.rec.setTranslateX((map.length - 1)*SIZE);
            t2.rec.setTranslateY(y*SIZE);
            terrainBuilding.getChildren().add(t2.rec);
            t2.rec.toBack();
        }
        
        for(int x = 1; x < map.length - 1; x++)
        {
            for(int y = 1; y < map[0].length - 1; y++)
            {
                CellType r = Road.cloneType();
                map[y][x] = r;
                r.createType(SIZE);
                r.rec.setTranslateX(x*SIZE);
                r.rec.setTranslateY(y*SIZE);
                terrainBuilding.getChildren().add(r.rec);
                r.rec.toBack();
            }
        }
    }   
    
    /**
     * Set outer lines, so that squares are visible
     */
    private void drawSquares(){
        squaresBetweenLines = new Group();
        double Transparency = 0.6;
        for(int x = 0; x < map.length; x++)
        {
            for(int y = 0; y < map[0].length; y++)
            {
                Rectangle lineX = new Rectangle(y*SIZE - SIZE/20, x*SIZE - SIZE/20, SIZE, SIZE/10);
                lineX.setFill(Color.rgb(200, 200, 200, Transparency));
                squaresBetweenLines.getChildren().add(lineX);
                Rectangle lineY = new Rectangle(y*SIZE - SIZE/20, x*SIZE + SIZE/20, SIZE/10, SIZE - SIZE/10);
                lineY.setFill(Color.rgb(200, 200, 200, Transparency));
                squaresBetweenLines.getChildren().add(lineY);
            }
        }
        for(int x = 0; x < map.length; x++)
        {
            Rectangle line = new Rectangle(map[0].length*SIZE - SIZE/20, x*SIZE - SIZE/20, SIZE/10, SIZE);
            line.setFill(Color.rgb(200, 200, 200, Transparency));
            squaresBetweenLines.getChildren().add(line);
        }
        for(int y = 0; y < map.length; y++)
        {
            Rectangle line = new Rectangle(y*SIZE - SIZE/20, map.length*SIZE - SIZE/20, SIZE, SIZE/10);
            line.setFill(Color.rgb(200, 200, 200, Transparency));
            squaresBetweenLines.getChildren().add(line);
        }
        Rectangle line = new Rectangle(map[0].length*SIZE - SIZE/20, map.length*SIZE - SIZE/20, SIZE/10, SIZE/10);
        line.setFill(Color.rgb(200, 200, 200, Transparency));
        squaresBetweenLines.getChildren().add(line);
        terrainBuilding.getChildren().add(squaresBetweenLines);
    }
    
    /**
     * Get stored shapes
     * @return ScrollPane with structures and stored shapes
     */
    private ScrollPane getShapes(){
        ArrayList<int[][]> shapes  = Reader.readShapes();
        ScrollPane scrollPane = new ScrollPane();
        VBox box = new VBox();
        for(int[][] m: shapes)
        {
            box.getChildren().add(drawPane(true, m, 15));
        }
        box.setSpacing(20);
        box.setAlignment(Pos.TOP_LEFT);
        scrollPane.setContent(box);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        
        return scrollPane;
    }
    
    /**
     * Draw structures 
     * @param withEvent true = add event on mouse clicked - move around terrain, false = place on terrain no mouse clicked event
     * @param map structure as a 2D matrix
     * @param size size of square
     * @return Group as visual object
     */
    private Group drawPane(boolean withEvent, int[][] map, int size)
    {
        Group g = new Group();
        for(int x = 0; x < map.length; x++)
        {
            for(int y = 0; y < map[0].length; y++)
            {
                CellType t = new CellType(map[x][y]);
                t.createType(size);
                t.rec.setTranslateX(y*size);
                t.rec.setTranslateY(x*size);
                g.getChildren().add(t.rec);
            }
        }
        if(withEvent){
            g.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    selectedShape = new Shape(drawPane(false, map, SIZE), map);
                    drawingShapeInMap = true;
                }
            });
        }
        return g;
    }
    
    /**
     * Draw existing terrain
     */
    private void drawMap(){
        for(int x = 0; x < map.length; x++)
        {
            for(int y = 0; y < map[0].length; y++)
            {
                CellType t = map[x][y];
                t.createType(SIZE);
                t.rec.setTranslateX(y*SIZE);
                t.rec.setTranslateY(x*SIZE);
                terrainBuilding.getChildren().add(t.rec);
                t.rec.toBack();
            }
        }
    }
    
    /**
     * Draw existing terrain
     * @param map terrain as a 2D matrix
     */
    private void drawMap(int[][] map){
        for(int x = 1; x < map[0].length - 1; x++)
        {
            for(int y = 1; y < map.length - 1; y++)
            {
                if(this.map[y][x].index != map[y][x])
                {
                    CellType t = new CellType(map[y][x]);
                    terrainBuilding.getChildren().remove(this.map[y][x].rec);
                    changes.add(new TerrainBuilder.Change(x, y, this.map[y][x]));
                    this.map[y][x] = t;
                    t.createType(SIZE);
                    t.rec.setTranslateX(x*SIZE);
                    t.rec.setTranslateY(y*SIZE);
                    terrainBuilding.getChildren().add(t.rec);
                    t.rec.toBack();
                }
            }
        }
    }
    
    /**
     * Convert terrain from cellType to 2D matrix
     * @return 2D matrix representation of terrain
     */
    public int[][] convertMap(){
        int[][] matrix = new int[map.length][map[0].length];
        for(int x = 0; x < map.length; x++)
        {
            for(int y = 0; y < map[0].length; y++)
            {
                matrix[x][y] = map[x][y].index;
            }
        }
        return matrix;
    }
     
    /**
     * Set variables for the visual representation
     */
    private void setVariables(){
        Door = new CellType(11);
        Window = new CellType(12);
        Wall = new CellType(10);
        Road = new CellType(0);
        Tower = new CellType(13);
        LowVisibility = new CellType(2);
        Target = new CellType(21);
        types = new int[]{0, 2, 10, 11, 12, 13, 21};
        selectedType = Wall.cloneType();
        try
        {
           FileInputStream inputstream1 = new FileInputStream("Data/Textures/Background.jpg");
           Background = new Background(new BackgroundImage(new Image(inputstream1, 1300, 1000, false, true),BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT));
        }
        catch(Exception exception)
        {
            Background = new Background(new BackgroundFill(Color.rgb(117, 74, 1), CornerRadii.EMPTY, Insets.EMPTY));
        };
        
        try
        {
           FileInputStream inputstream1 = new FileInputStream("Data/Textures/MenuBackground.jpg");
           MenuBackground = new Background(new BackgroundImage(new Image(inputstream1, 1300, 1000, false, true),BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT));
        }
        catch(Exception exception)
        {
           MenuBackground = new Background(new BackgroundFill(Color.rgb(117, 74, 1), CornerRadii.EMPTY, Insets.EMPTY));
        };
        
        try
        {
            FileInputStream inputstream = new FileInputStream("Data/Textures/ButtonBackground.jpg");
            ButtonBackground = new Background(new BackgroundImage(new Image(inputstream, 250, 60, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT));
        }
        catch(Exception exception)
        {
            ButtonBackground = new Background(new BackgroundFill(Color.rgb(117, 74, 1), CornerRadii.EMPTY, Insets.EMPTY));
        };
        ButtonStyle = "-fx-font: 22 arial;";
        ButtonWidth = 180;
        ButtonHeight = 40;
    }
    
    /**
     * Clone terrain
     * @param matrix 2D CellType representation of terrain
     * @return cloned terrain
     */
    private CellType[][] cloneMap(CellType[][] matrix){
        CellType[][] tempoMatrix = new CellType[matrix.length][matrix[0].length];
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++)
            {
                tempoMatrix[i][j] = matrix[i][j].cloneType();
            }
        }
        return tempoMatrix;
    }
    
    /**
     * Clone changes, keeping track of drawn objects
     * @param stack current changes as a Stack
     * @return cloned stack with changes
     */
    private Stack<Change> cloneChanges(Stack<Change> stack){
        Stack<Change> tempoStack = new Stack();
        for(int i = 0; i < stack.size(); i++)
        {
            tempoStack.push(stack.get(i).clone());
        }
        return tempoStack;
    }
    
    /**
     * Calculate how much the terrain will be moved
     * @return variable move
     */
    private double getAdjustment(){
        double a = (map.length*SIZE)/terrainSize;
        return a*SIZE*2;
    }
    
    /**
     * Class used to keep track of changes. The user draws on pane and can paint squares, this class keeps track of painted squares
     */
    private class Change{
        private int X;
        private int Y;
        private CellType previousType;
        
        private Change(int X, int Y, CellType previousType)
        {
            this.X = X;
            this.Y = Y;
            this.previousType = previousType;
        }
        
        public Change clone(){
            return new Change(X, Y, previousType.cloneType());
        }
    }
    
    /**
     * Class used to store structures
     */
    private class Shape{
        private Group g;
        private int[][] map;
        
        private Shape(Group g, int[][] map){
            this.g = g;
            this.map = map;
        }
    }
}
