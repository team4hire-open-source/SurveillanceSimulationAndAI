package UI.TerrainGenerator;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import javafx.geometry.Point2D;

/**
 * Class used for random map generation
 */
public class RandomMap {
    public int[][] map;
    private int maxSizeHouse;
    private int space;
    private Queue<int[][] > houses = new LinkedList<>();
    
    /**
     * Create a random map
     * @param maze true - labyrinth, false - fill with structures
     * @param tower true - generate random towers, false - don't generate towers
     * @param target true - add target, false - don't add target
     * @param space desired square space between two walls
     * @param width width of map
     * @param height height of map
     */
    public RandomMap(boolean maze, boolean tower, boolean target, int space, int width, int height){
        map = new int[height][width];
        this.space = space != -1 ? space : generateDefaultSpace(map.length);
        if(maze){
            generateRandomLabyrinth();
            if(target){
                putTargetinLabyrinth();
            }
        }
        else{
            if(target){
                putTarget();
            }
            createHouses();
            fillMapWithHouses();
        }
        if(tower){
            putTowers();
        }
        /*for(int y = 0; y < map.length; y++)
        {
            for(int x = 0; x < map.length; x++)
            {
                System.out.print(map[y][x] + " ");
            }
            System.out.println();
        }*/
    }
    
    /**
     * Method creates a list of random houses/structures
     */
    private void createHouses(){
        int housesCount = map.length/10;
        maxSizeHouse = Math.toIntExact(Math.round(-0.2192982 + (0.1336988*map.length) - (0.001217105*Math.pow(map.length, 2)) + (0.000004020468*Math.pow(map.length, 3))));
        for(int i = 0; i < housesCount; i++)
        {
            houses.add(createHouse());
        }
    }
    
    /**
     * Method creating a house/structure
     * @return matrix representing a structure
     */
    private int[][] createHouse(){
        int thisHouseSize = (int)Math.round((Math.random()*(maxSizeHouse - 1)) + 1);
        int holeCount = thisHouseSize;
        int[][] house = new int[thisHouseSize+2][thisHouseSize+2];
        for(int i = 0; i < house.length; i++)
        {
            house[0][i] = 10;
            house[house.length - 1][i] = 10;
            house[i][0] = 10;
            house[i][house.length - 1] = 10;
        }
        for(int y = 1; y < house.length - 1; y++)
        {
            for(int x = 1; x < house.length - 1; x++)
            {
                house[y][x] = 1;
            }
        }
        int count = 0;
        while(holeCount > count){
            boolean vertical = Math.round(Math.random()) == 0;
            int door1 = Math.round(Math.random()) == 0 ? 0 : house.length - 1;
            int door2 = (int)(Math.random()*(house.length - 3) + 1);
            if(vertical && house[door1][door2] == 10){
                count++;
                house[door1][door2] = 11;
            }
            else if(!vertical && house[door2][door1] == 10)
            {
                count++;
                house[door2][door1] = 11;
            }
        }
        count = 0;
        while(holeCount > count){
            boolean vertical = Math.round(Math.random()) == 0;
            int window1 = Math.round(Math.random()) == 0 ? 0 : house.length - 1;
            int window2 = (int)(Math.random()*(house.length - 3) + 1);
            if(vertical && house[window1][window2] == 10){
                count++;
                house[window1][window2] = 12;
            }
            else if(!vertical && house[window2][window1] == 10)
            {
                count++;
                house[window2][window1] = 12;
            }
        }
        
        return house;
    }
    
    /**
     * Put target on random coordinates
     */
    private void putTarget(){
        int targetSize = Math.toIntExact(Math.round(1.010438 - (0.007446565*map.length) + (0.0004097543*Math.pow(map.length, 2)) - (0.000001363905*Math.pow(map.length, 3))));
        int targetX = Math.toIntExact(Math.round(Math.random()*(map.length - 3) + 1));
        int targetY = Math.toIntExact(Math.round(Math.random()*(map.length - 3) + 1));
        for(int y = targetY - targetSize/2; y < targetY + targetSize/2 + 1; y++)
        {
            for(int x = targetX - targetSize/2; x < targetX + targetSize/2 + 1; x++)
            {
                map[y][x] = 21;
            }
        }
    }
    
    /**
     * Put target in maze
     */
    private void putTargetinLabyrinth(){
        int targetSize = Math.toIntExact(Math.round(1.010438 - (0.007446565*map.length) + (0.0004097543*Math.pow(map.length, 2)) - (0.000001363905*Math.pow(map.length, 3))));
        while(true)
        {
            int targetX = (int)Math.round(Math.random()*(map.length - 3) + 1);
            int targetY = (int)Math.round(Math.random()*(map.length - 3) + 1);
            boolean fits = true;
            for(int y = targetY - targetSize/2; y < targetY + targetSize/2 + 1; y++)
            {
                for(int x = targetX - targetSize/2; x < targetX + targetSize/2 + 1; x++)
                {
                    if(map[y][x] != 0)
                    {
                        fits = false;
                    }
                }
            }
            if(fits){
                for(int y = targetY - targetSize/2; y < targetY + targetSize/2 + 1; y++)
                {
                    for(int x = targetX - targetSize/2; x < targetX + targetSize/2 + 1; x++)
                    {
                       map[y][x] = 21;
                    }
                }
                break;
            }
        }
    }
    
    /**
     * Put towers on random coordinates
     */
    private void putTowers(){
        int towerCount = map.length/10;
        int count = 0;
        while(towerCount > count)
        {
            int targetX = (int)Math.round(Math.random()*(map.length - 3) + 1);
            int targetY = (int)Math.round(Math.random()*(map.length - 3) + 1);
            if(map[targetY][targetX] == 0)
            {
                count++;
                map[targetY][targetX] = 13;
            }
        }
    }
    
    /**
     * Fill map with structures
     */
    private void fillMapWithHouses(){
        for(int y = 1; y < map.length - 1; y++)
        {
            for(int x = 1; x < map.length - 1; x++)
            {
                if(map[y][x] == 0){
                    if(doesFit(y, x, houses.peek().length))
                    {
                        place(y, x, houses.remove());
                    }
                }
            }
        }
    }
    
    /**
     * Check if a structure will fit on given coordinates
     * @param Y y coordinate
     * @param X x coordinate
     * @param houseLength length of structure in question
     * @return true = fits, false = doesn't fit
     */
    private boolean doesFit(int Y, int X, int houseLength){
        for(int y = Y; y < houseLength + space*2 + Y; y++)
        {
            for(int x = X; x < houseLength + space*2 + X; x++)
            {
                if(x >= map.length || y >= map.length){
                    return false;
                }
                else if(map[y][x] != 0)
                {
                    if(map[y][x] == 13 || map[y][x] == 20)
                    {
                        if((y >= Y + space && y < houseLength + Y + space) || (x >= X + space && x < houseLength + X + space)){
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
            }
        }    
        return true;
    }
    
    /**
     * Place a structure on the map
     * @param Y y coordinate of map
     * @param X x coordinate of map
     * @param house structure to be placed
     */
    private void place(int Y, int X, int[][] house){
        for(int y = Y + space; y < house.length + space + Y; y++)
        {
            for(int x = X + space; x < house.length + space + X; x++)
            {
                map[y][x] = house[y - Y - space][x - X - space];
            }
        }
        houses.add(house);
    }
    
    /**
     * Recursive division method for maze generation
     */
    public void generateRandomLabyrinth(){
        Stack<Chamber> chambers = new Stack();
        chambers.push(new Chamber(new Point2D(0, 0), new Point2D(map.length - space, map[0].length - space), -1, -1));
        
        while(!chambers.isEmpty()){
            Chamber chamber = chambers.pop();
            int yWall = -1;
            if(chamber.BottomRight.getX() - chamber.TopLeft.getX() > 2 + space){
                do
                {
                    yWall = (int)((int)(Math.random()*(chamber.BottomRight.getX() - chamber.TopLeft.getX() - 3 - space)) + chamber.TopLeft.getX() + 1 + space);
                }
                while(chamber.yHole == -1 ? false : (chamber.BottomRight.getX() - chamber.TopLeft.getX() > 4 + space ? chamber.yHole == yWall : false));
                for(int i = (int)chamber.TopLeft.getY() + space; i < chamber.BottomRight.getY(); i++)
                {
                    map[i][yWall] = 10;
                }
            }
            int xWall = -1;
            if(chamber.BottomRight.getY() - chamber.TopLeft.getY() > 2 + space){
                do
                {
                    xWall = (int)((int)(Math.random()*(chamber.BottomRight.getY() - chamber.TopLeft.getY() - 3 - space)) + chamber.TopLeft.getY() + 1 + space); 
                }
                while(chamber.xHole == -1 ? false : (chamber.BottomRight.getY() - chamber.TopLeft.getY() > 4 + space ? chamber.xHole == xWall : false));
                for(int i = (int)chamber.TopLeft.getX() + space; i < chamber.BottomRight.getX(); i++)
                {
                    map[xWall][i] = 10;
                }
            }
            if(xWall != -1 && yWall != -1){
                int xHoleLeft = (int)((int)(Math.random()*(yWall - chamber.TopLeft.getX() - 1 - space)) + chamber.TopLeft.getX() + space);
                map[xWall][xHoleLeft] = 0;
                int xHoleRight = (int)((int)(Math.random()*(chamber.BottomRight.getX() - yWall - 1 - space)) + yWall + space);
                map[xWall][xHoleRight] = 0;
                int yHoleLeft = (int)((int)(Math.random()*(xWall - chamber.TopLeft.getY() - 1 - space)) + chamber.TopLeft.getY() + space);
                map[yHoleLeft][yWall] = 0;
                int yHoleRight = (int)((int)(Math.random()*(chamber.BottomRight.getY() - xWall - 1 - space)) + xWall + space);
                map[yHoleRight][yWall] = 0;
                if(yWall - chamber.TopLeft.getX() > 1 + space && xWall - chamber.TopLeft.getY() > 1 + space){
                    chambers.push(new Chamber(new Point2D(chamber.TopLeft.getX(), chamber.TopLeft.getY()), new Point2D(yWall, xWall), yHoleLeft, xHoleLeft));
                }
                if(chamber.BottomRight.getX() - yWall > 1 + space && xWall - chamber.TopLeft.getY() > 1 + space){
                    chambers.push(new Chamber(new Point2D(yWall, chamber.TopLeft.getY()), new Point2D(chamber.BottomRight.getX(), xWall), yHoleLeft, xHoleRight));
                }
                if(yWall - chamber.TopLeft.getX() > 1 + space && chamber.BottomRight.getY() - xWall > 1 + space){
                    chambers.push(new Chamber(new Point2D(chamber.TopLeft.getX(), xWall), new Point2D(yWall, chamber.BottomRight.getY()), yHoleRight, xHoleLeft));
                }
                if(chamber.BottomRight.getY() - xWall > 1 + space && chamber.BottomRight.getX() - yWall > 1 + space){
                    chambers.push(new Chamber(new Point2D(yWall, xWall), new Point2D(chamber.BottomRight.getX(), chamber.BottomRight.getY()), yHoleRight, xHoleRight));
                }
            }
            else if(xWall == -1 && yWall != -1){
                int yHole = (int)((int)(Math.random()*(chamber.BottomRight.getY() - chamber.TopLeft.getY() - 1 - space)) + chamber.TopLeft.getY() + space);
                map[yHole][yWall] = 0;
                if(yWall - chamber.TopLeft.getX() > 1 + space && chamber.BottomRight.getY() - chamber.TopLeft.getY() > 1 + space){
                    chambers.push(new Chamber(new Point2D(chamber.TopLeft.getX(), chamber.TopLeft.getY()), new Point2D(yWall, chamber.BottomRight.getY()), yHole, -1));
                }
                if(chamber.BottomRight.getY() - chamber.TopLeft.getY() > 1 + space && chamber.BottomRight.getX() - yWall > 1 + space){
                    chambers.push(new Chamber(new Point2D(yWall, chamber.TopLeft.getY()), new Point2D(chamber.BottomRight.getX(), chamber.BottomRight.getY()), yHole, -1));
                }
            }
            else if(yWall == -1 && xWall != -1){
                int xHole = (int)((int)(Math.random()*(chamber.BottomRight.getX() - chamber.TopLeft.getX() - 1 - space)) + chamber.TopLeft.getX() + space);
                map[xWall][xHole] = 0;
                if(xWall - chamber.TopLeft.getY() > 1 + space && chamber.BottomRight.getX() - chamber.TopLeft.getX() > 1 + space){
                    chambers.push(new Chamber(new Point2D(chamber.TopLeft.getX(), chamber.TopLeft.getY()), new Point2D(chamber.BottomRight.getX(), xWall), -1, xHole));
                }
                if(chamber.BottomRight.getX() - chamber.TopLeft.getX() > 1 + space && chamber.BottomRight.getY() - xWall > 1 + space){
                    chambers.push(new Chamber(new Point2D(chamber.TopLeft.getX(), xWall), new Point2D(chamber.BottomRight.getX(), chamber.BottomRight.getY()), -1, xHole));
                }
            }
        }
        for(int xx = 1; xx < map[0].length - 1; xx++)
        {
            for(int yy = 1; yy < map.length - 1; yy++)
            {
                if(map[yy][xx] == 10)
                {
                    if(map[yy - 1][xx] != 10 && ((map[yy - 1][xx - 1] == 10 && map[yy][xx - 1] != 10) || (map[yy - 1][xx + 1] == 10 && map[yy][xx + 1] != 10))){
                        map[yy][xx] = 0;
                    }
                    else if(map[yy][xx - 1] != 10 && ((map[yy - 1][xx - 1] == 10 && map[yy - 1][xx] != 10) || (map[yy + 1][xx - 1] == 10 && map[yy + 1][xx] != 10))){
                        map[yy][xx] = 0;
                    }
                    else if(map[yy][xx + 1] != 10 && ((map[yy + 1][xx + 1] == 10 && map[yy + 1][xx] != 10) || (map[yy - 1][xx + 1] == 10 && map[yy - 1][xx] != 10))){
                        map[yy][xx] = 0;
                    }
                    else if(map[yy + 1][xx] != 10 && ((map[yy + 1][xx - 1] == 10 && map[yy][xx - 1] != 10) || (map[yy + 1][xx + 1] == 10 && map[yy][xx + 1] != 10))){
                        map[yy][xx] = 0;
                    }
                }
            }
        }
    }
    
    /**
     * Create a default value for space between structures/walls
     * @param length size of map
     * @return value for space between structures
     */
    private int generateDefaultSpace(int length){
       return Math.toIntExact(Math.round((-0.245614 + (0.1376316*length) - (0.001346491*(Math.pow(length, 2))) + (0.000003947368*(Math.pow(length, 3))))));
    }
    
    /**
     * Class used for recursive division method for maze generation
     */
    private class Chamber{
        private Point2D TopLeft;
        private Point2D BottomRight;
        private int xHole, yHole;
        
        private Chamber(Point2D TopLeft, Point2D BottomRight, int xHole, int yHole)
        {
            this.TopLeft = TopLeft;
            this.BottomRight = BottomRight;
            this.xHole = xHole;
            this.yHole = yHole;
        }
    }
}
