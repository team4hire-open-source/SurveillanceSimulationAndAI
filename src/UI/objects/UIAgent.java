package UI.objects;

import agents.guards.Guard;
import agents.intruders.Intruder;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;

public class UIAgent extends Group{
    
    public UIAgent(int tileSize, Guard g){
        Circle c = new Circle(tileSize*g.radius, Color.LIGHTSKYBLUE);
        c.translateXProperty().bind(g.getCoordinates().getxPosProperty().multiply(tileSize));
        c.translateYProperty().bind(g.getCoordinates().getyPosProperty().multiply(tileSize));
        this.getChildren().add(c);
        setGuardVision(tileSize, g);
    }
    
    public UIAgent(int tileSize, Intruder i){
        Circle c = new Circle(tileSize*i.radius, Color.PINK);
        c.translateXProperty().bind(i.getCoordinates().getxPosProperty().multiply(tileSize));
        c.translateYProperty().bind(i.getCoordinates().getyPosProperty().multiply(tileSize));
        this.getChildren().add(c);
        setIntruderVision(tileSize, i);
    }
    
    private void setIntruderVision(int tileSize, Intruder i){
        Arc visualArc = new Arc();
        visualArc.setFill(Color.rgb(240,128,128, 0.5));
        visualArc.setRadiusX(tileSize*2.0f);
        visualArc.setRadiusY(tileSize*2.0f);
        visualArc.startAngleProperty().bind(i.getAngle());
        visualArc.setLength(45.0f);
        visualArc.setType(ArcType.ROUND);
        visualArc.centerXProperty().bind(i.getCoordinates().getxPosProperty().multiply(tileSize));
        visualArc.centerYProperty().bind(i.getCoordinates().getyPosProperty().multiply(tileSize));

        this.getChildren().add(visualArc);
    }
    
    private void setGuardVision(int tileSize, Guard g){
        Arc visualArc = new Arc();
        //NOT IN TOWER
        if(g.isInTower()==false)
        {
            visualArc.setFill(Color.rgb(135,206,250, 0.5));
            visualArc.setRadiusX(tileSize*1.5f);
            visualArc.setRadiusY(tileSize*1.5f);
            visualArc.startAngleProperty().bind(g.getAngle());
            visualArc.setLength(45.0f);
            visualArc.setType(ArcType.ROUND);
            visualArc.centerXProperty().bind(g.getCoordinates().getxPosProperty().multiply(tileSize));
            visualArc.centerYProperty().bind(g.getCoordinates().getyPosProperty().multiply(tileSize));
            this.getChildren().add(visualArc);
        }
        //IN TOWER
        else
        {
            Arc cutoffArc = new Arc();
            cutoffArc.setRadiusX(tileSize*1.0f);
            cutoffArc.setRadiusY(tileSize*1.0f);
            cutoffArc.startAngleProperty().bind(g.getAngle());
            cutoffArc.setLength(30.0f);
            cutoffArc.setType(ArcType.ROUND);
            cutoffArc.translateXProperty().bind(g.getCoordinates().getxPosProperty().multiply(tileSize));
            cutoffArc.translateYProperty().bind(g.getCoordinates().getyPosProperty().multiply(tileSize));
            cutoffArc.setFill(Color.rgb(255,0,0,0.1));
            //visualArc.setFill(Color.rgb(135,206,250, 0.5));

            visualArc.setFill(Color.rgb(135,206,250, 0.5));
            visualArc.setRadiusX(tileSize*4.0f);
            visualArc.setRadiusY(tileSize*4.0f);
            visualArc.startAngleProperty().bind(g.getAngle());
            visualArc.setLength(30.0f);
            visualArc.setType(ArcType.ROUND);
            visualArc.translateXProperty().bind(g.getCoordinates().getxPosProperty().multiply(tileSize));
            visualArc.translateYProperty().bind(g.getCoordinates().getyPosProperty().multiply(tileSize));
            this.getChildren().add(visualArc);
            this.getChildren().add(cutoffArc);
        }
    }
}
