package UI.objects;

import UI.data.CellType;
import agents.guards.Guard;
import agents.intruders.Intruder;
import core.Core;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import physics.NoiseModel;
import physics.Point;

/**
 * Visualised terrain 
 */
public class UITerrain extends Group{
    //variables used for terrain resizing
    public int squareSize;
    private int defaultSquareSize;
    private int MaxWidth, MaxHeight;
    private IntegerProperty FullSize;
    private DoubleProperty TranslateX;
    
    //enviroment
    private ArrayList<Circle> noises = new ArrayList<>();
    private ArrayList<UIAgent> agents = new ArrayList<>();
    private Core core;
    private boolean displayingNoise = true;
    private boolean t;

    /**
     * Create a visual terrain
     * @param MaxWidth MaxWidth of terrain
     * @param MaxHeight MaxHeight of terrain
     * @param t true = texture pack, false = colours
     */
    public UITerrain(int MaxWidth, int MaxHeight, Core core, boolean t){
        this.core = core;
        this.t = t;
        setData(MaxWidth, MaxHeight);

        Rectangle clip = new Rectangle(MaxWidth, MaxHeight);
        this.setClip(clip);
        this.MaxWidth = MaxWidth;
        this.MaxHeight = MaxHeight;
    }

    /**
     * Set data for terrain drawing
     * @param MaxWidth map MaxWidth
     * @param MaxHeight map MaxHeight
     */
    private void setData(int MaxWidth, int MaxHeight){
        this.MaxWidth = MaxWidth;
        this.MaxHeight = MaxHeight;
        int size = (MaxWidth > MaxHeight) ? MaxHeight : MaxWidth;
        squareSize = size/core.map.length;
        defaultSquareSize = squareSize;
        FullSize = new SimpleIntegerProperty(core.map.length*squareSize);
        TranslateX = new SimpleDoubleProperty(0);
        drawTerrain();
    }

    /**
     * Draw terrain from 2D matrix representation
     */
    public void drawTerrain() {
        this.getChildren().clear();
        for(int i=0;i<core.map.length;i++)
        {
            for(int j=0;j<core.map[0].length;j++)
            {
                CellType c = new CellType(core.map[i][j]);
                if(t){
                    c.createType(squareSize);
                    c.setTexture();
                    this.getChildren().add(c.imageContainer);
                    c.imageContainer.setTranslateX(j*squareSize);
                    c.imageContainer.setTranslateY(i*squareSize);
                }
                else{
                    c.createType(squareSize);
                    this.getChildren().add(c.rec);
                    c.rec.setTranslateX(j*squareSize);
                    c.rec.setTranslateY(i*squareSize);
                }
            }
        }
        this.setTranslateX((MaxWidth - FullSize.getValue())/2);
        TranslateX.setValue(this.getTranslateX());
        this.setTranslateY((MaxHeight - FullSize.getValue())/2);
        drawAgents();
    }
    
    public void displayEntryPoint(Point p){
        CellType c = new CellType(CellType.getCellType(CellType.CELLTYPE.Entry));
        if(t){
            c.createType(squareSize);
            c.setTexture();
            this.getChildren().add(c.imageContainer);
            c.imageContainer.setTranslateX(p.y*squareSize);
            c.imageContainer.setTranslateY(p.x*squareSize);
        }
        else{
            c.createType(squareSize);
            this.getChildren().add(c.rec);
            c.rec.setTranslateX(p.y*squareSize);
            c.rec.setTranslateY(p.x*squareSize);
        }
    }

    /**
     * Calculations used for correct screen representation
     * @return adjustment variable
     */
    public int getAdjustment(){
        double size = (MaxWidth > MaxHeight) ? MaxHeight : MaxWidth;
        double a = (core.map.length*squareSize)/size;
        return Math.toIntExact(Math.round(a*squareSize*2));
    }

    /**
     * Method will draw agents and display them.
     */
    public void drawAgents(){
        for(Guard g: core.guards)
        {
            UIAgent a = new UIAgent(squareSize, g);
            agents.add(a);
            this.getChildren().add(a);
        }
        for(Intruder i: core.intruders)
        {   
            UIAgent a = new UIAgent(squareSize, i);
            agents.add(a);
            this.getChildren().add(a);
        }
    }
    
    /**
     * Draw noise
     */
    public void drawNoise(){
        for(Circle c: noises)
        {
            this.getChildren().remove(c);
        }
        noises.clear();
        if(displayingNoise){
            for(NoiseModel n: core.noises){
                Circle c = new Circle((squareSize/2.0)*n.getVolume(), Color.rgb(0, 75, 0, 0.1));
                c.setTranslateX(n.getSource().getxPos()*squareSize);
                c.setTranslateY(n.getSource().getyPos()*squareSize);
                this.getChildren().add(c);
                noises.add(c);
            }
        }
    }
    
    /**
     * Returns view to initial default
     */
    public void returnToDefault(){
        this.getChildren().clear();
        squareSize = defaultSquareSize;
        drawTerrain();
        drawAgents();
        this.setTranslateX((MaxWidth - FullSize.getValue())/2);
        TranslateX.setValue(this.getTranslateX());
        this.setTranslateY((MaxHeight - FullSize.getValue())/2);
        this.getClip().setTranslateX(0);
        this.getClip().setTranslateY(0);
    }

    /**
     * Redraw agents
     */
    public void redrawAgents(){
        for(UIAgent a: agents){
            this.getChildren().remove(a);
        }
        agents.clear();
        drawAgents();
    }

    /**
     * Resize width
     * @param newWidth 
     */
    public void resizeWidth(int newWidth){
        MaxWidth = newWidth;
        int size = (MaxWidth > MaxHeight) ? MaxHeight : MaxWidth;
        Rectangle clip = new Rectangle(size, size);
        this.setClip(clip);
        squareSize = size/core.map.length;
        FullSize.setValue(squareSize*core.map.length);
        drawTerrain();
    }

    /**
     * Resize height
     * @param newHeight new height value
     */
    public void resizeHeight(int newHeight){
        MaxHeight = newHeight;
        int size = (MaxWidth > MaxHeight) ? MaxHeight : MaxWidth;
        Rectangle clip = new Rectangle(size, size);
        this.setClip(clip);
        squareSize = size/core.map.length;
        FullSize.setValue(squareSize*core.map.length);
        drawTerrain();
    }

    /**
     * move view to the left
     */
    public void moveLeft(){
        this.setTranslateX(this.getTranslateX() - getAdjustment());
        this.getClip().setTranslateX(this.getClip().getTranslateX() + getAdjustment());
    }

    /**
     * move view to the right
     */
    public void moveRight(){
        this.setTranslateX(this.getTranslateX() + getAdjustment());
        this.getClip().setTranslateX(this.getClip().getTranslateX() - getAdjustment());
    }

    /**
     * move view up
     */
    public void moveUp(){
        this.setTranslateY(this.getTranslateY() - getAdjustment());
        this.getClip().setTranslateY(this.getClip().getTranslateY() + getAdjustment());
    }

    /**
     * move view down
     */
    public void moveDown(){
        this.setTranslateY(this.getTranslateY() + getAdjustment());
        this.getClip().setTranslateY(this.getClip().getTranslateY() - getAdjustment());
    }

    /**
     * @return width size
     */
    public IntegerProperty getWidth(){
        return FullSize;
    }

    /**
     * @return x position of terrain
     */
    public DoubleProperty getXPosition(){
        return TranslateX;
    }
    
    public void setDisplayingNoise(boolean displayingNoise){
        this.displayingNoise = displayingNoise;
    }
}

