package UI.data;

import java.io.FileInputStream;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Used for reading terrain index and transforming it to a graphic object.
 */
public class CellType{
    public static enum CELLTYPE{Road, Floor, Bush, Wall, Door, Window, Tower, Entry, Target, Default};
    public int index; //index representing data type
    public int SIZE; //size of graphic object
    public Rectangle rec; //graphic object used for UI
    public Color c; //colour of object
    public String TYPE; //name of the cell type
    public String keyCode; //key code used for drawing terrain
    public StackPane imageContainer;

    /**
     * Constructor takes in an index from matrix and fills data accordingly.
     * @param index representing data type
     */
    public CellType(int index)
    {
        this.index = index;
        switch(index){
            case 0: c = Color.GREY; TYPE = CELLTYPE.Road.toString(); keyCode = "R"; break;
            case 1: c = Color.ORANGE; TYPE = CELLTYPE.Floor.toString(); break;
            case 2: c = Color.rgb(75, 75, 75); TYPE = CELLTYPE.Bush.toString(); keyCode = "Y"; break;
            case 10: c = Color.RED;  TYPE = CELLTYPE.Wall.toString(); keyCode = "E"; break;
            case 11: c = Color.BROWN;  TYPE = CELLTYPE.Door.toString(); keyCode = "Q"; break;
            case 12: c = Color.BLUE;  TYPE = CELLTYPE.Window.toString(); keyCode = "W"; break;
            case 13: c = Color.GREEN;  TYPE = CELLTYPE.Tower.toString(); keyCode = "T"; break;
            case 20: c = Color.GREY;  TYPE = CELLTYPE.Entry.toString(); break;
            case 21: c = Color.PLUM;  TYPE = CELLTYPE.Target.toString(); keyCode = "U"; break;
            default: c = Color.BLACK; TYPE = CELLTYPE.Default.toString(); break;
        }
    }
    
    public void setTexture(){
        try{
        FileInputStream file;
        switch(index){
            case 0: file = new FileInputStream("Data/Textures/Path.jpg"); break;
            case 1: file = new FileInputStream("Data/Textures/Floor.jpg"); break;
            case 2: file = new FileInputStream("Data/Textures/Path.jpg"); break;
            case 10: file = new FileInputStream("Data/Textures/Wall.jpg"); break;
            case 11: file = new FileInputStream("Data/Textures/Door.jpg"); break;
            case 12: file = new FileInputStream("Data/Textures/Window.jpg"); break;
            case 13: file = new FileInputStream("Data/Textures/Tower.jpg"); break;
            case 20: file = new FileInputStream("Data/Textures/Path.jpg"); break;
            case 21: file = new FileInputStream("Data/Textures/Target.jpg"); break;
            default: file = new FileInputStream("Data/Textures/Path.jpg"); break;
        }
        imageContainer = new StackPane();
        Image image = new Image(file, SIZE, SIZE, false, true);
        ImageView imageView = new ImageView(image);
        imageContainer.getChildren().addAll(rec, imageView);
        }
        catch(Exception e){
            System.out.println("error");
        }
    }

    /**
     * Method used for creating the graphic object.
     * @param SIZE size used to create the object
     */
    public void createType(int SIZE){
        this.SIZE = SIZE;
        rec = new Rectangle(SIZE, SIZE);
        rec.setFill(c);
    }

    /**
     * Custom clone method, creates a new CellType with same specifications.
     * @return cloned CellType
     */
    public CellType cloneType(){
        CellType clone = new CellType(index);
        if(rec != null)
        {
            clone.createType(SIZE);
        }
        return clone;
    }
    
    /**
     * Get a CellType from index
     * @param index representing cellType
     * @return name of cell
     */
    public static String getCellType(int index){
        switch(index){
            case 0: return CELLTYPE.Road.toString();
            case 1: return CELLTYPE.Floor.toString();
            case 2: return CELLTYPE.Bush.toString();
            case 10: return CELLTYPE.Wall.toString();
            case 11: return CELLTYPE.Door.toString();
            case 12: return CELLTYPE.Window.toString();
            case 13: return CELLTYPE.Tower.toString();
            case 20: return CELLTYPE.Entry.toString();
            case 21: return CELLTYPE.Target.toString();
            default: return CELLTYPE.Default.toString();
        }
    }
    
    /**
     * Get a cell type index from name
     * @param name name of cell
     * @return index of cell
     */
    public static int getCellType(CELLTYPE name){
        switch(name){
            case Road: return 0;
            case Floor: return 1;
            case Bush: return 2;
            case Wall: return 10;
            case Door: return 11;
            case Window: return 12;
            case Tower: return 13;
            case Entry: return 20;
            case Target: return 21;
            default: return -1;
        }
    }
}
