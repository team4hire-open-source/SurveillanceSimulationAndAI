package UI.data;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Used for reading and saving 2d matrix representations of terrains
 */
public class Reader {
    
    public static int[][] readTerrain(String name){
        try{
            FileReader f = new FileReader("Data/Terrain/" + name  + ".txt");
            int[][] gameMap;
            int height = 0;
            int width = 0;

            Scanner fileScanner = new Scanner(f);
            while (fileScanner.hasNextLine()) {
                if(height==0){
                    String line = fileScanner.nextLine();
                    Scanner lineScanner = new Scanner(line);
                    while (lineScanner.hasNext()) {
                        lineScanner.next();
                        width++;
                    }
                    lineScanner.close();
                }
                else{
                    fileScanner.nextLine();
                }
                height++;
            }

            fileScanner.close();
            gameMap = new int[height][width];
            height = 0;
            width =  0;

            fileScanner = new Scanner(f);
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                while (lineScanner.hasNextInt()) {
                  int token = lineScanner.nextInt();
                  gameMap[height][width] = token;
                  width++;
                }
                width = 0;
                lineScanner.close();
                height++;
            }
            fileScanner.close();
            return gameMap;
        }
        catch(Exception e){
            System.out.println("Error while reading terrains");
            return null;
        }
    }
    
    /**
     * Read existing terrains
     * @return list of maps with a name
     */
    public static ArrayList<TerrainData> readTerrains(){
        try{
            File f = new File("Data/Terrain");
            File[] files = f.listFiles();
            ArrayList<TerrainData> shapes = new ArrayList<>();
            for(int i = 0; i < files.length; i++){
                int[][] gameMap;
                int height = 0;
                int width = 0;
                FileReader file = new FileReader("Data/Terrain/" + files[i].getName());

                Scanner fileScanner = new Scanner(file);
                while (fileScanner.hasNextLine()) {
                    if(height==0){
                        String line = fileScanner.nextLine();
                        Scanner lineScanner = new Scanner(line);
                        while (lineScanner.hasNext()) {
                            lineScanner.next();
                            width++;
                        }
                        lineScanner.close();
                    }
                    else{
                        fileScanner.nextLine();
                    }
                    height++;
                }
                
                fileScanner.close();
                gameMap = new int[height][width];
                height = 0;
                width =  0;

                file = new FileReader("Data/Terrain/" + files[i].getName());
                fileScanner = new Scanner(file);
                while (fileScanner.hasNextLine()) {
                    String line = fileScanner.nextLine();
                    Scanner lineScanner = new Scanner(line);
                    while (lineScanner.hasNextInt()) {
                      int token = lineScanner.nextInt();
                      gameMap[height][width] = token;
                      width++;
                    }
                    width = 0;
                    lineScanner.close();
                    height++;
                }
                fileScanner.close();
                shapes.add(new TerrainData(gameMap, files[i].getName()));
            }
            return shapes;
        }
        catch(Exception e){
            System.out.println("Error while reading terrains");
            return null;
        }
    }
    
    /**
     * Save a terrain
     * @param terrain matrix representation of terrain
     * @param name name of terrain
     */
    public static void saveTerrain(int[][] terrain, String name){
        try{
            PrintWriter out = new PrintWriter("Data/Terrain/" + name + ".txt");
            for(int x = 0; x < terrain.length; x++)
            {
                for(int y = 0; y < terrain[0].length; y++)
                {
                    out.print(terrain[x][y] + " ");
                }
                out.println();
            }
            out.close();
        }
        catch(Exception e){
            try{
                File f = new File("Data/Terrain");
                File[] files = f.listFiles();
                int fileSize = files.length;
                PrintWriter out = new PrintWriter("Data/Terrain/" + fileSize + ".txt");
                for(int x = 0; x < terrain.length; x++)
                {
                    for(int y = 0; y < terrain[0].length; y++)
                    {
                        out.print(terrain[x][y] + " ");
                    }
                    out.println();
                }
                out.close();
            }
            catch(Exception ex){
                System.out.println("Error while storing new map");
            }
        }
    }
    
    /**
     * Save a structure/house/shape to a list
     * @param shape matrix representation
     */
    public static void saveShape(int[][] shape){
        try{
            File f = new File("Data/Shapes");
            File[] files = f.listFiles();
            int fileSize = files.length;
            PrintWriter out = new PrintWriter("Data/Shapes/" + fileSize + ".txt");
            for(int x = 0; x < shape.length; x++)
            {
                for(int y = 0; y < shape[0].length; y++)
                {
                    out.print(shape[x][y] + " ");
                }
                out.println();
            }
            out.close();
        }
        catch(Exception e){
                System.out.println("Error while storing new shape");
        }
    }
    
    /**
     * Read existing shapes
     * @return list of shapes/structures as 2D matrices
     */
    public static ArrayList<int[][]> readShapes(){
        try{
            File f = new File("Data/Shapes");
            File[] files = f.listFiles();
            ArrayList<int[][]> shapes = new ArrayList<>();
            for(int i = 0; i < files.length; i++){
                int[][] gameMap;
                int height = 0;
                int width = 0;
                FileReader file = new FileReader("Data/Shapes/" + files[i].getName());

                Scanner fileScanner = new Scanner(file);
                while (fileScanner.hasNextLine()) {
                    if(height==0){
                        String line = fileScanner.nextLine();
                        Scanner lineScanner = new Scanner(line);
                        while (lineScanner.hasNext()) {
                            lineScanner.next();
                            width++;
                        }
                        lineScanner.close();
                    }
                    else{
                        fileScanner.nextLine();
                    }
                    height++;
                }
                
                fileScanner.close();
                gameMap = new int[height][width];
                height = 0;
                width =  0;

                file = new FileReader("Data/Shapes/" + files[i].getName());
                fileScanner = new Scanner(file);
                while (fileScanner.hasNextLine()) {
                    String line = fileScanner.nextLine();
                    Scanner lineScanner = new Scanner(line);
                    while (lineScanner.hasNextInt()) {
                      int token = lineScanner.nextInt();
                      gameMap[height][width] = token;
                      width++;
                    }
                    width = 0;
                    lineScanner.close();
                    height++;
                }
                fileScanner.close();
                shapes.add(gameMap);
            }
            return shapes;
        }
        catch(Exception e){
            System.out.println("Error while reading shapes");
            return null;
        }
    }
}
