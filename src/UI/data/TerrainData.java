package UI.data;

/**
 * Used for storing, reading and display of existing terrains
 */
public class TerrainData {
    public int[][] map;
    public String name;
    
    /**
     * Constructor
     * @param map 2D representation of terrain
     * @param name name of terrain
     */
    public TerrainData(int[][] map, String name){
        this.map = map;
        this.name = reformatName(name.toCharArray());
    }
    
    /**
     * Method used for reading file names and extracting the name without extension.
     * @param name file name
     * @return file name without extension
     */
    private String reformatName(char[] name){
        String reformatedName = "";
        int index = 0;
        for(int i = 0; i < name.length; i++)
        {
            if(name[i] == '.')
            {
                index = i;
            }
        }
        for(int i = 0; i < index; i++)
        {
            reformatedName+=name[i];
        }
        return reformatedName;
    }
}
