package physics;

public class NormalDist {

    private static double x2;
    private static int count = 1;
    public static double NormalDist(double mu, double sigma) {
        if (count%2==0) {
            count++;
            return mu + sigma*x2;
        }
        else {
            double u1 = Math.random();
            double u2 = Math.random();
            x2 = Math.sqrt(-2*Math.log(u1))*Math.sin(2*Math.PI * u2);
            double x1 = Math.sqrt(-2*Math.log(u1))*Math.cos(2*Math.PI * u2);
            count++;
            return mu + sigma*x1;
        }
    }
}
