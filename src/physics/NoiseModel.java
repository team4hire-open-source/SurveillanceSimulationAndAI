package physics;

import static core.Core.millisecondsCounter;

public class NoiseModel {

    private Vector source;
    private int volume;
    private long birthday;

    public NoiseModel(Vector source, int volume) {
        this.source = source;
        this.volume = volume;
        birthday = millisecondsCounter;
    }

    public Vector getSource() {
        return source;
    }

    public int getVolume() {
        return volume;
    }

    public long getBirthday() { return birthday; }
}
