package physics;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Vector {

  private DoubleProperty xPos;
  private DoubleProperty yPos;

  public Vector(){
    this.xPos = new SimpleDoubleProperty(0);
    this.yPos = new SimpleDoubleProperty(0);
  }

  public Vector(double xPos, double yPos){
    this.xPos = new SimpleDoubleProperty(xPos);
    this.yPos = new SimpleDoubleProperty(yPos);
  }

  public double getxPos() {
    return xPos.getValue();
  }
  
  public DoubleProperty getxPosProperty() {
    return xPos;
  }

  public void setxPos(double xPos) {
    this.xPos.setValue(xPos);
  }

  public double getyPos() {
    return yPos.getValue();
  }
  
  public DoubleProperty getyPosProperty() {
    return yPos;
  }

  public void setyPos(double yPos) {
    this.yPos.setValue(yPos);
  }
}
