package physics;

import core.Core;
import java.util.ArrayList;

public class Noise {

    private int volume;
    private final double lambda = 0.1;
    private double timeInterval = 0.01666666666666666666666666666667; //in minutes
    private Core core;
    private ArrayList<Long> arrivalTimes = new ArrayList<>();
    private ArrayList<int[]> squares = new ArrayList<>();
    
    public Noise(Core core, int[][] map){
        this.core = core;
        for (int i = 5; i < map.length-1; i = i + 5) {
            for (int j = 5; j < map.length-1; j = j + 5) {
                squares.add(new int[]{i-5,j-5,i,j});
            }
        }
        for (int k = 0; k < squares.size(); k++) {
            arrivalTimes.add(Long.valueOf(0));
            newArrivalTime(k);
        }
    }

    public void commitPoisson() {
        for (int i = 0; i < arrivalTimes.size(); i++) {
            if (core.millisecondsCounter >= arrivalTimes.get(i)) {
                findRandomSource(squares.get(i)[0],squares.get(i)[1],squares.get(i)[2],squares.get(i)[3]);
                newArrivalTime(i);
            }
        }
    }

    public void newArrivalTime(int i) {
        long seconds;
        if (arrivalTimes.get(i) != 0) {
            seconds = arrivalTimes.get(i)/1000;
        }
        else {
            seconds = 0;
        }
        long newSec = Math.round(1000*(seconds - (1/(lambda*timeInterval))*Math.log(Math.random())));
        arrivalTimes.set(i, newSec);
    }

    public void findRandomSource(int startX, int startY, int endX, int endY) {
        volume = 5;
        int size = endX-startX;
        double xCoord = Math.random() * size;
        xCoord = xCoord +startX;
        size = endY-startY;
        double yCoord = Math.random() * size;
        yCoord = yCoord + startY;
        core.noises.add(new NoiseModel(new Vector(xCoord,yCoord), volume));
    }


    public void noiseWalk(double velocity, Vector pos) {
        if (velocity < 0.5) {
            volume = 1;
        }
        else if (velocity < 1 && velocity > 0.5) {
            volume = 3;
        }
        else if (velocity < 2 && velocity > 1) {
            volume = 5;
        }
        else if (velocity > 2) {
            volume = 10;
        }
        core.noises.add(new NoiseModel(pos, volume));

    }

    public void noiseEnter(boolean door, Vector pos) {
        if (door) {
            volume = 5;
        }
        else {
            volume = 10;
        }
        core.noises.add(new NoiseModel(pos, volume));
    }

}
