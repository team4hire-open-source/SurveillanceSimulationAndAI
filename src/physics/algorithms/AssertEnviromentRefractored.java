package physics.algorithms;

import environment.Vertice;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.geometry.Point2D;
import physics.PhysicsAgents;
import physics.Point;

public class AssertEnviromentRefractored {
    private int[][] exploredMap;
    private double x;
    private double y;
    private int xInt;
    private int yInt;
    private double[][] weightList;
    private double[] genes;
    private HashMap<Point2D, Vertice> hashMap;
    //public int nextStepInPathIndex;
    //public Point2D nextStepInPath;
    //public ArrayList<Point2D> path;
    
    private boolean reachedFinalDestination = false;
    private boolean doIseeGoal = false;
    private boolean doIHaveToHide = false;
    private boolean pathError = false;
    public boolean stayAndObserve = false;
    public Point2D nextStep;
    private ArrayList<Point> needExploration;
    public Point2D finalDestination;
    
    public AssertEnviromentRefractored(boolean guard, int[][] exploredMap, double x, double y, 
            double[][] weightList, double[] genes, HashMap<Point2D, Vertice> hashMap, Point2D finalDestination){
        this.exploredMap = exploredMap;
        this.x = x;
        this.y = y;
        this.weightList = weightList;
        this.genes = genes;
        this.hashMap = hashMap;
        this.finalDestination = finalDestination;
        doIHaveToHide = doIHaveToHideRefractored();
        AStar a;
        if(finalDestination == null){
            a = new AStar(true, 0, 0, hashMap, exploredMap, x, y, weightList, doIHaveToHide, genes);
        }
        else{
            a = new AStar(false, finalDestination.getX(), finalDestination.getY(), hashMap, exploredMap, x, y, weightList, doIHaveToHide, genes);
        }
        ArrayList<Point2D> path = a.getSolution();
        if(path == null || path.isEmpty()){
            stayAndObserve = true;
            this.finalDestination = null;
        }
        else if(path.size() == 1){
            this.finalDestination = null;
            stayAndObserve = true;
        }
        else{
            nextStep = path.get(1);
            this.finalDestination = path.get(path.size() - 1);
        }
    }
    
    public AssertEnviromentRefractored(boolean guard, int[][] exploredMap, double x, double y, Point2D finalDestination, 
            double[][] weightList, double[] genes, HashMap<Point2D, Vertice> hashMap){
        this.exploredMap = exploredMap;
        this.x = x;
        this.y = y;
        this.xInt = Math.toIntExact(Math.round(x - 0.5));
        this.yInt = Math.toIntExact(Math.round(y - 0.5));
        this.finalDestination = finalDestination;
        this.weightList = weightList;
        this.genes = genes;
        this.hashMap = hashMap;
        reachedFinalDestination = check(new Point2D(x, y), finalDestination);
        doIseeGoal = doIseeGoal();
        doIHaveToHide = doIHaveToHide();
        if(guard){
            whatToDoGuard();
        }
        else{
            whatToDoIntruder();
        }
    }
    
    private void whatToDoGuard(){
        Point p = needExploration.get(Math.toIntExact(Math.round(Math.random()*(needExploration.size() - 1))));
        pathToSpecificCoordinate(p);
    }
    
    private void whatToDoIntruder(){
        if(doIseeGoal){
            AStar a = new AStar(hashMap, exploredMap, x, y, weightList, false, genes);
            ArrayList<Point2D> path = a.getSolution();
            if(path == null || path.isEmpty()){
                pathError = true;
            }
            else if(path.size() == 1){
                reachedFinalDestination = true;
                stayAndObserve = true;
                finalDestination = path.get(0);
            }
            else{
                nextStep = path.get(1);
                finalDestination = path.get(path.size() - 1);
            }
        }
        else if(doIHaveToHide){
            AStar a = new AStar(hashMap, exploredMap, x, y, weightList, true, genes);
            ArrayList<Point2D> path = a.getSolution();
            if(path == null || path.isEmpty()){
                pathError = true;
            }
            else if(path.size() == 1){
                reachedFinalDestination = true;
                stayAndObserve = true;
                finalDestination = path.get(0);
            }
            else{
                nextStep = path.get(1);
                finalDestination = path.get(path.size() - 1);
            }
        }
        else{            
            if(!needExploration.isEmpty()){
                if(finalDestination == null){
                    Point p = needExploration.get(Math.toIntExact(Math.round(Math.random()*(needExploration.size() - 1))));
                    pathToSpecificCoordinate(p);
                }
                else{
                    double xCheck = Math.round(x*10.0)/10.0;
                    double yCheck = Math.round(y*10.0)/10.0;
                    xCheck = (xCheck*10)%2 == 0 ? Math.round((xCheck + 0.1)*10.0)/10.0: xCheck;
                    yCheck = (yCheck*10)%2 == 0 ? Math.round((yCheck + 0.1)*10.0)/10.0: yCheck;
                    double angleCurrent = 180-Math.toDegrees(Math.atan2(x-finalDestination.getX(), y-finalDestination.getY()));
                    double[] rotationMaximum = PhysicsAgents.getBlindMaxAngleRad();
                    rotationMaximum[0] = Math.toDegrees(rotationMaximum[0]);
                    rotationMaximum[1] = Math.toDegrees(rotationMaximum[1]);
                    Point p = null;
                    for(Point pp : needExploration){
                        double angleFuture = 180-Math.toDegrees(Math.atan2(x-pp.x, y-pp.y));
                        if(angleFuture >= angleCurrent - rotationMaximum[0] && angleFuture <= angleCurrent + rotationMaximum[0]){
                            p = pp;
                            break;
                        }
                    }
                    if(p != null){
                        pathToSpecificCoordinate(p);
                    }
                    else{
                        p = needExploration.get(Math.toIntExact(Math.round(Math.random()*(needExploration.size() - 1))));
                        pathToSpecificCoordinate(p);
                    }
                }
            }
        }
        if(pathError){
            pathError = false;
            if(!needExploration.isEmpty()){
                Point p = needExploration.get(Math.toIntExact(Math.round(Math.random()*(needExploration.size() - 1))));
                pathToSpecificCoordinate(p);
            }
            else{
                stayAndObserve = true;
            }
            if(pathError){
                stayAndObserve = true;
            }
        }
    }
    
    private void pathToSpecificCoordinate(Point p){
        AStar a = new AStar(hashMap, exploredMap, x, y, p.x, p.y, weightList, false, genes);
        ArrayList<Point2D> path = a.getSolution();
        if(path == null || path.isEmpty()){
            pathError = true;
        }
        else if(path.size() == 1){
            reachedFinalDestination = true;
            stayAndObserve = true;
            finalDestination = path.get(0);
        }
        else{
            nextStep = path.get(1);
            finalDestination = path.get(path.size() - 1);
        }
    }
    
    private boolean doIHaveToHideRefractored(){
        int countWalkable = 0;
        int countWalkableNotRestricted = 0;
        for(int i = 0; i < exploredMap.length; i++){
            for(int j = 0; j < exploredMap[0].length; j++){
                int index = exploredMap[i][j];
                if(index == 21 || index == 0 || index == 1 || index == 2){
                    countWalkable++;
                    if(weightList[i][j] <= genes[5]){
                        countWalkableNotRestricted++;
                    }
                }
            }
        }
        if(countWalkableNotRestricted < genes[6] && countWalkable > genes[6]){
            return true;
        }
        return false;
    }
    
    private boolean doIHaveToHide(){
        int countWalkable = 0;
        int countWalkableNotRestricted = 0;
        needExploration = new ArrayList<Point>();
        for(int i = 0; i < exploredMap.length; i++){
            for(int j = 0; j < exploredMap[0].length; j++){
                int index = exploredMap[i][j];
                if(index == 21 || index == 0 || index == 1 || index == 2){
                    countWalkable++;
                    if(weightList[i][j] <= genes[5]){
                        countWalkableNotRestricted++;
                    }
                    if(i - 1 >= 0 && exploredMap[i - 1][j] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(i - 1 >= 0 && j - 1 >= 0 && exploredMap[i - 1][j - 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(i - 1 >= 0 && j + 1 < exploredMap[0].length && exploredMap[i - 1][j + 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(j - 1 >= 0 && exploredMap[i][j - 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(j + 1 < exploredMap[0].length && exploredMap[i][j + 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(i + 1 < exploredMap.length && j - 1 >= 0 && exploredMap[i + 1][j - 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(i + 1 < exploredMap.length && exploredMap[i + 1][j] == -1){
                        needExploration.add(new Point(i, j));
                    }
                    else if(i + 1 < exploredMap.length && j + 1 < exploredMap[0].length &&  exploredMap[i + 1][j + 1] == -1){
                        needExploration.add(new Point(i, j));
                    }
                }
            }
        }
        if(countWalkableNotRestricted < genes[6] && countWalkable > genes[6]){
            return true;
        }
        return false;
    }
    
    private boolean doIseeGoal(){
        for(int i = 0; i < exploredMap.length; i++){
            for(int j = 0; j < exploredMap[0].length; j++){
                if(exploredMap[i][j] == 21){
                    return true;
                }
            }
        }
        return false;
    }
    
    
    private boolean check(Point2D origin, Point2D goal){
        if(goal == null || origin == null){
            return false;
        }
        double x = Math.round(origin.getX()*10.0)/10.0;
        double y = Math.round(origin.getY()*10.0)/10.0;
        x = (x*10)%2 == 0 ? Math.round((x + 0.1)*10.0)/10.0: x;
        y = (y*10)%2 == 0 ? Math.round((y + 0.1)*10.0)/10.0: y;
        double i = Math.round(goal.getX()*10.0)/10.0;
        double j = Math.round(goal.getY()*10.0)/10.0;
        i = (i*10)%2 == 0 ? Math.round((i + 0.1)*10.0)/10.0: i;
        j = (j*10)%2 == 0 ? Math.round((j + 0.1)*10.0)/10.0: j;
        if(x == i && y == j){
            return true;
        }
        else{
            return false;
        }
    }
}
