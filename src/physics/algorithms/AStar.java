package physics.algorithms;

import environment.Vertice;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.geometry.Point2D;
import physics.Point;

/**
 * Class used to solve find a path using Astar algorith,
 * @author Jordam, Tim, Carlos, Benjamin
 * @date 26.06
 * @version 3.0
 */
public class AStar {
    private ArrayList<Point2D> solution = new ArrayList<>();
    private double goalX;
    private double goalY;
    private boolean goalCoordinates = false;
    private int[][] map;
    private double[][] weightList;
    private int[][] seenMap;
    private boolean searchForHidingSpot = false;
    private double[] genes;
    private static HashMap<Point2D, Vertice> pathMap = new HashMap<Point2D, Vertice>();
    private HashMap<Point2D, Vertice> hashMap;
    private boolean generateSolutionOnFault = false;
    
    public static void main(String[] args){
        int[][] map1 = new int[][]{{10, 21, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                                {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10} ,
                                {10, 0, 0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 0, 10, 10, 0, 10},
                                {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                                {10, 0, 0, 10, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 10, 10, 0, 10, 0, 10},
                                {10, 0, 0, 10, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 10, 10, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 10, 0, 10},
                                {10, 0, 0, 0, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 0, 10},
                                {10, 0, 0, 0, 10, 10, 10, 10, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10, 0, 10},
                                {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                                {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21, 10 },
                                {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}};
        
        int[][] map2 = new int[][]{{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                {10, 0 ,0, 0, 0, 0 ,0 ,0 ,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10 },
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,10}, 
                {                   10, 0, 0, 0 ,0, 0, 10, 10, 10, 10, 10, 0, 0, 0, 0, 0 ,0, 0, 0, 10 }, 
                {                   10, 0, 0, 0 ,0 ,0 ,12 ,0 ,0 ,0 ,10, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,10 }, 
                {                   10, 0, 0, 0, 0, 0 ,12 ,0 ,0 ,0 ,11 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,10 }, 
                {                    10, 0, 0, 0, 0, 0, 10, 0, 0, 0 ,10, 0, 0, 0, 0 ,0 ,0 ,0, 0, 10},  
                {                   10, 0, 0, 0, 0, 0, 10, 0 ,0, 0, 10, 0, 0, 0, 0, 0, 0, 13, 0, 10 }, 
                {                   10, 0, 0, 0, 0, 0, 10, 10, 10, 10, 10, 0, 0, 0, 0, 0 ,0, 0, 0, 10 }, 
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 }, 
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
               
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21, 0, 0, 0, 10 },
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                 
                {                   10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0, 10 },
                  {                  10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}};
        createPath(map1);
        AStar a = new AStar(pathMap, map1, 0.5, 1.5, 18, 18, null, false, null);
        //createPath(map2);
        //AStar a = new AStar(pathMap, map2, 1.5, 1.5, null, false, null);
        for(Point2D p: a.getSolution()){
            System.out.println(p.getX() + " " + p.getY());
        }
        int[][] map = new int[][]{{0, 0, 0, 0},
                                  {10, 10, 10, 0},
                                  {0, 0, 0, 0},
                                  {21, 0, 0, 0}};
        
       // AStar a = new AStar(map, 0, 0);
        //AStar a1 = new AStar(map1, 1, 1, 18, 18);
        
    }
    
    public AStar(boolean generateSolutionOnFault, double goalX, double goalY, HashMap<Point2D, Vertice> hashMap, int[][] map, double startX, double startY, double[][] weightList, boolean searchForHidingSpot, double[] genes){
        this.generateSolutionOnFault = generateSolutionOnFault;
        goalX = Math.round((goalX)*10.0)/10.0;
        goalY = Math.round((goalY)*10.0)/10.0;
        this.goalX = (goalX*10)%2 == 0 ? Math.round((goalX + 0.1)*10.0)/10.0: goalX;
        this.goalY = (goalY*10)%2 == 0 ? Math.round((goalY + 0.1)*10.0)/10.0: goalY;
        if(!generateSolutionOnFault){
            goalCoordinates = true;
        }
        this.hashMap = hashMap;
        this.map = map;
        if(weightList != null){
            this.weightList = weightList;
        }
        else{
            createWeightList();
        }
        this.searchForHidingSpot = searchForHidingSpot;
        this.genes = genes;
        startX = Math.round(startX*10.0)/10.0;
        startY = Math.round(startY*10.0)/10.0;
        startX = (startX*10)%2 == 0 ? Math.round((startX + 0.1)*10.0)/10.0: startX;
        startY = (startY*10)%2 == 0 ? Math.round((startY + 0.1)*10.0)/10.0: startY;
        Vertice m = hashMap.get(new Point2D(startX, startY));
        if(m == null){
            System.out.println("Null: " + startX + " " + startY);
            solution = findPathError(startX, startY);
        }
        else{
            solution = findPath(map, m);
        }
    }
    
    public AStar(HashMap<Point2D, Vertice> hashMap, int[][] map, double startX, double startY, double[][] weightList, boolean searchForHidingSpot, double[] genes){
        this.hashMap = hashMap;
        this.map = map;
        if(weightList != null){
            this.weightList = weightList;
        }
        else{
            createWeightList();
        }
        this.searchForHidingSpot = searchForHidingSpot;
        this.genes = genes;
        startX = Math.round(startX*10.0)/10.0;
        startY = Math.round(startY*10.0)/10.0;
        startX = (startX*10)%2 == 0 ? Math.round((startX + 0.1)*10.0)/10.0: startX;
        startY = (startY*10)%2 == 0 ? Math.round((startY + 0.1)*10.0)/10.0: startY;
        Vertice m = hashMap.get(new Point2D(startX, startY));
        if(m == null){
            System.out.println("Null: " + startX + " " + startY);
            solution = findPathError(startX, startY);
        }
        else{
            solution = findPath(map, m);
        }
    }
    
    public AStar(HashMap<Point2D, Vertice> hashMap, int[][] map, double startX, double startY, double goalX, double goalY, double[][] weightList, boolean searchForHidingSpot, double[] genes){
        this.map = map;
        this.hashMap = hashMap;
        goalX = Math.round((goalX + 0.5)*10.0)/10.0;
        goalY = Math.round((goalY + 0.5)*10.0)/10.0;
        this.goalX = (goalX*10)%2 == 0 ? Math.round((goalX + 0.1)*10.0)/10.0: goalX;
        this.goalY = (goalY*10)%2 == 0 ? Math.round((goalY + 0.1)*10.0)/10.0: goalY;
        goalCoordinates = true;
        if(weightList != null){
            this.weightList = weightList;
        }
        else{
            createWeightList();
        }
        this.searchForHidingSpot = searchForHidingSpot;
        this.genes = genes;
        startX = Math.round(startX*10.0)/10.0;
        startY = Math.round(startY*10.0)/10.0;
        startX = (startX*10)%2 == 0 ? Math.round((startX + 0.1)*10.0)/10.0: startX;
        startY = (startY*10)%2 == 0 ? Math.round((startY + 0.1)*10.0)/10.0: startY;
        Vertice m = hashMap.get(new Point2D(startX, startY));
        if(m == null){
            System.out.println("NUll: " + startX + " " + startY);
            solution = findPathError(startX, startY);
        }
        else{
            solution = findPath(map, m);
        }
    }
    
    public AStar(int[][] map, int startX, int startY){
        this.map = map;
        createWeightList();
        solution = findPath(startX, startY);
    }
    
    public AStar(int[][] map, int startX, int startY, int goalX, int goalY){
        this.map = map;
        createWeightList();
        solution = findPath(startX, startY, goalX, goalY);
    }
    
    public AStar(int[][] map, int startX, int startY, double[][] weightList, boolean searchForHidingSpot, double[] genes){
        this.map = map;
        this.weightList = weightList;
        this.searchForHidingSpot = searchForHidingSpot;
        this.genes = genes;
        solution = findPath(startX, startY);
    }
    
    public AStar(int[][] map, int startX, int startY, int goalX, int goalY, double[][] weightList, boolean searchForHidingSpot, double[] genes){
        this.map = map;
        this.weightList = weightList;
        this.searchForHidingSpot = searchForHidingSpot;
        this.genes = genes;
        solution = findPath(startX, startY, goalX, goalY);
    }
    
    private ArrayList<Point2D> findPathError(double x, double y){
        double centerX = (Math.round(x - 0.5) + 0.5);
        double centerY = (Math.round(y - 0.5) + 0.5);
        ArrayList<Point2D> solution = new ArrayList<Point2D>();
        solution.add(new Point2D(x, y));
        solution.add(new Point2D(centerX, centerY));
        return solution;
    }
    
    /**
     * Actual Astar algorithm. Weights between two points are previously stored in an adjacency matrix.
     * @param x starting x
     * @param y starting y
     * @return ArraList of Point2D containing each corner coordinate.
     */
    private ArrayList<Point2D> findPath(int x, int y){
        Path open = new Path();
        ArrayList<double[]> closed = new ArrayList();
        //initialization
        open.add(x, y, 0, x, y);
        double[] node_Current = new double[5];
        while(!open.nodes.isEmpty()){
            node_Current = open.nodes.get(0);
            x = (int)node_Current[0];
            y = (int)node_Current[1];
            //goal
            //target condition goal = 
            if(!searchForHidingSpot && map[x][y] == 21){
                break;
            }
            else if(searchForHidingSpot && (map[x][y]  == 2 || map[x][y] == 12 || map[x][y] == 11))
            {
                break;
            }
            Path newOpen = new Path();
            if(y + 1 < map[0].length){
                int index = map[x][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x, y + 1, weightList[x][y + 1]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x, y + 1, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x, y + 1, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x, y + 1, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            if(x + 1 < map.length){
                int index = map[x + 1][y];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x + 1, y, weightList[x + 1][y]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x + 1, y, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x + 1, y, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x + 1, y, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y + 1 < map.length && x + 1 < map.length){
                int index = map[x + 1][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x + 1, y + 1, weightList[x + 1][y + 1]);
                }
            }
            if(y - 1 >= 0 && x + 1 < map.length){
                int index = map[x + 1][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x + 1, y - 1, weightList[x + 1][y - 1]);
                }
            }*/
            if(y - 1 >= 0){
                int index = map[x][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x, y - 1, weightList[x][y - 1]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x, y - 1, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x, y - 1, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x, y - 1, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y - 1 >= 0 && x - 1 >= 0){
                int index = map[x - 1][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x - 1, y - 1, weightList[x - 1][y - 1]);
                }
            }*/
            if(x - 1 >= 0){
                int index = map[x - 1][y];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x - 1, y, weightList[x - 1][y]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x - 1, y, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x - 1, y, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x - 1, y, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y + 1 < map.length && x - 1 >= 0){
                int index = map[x - 1][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x - 1, y + 1, weightList[x - 1][y + 1]);
                }
            }*/
            for(double[] node_Successor: newOpen.nodes){
                node_Successor[2]+= node_Current[2];
                boolean inOpen = false;
                for(double[] node_Open: open.nodes){
                    if(node_Successor[0] == node_Open[0] && node_Successor[1] == node_Open[1]){
                        inOpen = true;
                        if(node_Open[2] <= node_Successor[2]){
                            break;
                        }
                        else{
                            node_Open = new double[]{node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]};
                            break;
                        }
                    }
                }
                boolean inClosed = false;
                if(!inOpen){
                    for(double[] node_Closed: closed){
                        if(node_Successor[0] == node_Closed[0] && node_Successor[1] == node_Closed[1]){
                            inClosed = true;
                            if(node_Closed[2] <= node_Successor[2]){
                                break;
                            }
                            else{
                                closed.remove(node_Closed);
                                node_Closed = new double[]{node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]};
                                open.add(node_Closed[0], node_Closed[1], node_Closed[2], node_Closed[3], node_Closed[4]);
                                break;
                            }
                        }
                    }
                }
                if(!(inClosed || inOpen)){
                    open.add(node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]);
                }
            }
            open.nodes.remove(node_Current);
            closed.add(node_Current);
        }
        ArrayList<Point2D> points = new ArrayList<>();
        if(map[(int)node_Current[0]][(int)node_Current[1]] != 21){
            //System.out.println("No path found");
        }
        else{
            //System.out.println("Path found");
            while(node_Current[0] != node_Current[3] || node_Current[1] != node_Current[4]){
                points.add(0, new Point2D(node_Current[0], node_Current[1]));
                for(double[] node: closed){
                    if(node_Current[3] == node[0] && node_Current[4] == node[1]){
                        node_Current = node;
                        closed.remove(node);
                        break;
                    }
                }
            }
            points.add(0, new Point2D(node_Current[0], node_Current[1]));
        }
        /*for(Point2D p: points){
            System.out.println(p.getX() + " " + p.getY());
        }*/
        return points;
    }
    
    private ArrayList<Point2D> findPath(int[][] map, Vertice m){
        PathMapPieces open = new PathMapPieces();
        ArrayList<Storage> closed = new ArrayList();
        //initialization
        open.add(m, 0, m);
        Storage node_Current = null;
        while(!open.nodes.isEmpty()){
            node_Current = open.nodes.get(0);
            m = node_Current.m;
            //goal
            //target condition goal = 
            if(goalCoordinates && m.coordinates.getX() == goalX && m.coordinates.getY() == goalY){
                break;
            }
            else if(!goalCoordinates  && !searchForHidingSpot && map[m.origin.x][m.origin.y] == 21 && m.isTrueTarget){
                break;
            }
            else if(!goalCoordinates && searchForHidingSpot && (map[m.origin.x][m.origin.y]  == 2 || map[m.origin.x][m.origin.y] == 12 || map[m.origin.x][m.origin.y] == 11))
            {
                break;
            }
            PathMapPieces newOpen = new PathMapPieces();
            for(Vertice mm: m.links){
                int index = map[mm.origin.x][mm.origin.y];
                if(index != -1 &&(searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(mm, weightList[mm.origin.x][mm.origin.y]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(m, genes[9]);
                            break;
                        case 11:
                            newOpen.add(m, genes[7]);
                            break;
                        case 12:
                            newOpen.add(m, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            for(Storage node_Successor: newOpen.nodes){
                node_Successor.weight+= node_Current.weight;
                boolean inOpen = false;
                for(Storage node_Open: open.nodes){
                    if(node_Successor.m.coordinates.getX() == node_Open.m.coordinates.getX() && node_Successor.m.coordinates.getY() == node_Open.m.coordinates.getY()){
                        inOpen = true;
                        if(node_Open.weight <= node_Successor.weight){
                            break;
                        }
                        else{
                            node_Open = new Storage(node_Successor.m, node_Successor.weight, node_Current.m);
                            break;
                        }
                    }
                }
                boolean inClosed = false;
                if(!inOpen){
                    for(Storage node_Closed: closed){
                        if(node_Successor.m.coordinates.getX() == node_Closed.m.coordinates.getX() && node_Successor.m.coordinates.getY() == node_Closed.m.coordinates.getY()){
                            inClosed = true;
                            if(node_Closed.weight <= node_Successor.weight){
                                break;
                            }
                            else{
                                closed.remove(node_Closed);
                                node_Closed = new Storage(node_Successor.m, node_Successor.weight, node_Current.m);
                                open.add(node_Closed.m, node_Closed.weight, node_Closed.parent);
                                break;
                            }
                        }
                    }
                }
                if(!(inClosed || inOpen)){
                    open.add(node_Successor.m, node_Successor.weight, node_Current.m);
                }
            }
            open.nodes.remove(node_Current);
            closed.add(node_Current);
        }
        ArrayList<Point2D> points = new ArrayList<>();
        if(goalCoordinates && (m.coordinates.getX() != goalX || m.coordinates.getY() != goalY)){
            System.out.println("Path not found");
            if(generateSolutionOnFault){
                while(node_Current.m.coordinates.getX() != node_Current.parent.coordinates.getX() || node_Current.m.coordinates.getY() != node_Current.parent.coordinates.getY()){
                    points.add(0, node_Current.m.coordinates);
                    for(Storage node: closed){
                        if(node_Current.parent.coordinates.getX() == node.m.coordinates.getX() && node_Current.parent.coordinates.getY() == node.m.coordinates.getY()){
                            node_Current = node;
                            closed.remove(node);
                            break;
                        }
                    }
                }
                points.add(0, node_Current.m.coordinates);
            }
            
            System.out.println();
        }
        else if(!goalCoordinates && map[m.origin.x][m.origin.y] != 21){
            System.out.println("Path not found");
            if(generateSolutionOnFault){
                while(node_Current.m.coordinates.getX() != node_Current.parent.coordinates.getX() || node_Current.m.coordinates.getY() != node_Current.parent.coordinates.getY()){
                    points.add(0, node_Current.m.coordinates);
                    for(Storage node: closed){
                        if(node_Current.parent.coordinates.getX() == node.m.coordinates.getX() && node_Current.parent.coordinates.getY() == node.m.coordinates.getY()){
                            node_Current = node;
                            closed.remove(node);
                            break;
                        }
                    }
                }
                points.add(0, node_Current.m.coordinates);
            }
            System.out.println();
        }
        else{
            //System.out.println("Path found");
            while(node_Current.m.coordinates.getX() != node_Current.parent.coordinates.getX() || node_Current.m.coordinates.getY() != node_Current.parent.coordinates.getY()){
                points.add(0, node_Current.m.coordinates);
                for(Storage node: closed){
                    if(node_Current.parent.coordinates.getX() == node.m.coordinates.getX() && node_Current.parent.coordinates.getY() == node.m.coordinates.getY()){
                        node_Current = node;
                        closed.remove(node);
                        break;
                    }
                }
            }
            points.add(0, node_Current.m.coordinates);
        }
        /*for(Point2D p: points){
            System.out.println(p.getX() + " " + p.getY());
        }*/
        return points;
    }
    
    private ArrayList<Point2D> findPath(int x, int y, int goalX, int goalY){
        Path open = new Path();
        ArrayList<double[]> closed = new ArrayList();
        //initialization
        open.add(x, y, 0, x, y);
        double[] node_Current = new double[5];
        while(!open.nodes.isEmpty()){
            node_Current = open.nodes.get(0);
            x = (int)node_Current[0];
            y = (int)node_Current[1];
            //goal
            //target condition goal = 
            if(!searchForHidingSpot && x == goalX && y == goalY){
                break;
            }
            else if(searchForHidingSpot && (map[x][y]  == 2 || map[x][y] == 12 || map[x][y] == 11))
            {
                break;
            }
            Path newOpen = new Path();
            if(y + 1 < map[0].length){
                int index = map[x][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x, y + 1, weightList[x][y + 1]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x, y + 1, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x, y + 1, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x, y + 1, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            if(x + 1 < map.length){
                int index = map[x + 1][y];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x + 1, y, weightList[x + 1][y]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x + 1, y, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x + 1, y, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x + 1, y, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y + 1 < map.length && x + 1 < map.length){
                int index = map[x + 1][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x + 1, y + 1, weightList[x + 1][y + 1]);
                }
            }
            if(y - 1 >= 0 && x + 1 < map.length){
                int index = map[x + 1][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x + 1, y - 1, weightList[x + 1][y - 1]);
                }
            }*/
            if(y - 1 >= 0){
                int index = map[x][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x, y - 1, weightList[x][y - 1]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x, y - 1, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x, y - 1, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x, y - 1, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y - 1 >= 0 && x - 1 >= 0){
                int index = map[x - 1][y - 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x - 1, y - 1, weightList[x - 1][y - 1]);
                }
            }*/
            if(x - 1 >= 0){
                int index = map[x - 1][y];
                if(index != 10 && index != 11 && index != 12 && index != 13 && index != -1 && (searchForHidingSpot ? index != 2 : true)){
                    newOpen.add(x - 1, y, weightList[x - 1][y]);
                }
                else if(searchForHidingSpot){
                    switch (index) {
                        case 2:
                            newOpen.add(x - 1, y, genes[9]);
                            break;
                        case 11:
                            newOpen.add(x - 1, y, genes[7]);
                            break;
                        case 12:
                            newOpen.add(x - 1, y, genes[8]);
                            break;
                        default:
                            break;
                    }
                }
            }
            /*if(y + 1 < map.length && x - 1 >= 0){
                int index = map[x - 1][y + 1];
                if(index != 10 && index != 11 && index != 12 && index != 13){
                    newOpen.add(x - 1, y + 1, weightList[x - 1][y + 1]);
                }
            }*/
            for(double[] node_Successor: newOpen.nodes){
                node_Successor[2]+= node_Current[2];
                boolean inOpen = false;
                for(double[] node_Open: open.nodes){
                    if(node_Successor[0] == node_Open[0] && node_Successor[1] == node_Open[1]){
                        inOpen = true;
                        if(node_Open[2] <= node_Successor[2]){
                            break;
                        }
                        else{
                            node_Open = new double[]{node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]};
                            break;
                        }
                    }
                }
                boolean inClosed = false;
                if(!inOpen){
                    for(double[] node_Closed: closed){
                        if(node_Successor[0] == node_Closed[0] && node_Successor[1] == node_Closed[1]){
                            inClosed = true;
                            if(node_Closed[2] <= node_Successor[2]){
                                break;
                            }
                            else{
                                closed.remove(node_Closed);
                                node_Closed = new double[]{node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]};
                                open.add(node_Closed[0], node_Closed[1], node_Closed[2], node_Closed[3], node_Closed[4]);
                                break;
                            }
                        }
                    }
                }
                if(!(inClosed || inOpen)){
                    open.add(node_Successor[0], node_Successor[1], node_Successor[2], node_Current[0], node_Current[1]);
                }
            }
            open.nodes.remove(node_Current);
            closed.add(node_Current);
        }
        ArrayList<Point2D> points = new ArrayList<>();
        if((int)node_Current[0] != goalX || (int)node_Current[1] != goalY){
            System.out.println("Path not found");
        }
        else{
            //System.out.println("Path found");
            while(node_Current[0] != node_Current[3] || node_Current[1] != node_Current[4]){
                points.add(0, new Point2D(node_Current[0], node_Current[1]));
                for(double[] node: closed){
                    if(node_Current[3] == node[0] && node_Current[4] == node[1]){
                        node_Current = node;
                        closed.remove(node);
                        break;
                    }
                }
            }
            points.add(0, new Point2D(node_Current[0], node_Current[1]));
        }
        /*for(Point2D p: points){
            System.out.println(p.getX() + " " + p.getY());
        }*/
        return points;
    }
    
    private void createWeightList(){
        double[][] weightList = new double[map.length][map[0].length];
        for(int i = 0; i < weightList.length; i++)
        {
            for(int j = 0; j < weightList[0].length; j++)
            {
                weightList[i][j] = 1;
            }
        }
        this.weightList = weightList;
    }
   
    /**
     * Find angle between two points
     * @param p Start of vector
     * @param pp End of vector
     * @return double angle of vector
     */
    private double angle(Point2D p, Point2D pp){
        double y = pp.getY() - p.getY();
        double x = pp.getX() - p.getX();
        if(y == 0 && x == 0){
            return 0;
        }
        else if(y < 0 && x < 0){
            return Math.toDegrees(Math.atan(y/x)) - 180;
        }
        else if(y >= 0 && x < 0){
            return Math.toDegrees(Math.atan(y/x)) + 180;
        }
        else{
            return Math.toDegrees(Math.atan(y/x));
        }
    }
    /**
     * Class path, used to store open and closed checked coordinates from Astar algorithm. It has an add method, that would add a new path depending on weight.
     */
    private class Path{
        private ArrayList<double[]> nodes;
        private Path(){
            nodes = new ArrayList<>();
        }
        /**
         * Add a new node, depending on weight
         * @param x x position of node
         * @param y y position of node
         * @param weight weight of node
         */
        private void add(double x, double y, double weight){
            if(nodes.isEmpty()){
                nodes.add(new double[]{x, y, weight});
            }
            else if(nodes.get(nodes.size() - 1)[2] > weight ){
                for(int i = 0; i < nodes.size(); i++){
                    if(nodes.get(i)[2] > weight){
                        nodes.add(i, new double[]{x, y, weight});
                        break;
                    }
                }
            }
            else{
                nodes.add(new double[]{x, y, weight});
            }
        }
        /**
         * Add a new node, depending on weight
         * @param x x position of node
         * @param y y position of node
         * @param weight weight of node
         * @param parentX x position of parent
         * @param parentY y position of parent
         */
        private void add(double x, double y, double f, double parentX, double parentY){
            if(nodes.size() == 0){
                nodes.add(new double[]{x, y, f, parentX, parentY});
            }
            else if(nodes.get(nodes.size() - 1)[2] > f){
                for(int i = 0; i < nodes.size(); i++){
                    if(nodes.get(i)[2] > f){
                        nodes.add(i, new double[]{x, y, f, parentX, parentY});
                        break;
                    }
                }
            }
            else{
                nodes.add(new double[]{x, y, f, parentX, parentY});
            }
        }    
    }
    
    private class Storage{
        private Vertice m;
        private double weight;
        private Vertice parent;
        
        private Storage(Vertice m, double weight, Vertice parent){
            this.m = m;
            this.weight = weight;
            this.parent = parent;
        }
        
        private Storage(Vertice m, double weight){
            this.m = m;
            this.weight = weight;
        }
    }
    
    private class PathMapPieces{
        private ArrayList<Storage> nodes;
        private PathMapPieces(){
            nodes = new ArrayList<>();
        }
        /**
         * Add a new node, depending on weight
         * @param x x position of node
         * @param y y position of node
         * @param weight weight of node
         */
        private void add(Vertice m, double weight){
            if(nodes.isEmpty()){
                nodes.add(new Storage(m, weight));
            }
            else if(nodes.get(nodes.size() - 1).weight > weight ){
                for(int i = 0; i < nodes.size(); i++){
                    if(nodes.get(i).weight > weight){
                        nodes.add(i, new Storage(m, weight));
                        break;
                    }
                }
            }
            else{
                nodes.add(new Storage(m, weight));
            }
        }
        /**
         * Add a new node, depending on weight
         * @param x x position of node
         * @param y y position of node
         * @param weight weight of node
         * @param parentX x position of parent
         * @param parentY y position of parent
         */
        private void add(Vertice m, double f, Vertice parent){
            if(nodes.size() == 0){
                nodes.add(new Storage(m, f, parent));
            }
            else if(nodes.get(nodes.size() - 1).weight > f){
                for(int i = 0; i < nodes.size(); i++){
                    if(nodes.get(i).weight > f){
                        nodes.add(i, new Storage(m, f, parent));
                        break;
                    }
                }
            }
            else{
                nodes.add(new Storage(m, f, parent));
            }
        }    
    }
    /**
     * @return list of corner points- solution of a maze.
     */
    public ArrayList<Point2D> getSolution(){
        return solution;
    }
    
    public static void createPath(int[][] map){
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                int index = map[i][j];
                if(index == 21 || index == 0 || index == 1 || index == 2){
                    for(double I = 0; I < 1.0; I+=0.2){
                        for(double J = 0; J < 1.0; J+=0.2){
                            boolean flag = false;
                            double X = Math.round((i + I + 0.1)*10.0)/10.0;
                            double Y = Math.round((j + J + 0.1)*10.0)/10.0;
                            for (int m = Math.toIntExact(Math.round(X - (1.0/3.0) - 0.5)); m < Math.round(X + (1.0/3.0) + 1 - 0.5); m++) {
                                for (int n = Math.toIntExact(Math.round(Y - (1.0/3.0) - 0.5)); n < Math.round(Y + (1.0/3.0) + 1 - 0.5); n++) {
                                    if(m >= 0 && n >= 0 && m < map.length && n < map[0].length){
                                    if(map[m][n] == 10 || map[m][n] == 11 || map[m][n] == 12 || map[m][n] == 13 ){
                                        flag = true;
                                    }
                                    }
                                }
                            }
                            if(!flag){
                                pathMap.put(new Point2D(X, Y), new Vertice(new Point2D(X, Y), new Point(i, j)));
                            }
                            //pathMap.add(new Vertice(new Point2D(i + I + 0.1, j + J + 0.1), new Point(i, j)));
                        }
                    }
                }
            }
        }
        for(Point2D p : pathMap.keySet()) {
            Vertice m = pathMap.get(p);
            boolean isTrueTarget = true;
            for(double I = -0.2; I <= 0.2; I+=0.2){
                 for(double J = -0.2; J <= 0.2; J+=0.2){
                     if(Math.round((p.getX() + I)*10.0)/10.0 != p.getX() || Math.round((p.getY() + J)*10.0)/10.0 != p.getY()){
                         Vertice mm = pathMap.get(new Point2D(Math.round((p.getX() + I)*10.0)/10.0, Math.round((p.getY() + J)*10.0)/10.0));
                         if(mm != null){
                             if(map[mm.origin.x][mm.origin.y] != 21){
                                isTrueTarget = false;
                             }
                             m.links.add(mm);
                         }
                         else{
                             isTrueTarget = false;
                         }
                    }
                }
            }
            m.isTrueTarget = isTrueTarget;
        }
    }   
}
