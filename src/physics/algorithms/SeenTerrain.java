package physics.algorithms;

import agents.guards.Guard;
import agents.intruders.Intruder;
import core.Core;
import java.util.ArrayList;
import javafx.geometry.Point2D;

public class SeenTerrain {
    private int[][] seenMap;
    private int[][] map;
    private Core core;
    public ArrayList<Guard> seenGuards = new ArrayList<>();
    public ArrayList<Intruder> seenIntruders = new ArrayList<>();
    
    public int[][] getSeenMap(Core core, int startX, int startY, double visionAngle, double visionRange, double agentVisionLength){
        this.core = core;
        this.map = core.map;
        seenMap = new int[map.length][map[0].length];
        for(int i = 0; i < seenMap.length; i++){
            for(int j = 0; j < seenMap[0].length; j++){
                seenMap[i][j] = -1;
            }
        }
        checkCoordinates(map, new Point2D(startX, startY), new Point2D(startX, startY), visionAngle, visionRange, agentVisionLength);
        //getSeenAgents(new Point2D(startX, startY), visionAngle, visionRange, agentVisionLength);
        return seenMap;
    }
    
    private void getSeenAgents(Point2D origin, double visionAngle, double visionRange, double agentVisionLength){
        for(Guard g: core.guards){
            Point2D G = new Point2D(g.coordinates.getyPos(), g.coordinates.getxPos());
            if(!g.inDarkArea && !g.isHidden && origin.distance(G) <= agentVisionLength &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenGuards.add(g);
            }
            if(g.inDarkArea && !g.isHidden && origin.distance(G) <= agentVisionLength/2.0 &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenGuards.add(g);
            }
            if(g.inDarkArea && g.isHidden && origin.distance(G) <= 1.0 &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenGuards.add(g);
            }
        }
        for(Intruder i: core.intruders){
            Point2D G = new Point2D(i.coordinates.getyPos(), i.coordinates.getxPos());
            if(!i.inDarkArea && !i.isHidden && origin.distance(G) <= agentVisionLength &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenIntruders.add(i);
            }
            if(i.inDarkArea && !i.isHidden && origin.distance(G) <= agentVisionLength/2.0 &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenIntruders.add(i);
            }
            if(i.inDarkArea && i.isHidden && origin.distance(G) <= 1.0 &&
                    isInVisionAngle(origin, G, visionAngle, visionRange) &&
                    isInLineOfSight(map, origin, G, visionAngle, visionRange)){
                seenIntruders.add(i);
            }
        }
    }
    
    private void checkCoordinates(int[][] map, Point2D origin, Point2D coordinate, double visionAngle, double visionRange, double agentVisionLength){
        int x = Math.toIntExact(Math.round(coordinate.getX()));
        int y = Math.toIntExact(Math.round(coordinate.getY()));
        
        if(map[x][y] == 13 && origin.distance(coordinate) <= 18 &&
                isInLineOfSight(map, origin, coordinate, visionAngle, visionRange)){
                seenMap[x][y] = map[x][y];
        }
        else if((map[x][y] == 10 || map[x][y] == 11 || map[x][y] == 12) && origin.distance(coordinate) <= 10 &&
                isInLineOfSight(map, origin, coordinate, visionAngle, visionRange)){
                seenMap[x][y] = map[x][y];
        }
        else if((map[x][y] == 0 || map[x][y] == 1 || map[x][y] == 21) && origin.distance(coordinate) <= agentVisionLength &&
                isInLineOfSight(map, origin, coordinate, visionAngle, visionRange)){
                seenMap[x][y] = map[x][y];
        }
        else if((map[x][y] == 2) && origin.distance(coordinate) <= agentVisionLength/2.0 && 
                isInLineOfSight(map, origin, coordinate, visionAngle, visionRange)){
                seenMap[x][y] = map[x][y];
        }
        else{
            seenMap[x][y] = -2;
        }
        
        if(x + 1 < map.length && seenMap[x+1][y] == -1 && isInVisionAngle(origin, new Point2D(x+1, y), visionAngle, visionRange) && origin.distance(new Point2D(x+1, y)) <= 18){
            checkCoordinates(map, origin, new Point2D(x+1,y), visionAngle, visionRange, agentVisionLength);
        }
        if(y + 1 < map[0].length && seenMap[x][y+1] == -1 && isInVisionAngle(origin, new Point2D(x, y+1), visionAngle, visionRange) && origin.distance(new Point2D(x, y+1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x,y+1), visionAngle, visionRange, agentVisionLength);
        }
        if(y  - 1 >= 0 && seenMap[x][y-1] == -1 && isInVisionAngle(origin, new Point2D(x, y-1), visionAngle, visionRange) && origin.distance(new Point2D(x, y-1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x,y-1), visionAngle, visionRange, agentVisionLength);
        }
        if(x - 1 >= 0 && seenMap[x-1][y] == -1 && isInVisionAngle(origin, new Point2D(x-1, y), visionAngle, visionRange) && origin.distance(new Point2D(x-1, y)) <= 18){
            checkCoordinates(map, origin, new Point2D(x-1,y), visionAngle, visionRange, agentVisionLength);
        }
        if(x + 1 < map.length && y + 1 < map[0].length && seenMap[x+1][y+1] == -1 && isInVisionAngle(origin, new Point2D(x+1, y+1), visionAngle, visionRange) && origin.distance(new Point2D(x+1, y+1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x+1,y+1), visionAngle, visionRange, agentVisionLength);
        }
        if(y + 1 < map[0].length && x - 1 >= 0 && seenMap[x-1][y+1] == -1 && isInVisionAngle(origin, new Point2D(x-1, y+1), visionAngle, visionRange) && origin.distance(new Point2D(x-1, y+1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x-1,y+1), visionAngle, visionRange, agentVisionLength);
        }
        if(y  - 1 >= 0 && x + 1 < map.length && seenMap[x+1][y-1] == -1 && isInVisionAngle(origin, new Point2D(x+1, y-1), visionAngle, visionRange) && origin.distance(new Point2D(x+1, y-1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x+1,y-1), visionAngle, visionRange, agentVisionLength);
        }
        if(x - 1 >= 0 && y - 1 >= 0 && seenMap[x-1][y-1] == -1 && isInVisionAngle(origin, new Point2D(x-1, y-1), visionAngle, visionRange) && origin.distance(new Point2D(x-1, y-1)) <= 18){
            checkCoordinates(map, origin, new Point2D(x-1,y-1), visionAngle, visionRange, agentVisionLength);
        }
    }
    
    private boolean isInVisionAngle(Point2D origin, Point2D coordinate, double visionAngle, double visionRange){
        double angle = 180-Math.toDegrees(Math.atan2(origin.getX()-coordinate.getX(), origin.getY()-coordinate.getY()));
        if(visionAngle >= 0 && visionAngle + visionRange <= 360){
            if(angle >= visionAngle && angle <= (visionAngle + visionRange)){
                return true;
            }
        }
        else if(visionAngle >= 0 && visionAngle + visionRange > 360){
            if((angle >= visionAngle && angle <= 360)){
                return true;
            }
            else if((angle >= 0 && angle <= visionAngle + visionRange -360)){
                return true;
            }
        }
        else{
            if((angle >= 0 && angle <= visionAngle + visionRange)){
                return true;
            }
            else if((angle >= (360 + visionAngle) && angle <= 360)){
                return true;
            }
        }
        return false;
    }
    
    private boolean isInLineOfSight(int[][] map, Point2D origin, Point2D coordinate, double visionAngle, double visionRange){
        int x = Math.toIntExact(Math.round(origin.getX()));
        int y = Math.toIntExact(Math.round(origin.getY()));
        int X = Math.toIntExact(Math.round(coordinate.getX()));
        int Y = Math.toIntExact(Math.round(coordinate.getY()));
        int I, J;
        if(x <= X && y <= Y){
            I = 1; J = 1;
        }
        else if(x >= X && y >= Y){
            I = -1; J = -1;
        }
        else if(x >= X && y <= Y){
            I = -1; J = 1;
        }
        else{
            I = 1; J = -1;
        }
        for(int i = x; x <= X && I > 0? i <= X: i >= X; i+=I){
            for(int j = y; y <= Y && J > 0? j <= Y: j >= Y; j+=J){
                if(!(X == i && Y == j) && !(x == i && y == j)){
                    //System.out.println(X + " " + x + " " + I);
                    //System.out.println(i + " " + j);
                    double[] angles = getClosestAnglesRefractored(origin, i, j);
                    angles[2] = 180-Math.toDegrees(Math.atan2(origin.getX()- coordinate.getX(), origin.getY()- coordinate.getY()));
                    if((map[i][j] == 10 || map[i][j] == 11 || map[i][j] == 12 || map[i][j] == 13) && isInVisionAngle(origin, new Point2D(i, j), visionAngle, visionRange)&& 
                        isAngleInRange(angles[0], angles[1], angles[2])){
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    private double[] getClosestAnglesRefractored(Point2D origin, int i, int j){
        Point2D[] p = new Point2D[4];
        p[0] = new Point2D(i-core.graphicDeviation, j-core.graphicDeviation);
        p[1] = new Point2D(i + core.graphicDeviation, j-core.graphicDeviation);
        p[2] = new Point2D(i-core.graphicDeviation, j + core.graphicDeviation);
        p[3] = new Point2D(i + core.graphicDeviation, j + core.graphicDeviation);
        double[] angles = new double[4];
        angles[0] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[0].getX(), origin.getY()-p[0].getY()));
        angles[1] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[1].getX(), origin.getY()-p[1].getY()));
        angles[2] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[2].getX(), origin.getY()-p[2].getY()));
        angles[3] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[3].getX(), origin.getY()-p[3].getY()));
        double[] distance = new double[6];
        int count = 0;
        for(int m = 0; m < angles.length  - 1; m++){
            for(int n = m + 1; n < angles.length; n++){
                distance[count] = Math.abs(angles[m] - angles[n]);
                count++;
            }
        }
        int index = 0;
        for(int m = 1; m < distance.length; m++){
            if(distance[index] < distance[m]){
                index = m;
            }
        }
        double[] newAngles = new double[3];
        if(index < 3){
            newAngles[0] = angles[index];
            newAngles[1] = angles[index + 1];
        }
        else if(index < 5){
            newAngles[0] = angles[1];
            newAngles[1] = angles[index - 1];
        }
        else{
            newAngles[0] = angles[2];
            newAngles[1] = angles[3];
        }
        return newAngles;
    }
    
    
    private double[] getClosestAngles(Point2D origin, int i, int j){
        Point2D[] p = new Point2D[4];
        p[0] = new Point2D(i-core.graphicDeviation, j-core.graphicDeviation);
        p[1] = new Point2D(i + core.graphicDeviation, j-core.graphicDeviation);
        p[2] = new Point2D(i-core.graphicDeviation, j + core.graphicDeviation);
        p[3] = new Point2D(i + core.graphicDeviation, j + core.graphicDeviation);
        double[] distance = new double[4];
        distance[0] = origin.distance(p[0]);
        distance[1] = origin.distance(p[1]);
        distance[2] = origin.distance(p[2]);
        distance[3] = origin.distance(p[3]);
        int[] sorted = new int[4];
        sorted[1] = -1;
        sorted[2] = -1;
        sorted[0] = -1;
        sorted[3] = -1;
        int count = 0;
        for(int n = 0; n < 4; n++){
            for(int m = 0; m < 4; m++){
                if(m != n && distance[n] > distance[m]){
                    count++;
                }
            }
            if(sorted[count] == -1)
            {
                sorted[count] = n;
            }
            else{
                sorted[count+1] = n;
            }
            count = 0;
        }
        double[] angles = new double[3];
        angles[0] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[sorted[0]].getX(), origin.getY()-p[sorted[0]].getY()));
        angles[1] = 180-Math.toDegrees(Math.atan2(origin.getX()- p[sorted[1]].getX(), origin.getY()-p[sorted[1]].getY()));
        return angles;
    }
    
    private boolean isAngleInRange(double angle1, double angle2, double angle){
        if(angle1 < angle2){
            if(angle >= angle1 && angle < angle2){
                return true;
            }
        }
        else{
            if(angle >= angle2 && angle < angle1){
                return true;
            }
        }
        return false;
    }
}
