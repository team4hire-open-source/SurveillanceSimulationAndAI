package physics.algorithms;

import environment.Vertice;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.geometry.Point2D;
import physics.PhysicsAgents;
import physics.Point;

public class AssertEnviroment {
    public ArrayList<Point> windows = new ArrayList<>();
    public ArrayList<Point> doors = new ArrayList<>();
    public ArrayList<Point> goals = new ArrayList<>();
    public ArrayList<Point> darkAreas = new ArrayList<>();
    //public ArrayList<Point> guardTowers = new ArrayList<>();
    private int[][] map;
    private double[][] weightList;
    private double[] genes;
    private HashMap<Point2D, Vertice> hashMap;
    private int possibleMoves = 0;
    private double angle;
    public boolean blocked = false;
    public boolean IcanHide = false;
    public boolean stayAndObserve = false;
    public boolean waitToHide = false;
    public Point2D pointToExplore;
    public ArrayList<Point2D> path;
    public int pathIndex;
    public Point2D goal;
    
    public AssertEnviroment(HashMap<Point2D, Vertice> hashMap, double[] genes, double x, double y, ArrayList<Point2D> path, int pathIndex, double angle, int[][] map, double[][] weightList){
        this.hashMap = hashMap;
        this.map = map;
        this.weightList = weightList;
        this.genes = genes;
        this.path = path;
        this.pathIndex = pathIndex;
        this.angle = angle;
        //System.out.println(x + " " + y);
        assertEnviromentRefractored(x, y);
    }
    
    public AssertEnviroment(HashMap<Point2D, Vertice> hashMap, double[] genes, double x, double y, Point2D goal, double angle, int[][] map, double[][] weightList){
        this.hashMap = hashMap;
        this.map = map;
        this.weightList = weightList;
        this.genes = genes;
        this.goal = goal;
        this.angle = angle;
        //System.out.println(x + " " + y);
        assertEnviromentRefractored(x, y);
    }
    
    private void assertEnviromentRefractored(double x, double y){
        int[][] copyMap = new int[map.length][map[0].length];
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                copyMap[i][j] = map[i][j];
            }
        }
        assertEnviroment(copyMap, Math.toIntExact(Math.round(x - 0.5)), Math.toIntExact(Math.round(y - 0.5)));
        //If goal is found try to reach goal
        if(!goals.isEmpty()){
            goal = null;
            AStar a = new AStar(hashMap, map, x, y, weightList, false, genes);
            path = a.getSolution();
            if(path.size() < 2){
                stayAndObserve = true;
            }
            else{
                pathIndex = 1;
                pointToExplore = path.get(pathIndex);
            }
        }
        //No found goals and I am blocked
        else if(goals.isEmpty() && possibleMoves < genes[6]){
            goal = null;
            blocked = true;
            //I can hide
            if(!doors.isEmpty() || !windows.isEmpty() || !darkAreas.isEmpty()){
                IcanHide = true;
                AStar a = new AStar(hashMap, map, x, y, weightList, true, genes);
                path = a.getSolution();
                if(path.size() < 2){
                    //System.out.println(3);
                    stayAndObserve = true;
                }
                else{
                    //System.out.println(4);
                    pathIndex = 1;
                    pointToExplore = path.get(pathIndex);
                }
            }
            //I cant hide
            else{
                Point p  = exploreWorld(x, y);
                //System.out.println(p.x + " " + p.y);
                AStar a = new AStar(hashMap, map, x, y, p.x, p.y, weightList, false, genes);
                path = a.getSolution();
                if(path.size() < 2){
                    System.out.println(5);
                    stayAndObserve = true;
                }
                else{
                    System.out.println(6);
                    pathIndex = 1;
                    pointToExplore = path.get(pathIndex);
                }
            }
        }
        else{
            Point p = null;
            if(goal == null){
                System.out.println("goal is null");
                p  = exploreWorld(x, y);
            }
            else{
                int i = Math.toIntExact(Math.round(x - 0.5));
                int j = Math.toIntExact(Math.round(y - 0.5));
                int X = Math.toIntExact(Math.round(goal.getX() - 0.5));
                int Y = Math.toIntExact(Math.round(goal.getY() - 0.5));
                if(i == X && j == Y){
                    if(i - 1 >= 0 && map[i - 1][j] == -1){
                        stayAndObserve = true;
                    }
                    else if(i - 1 >= 0 && j - 1 >= 0 && map[i - 1][j - 1] == -1){
                        stayAndObserve = true;
                    }
                    else if(i - 1 >= 0 && j + 1 < map[0].length && map[i - 1][j + 1] == -1){
                        stayAndObserve = true;
                    }
                    else if(j - 1 >= 0 && map[i][j - 1] == -1){
                        stayAndObserve = true;
                    }
                    else if(j + 1 < map[0].length && map[i][j + 1] == -1){
                        stayAndObserve = true;
                    }
                    else if(i + 1 < map.length && j - 1 >= 0 && map[i + 1][j - 1] == -1){
                        stayAndObserve = true;
                    }
                    else if(i + 1 < map.length && map[i + 1][j] == -1){
                        stayAndObserve = true;
                    }
                    else if(i + 1 < map.length && j + 1 < map[0].length &&  map[i + 1][j + 1] == -1){
                        stayAndObserve = true;
                    }
                    else{
                        System.out.println("something");
                        goal = null;
                        p  = exploreWorld(x, y);
                    }
                }
                else{
                    System.out.println("yes");
                    System.out.println(goal.getX() + " " + goal.getY());
                    System.out.println(X + " " + Y);
                    p = new Point(X, Y);
                }    
            }
            //System.out.println(p.x + " " + p.y);
            AStar a = new AStar(hashMap, map, x, y, p.x, p.y, weightList, false, genes);
            path = a.getSolution();
            if(path.size() < 2){
                System.out.println(7);
                goal = null;
                stayAndObserve = true;
            }
            else{
                System.out.println(8);
                pathIndex = 1;
                pointToExplore = path.get(pathIndex);
                goal = path.get(path.size() - 1);
            }
        }
    }
    
    private void assertEnviroment(double x, double y){
        int[][] copyMap = new int[map.length][map[0].length];
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                copyMap[i][j] = map[i][j];
            }
        }
        assertEnviroment(copyMap, Math.toIntExact(Math.round(x)), Math.toIntExact(Math.round(y)));
        //If goal is found try to reach goal
        if(!goals.isEmpty()){
                int X = Math.toIntExact(Math.round(path.get(path.size() - 1).getX() - 0.5));
                int Y = Math.toIntExact(Math.round(path.get(path.size() - 1).getY() - 0.5));
            if(path != null && !path.isEmpty()){
                if(map[X][Y] == 21){
                    x = Math.round(x*10.0)/10.0;
                    y = Math.round(y*10.0)/10.0;
                    x = (x*10)%2 == 0 ? Math.round((x + 0.1)*10.0)/10.0: x;
                    y = (y*10)%2 == 0 ? Math.round((y + 0.1)*10.0)/10.0: y;
                    for(int i = pathIndex -1; i < path.size(); i++){
                        if(path.get(i).getX() == x && path.get(i).getY() == y){
                            if(i == path.size() - 1){
                                stayAndObserve = true;
                            }
                            else{
                                pathIndex = i+1;
                                pointToExplore = path.get(pathIndex);
                            }
                            break;
                        }
                    }
                }
                else{
                    AStar a = new AStar(hashMap, map, x, y, weightList, false, genes);
                    path = a.getSolution();
                    if(path.size() < 2){
                        //System.out.println(1);
                        stayAndObserve = true;
                    }
                    else{
                        //System.out.println(2);
                        pathIndex = 1;
                        pointToExplore = path.get(1);
                    }
                }
            }else{
                AStar a = new AStar(hashMap, map, x, y, weightList, false, genes);
                path = a.getSolution();
                if(path.size() < 2){
                    //System.out.println(1);
                    stayAndObserve = true;
                }
                else{
                    //System.out.println(2);
                    pathIndex = 1;
                    pointToExplore = path.get(1);
                }
            }
        }
        //No found goals and I am blocked
        else if(goals.isEmpty() && possibleMoves < genes[6]){
            blocked = true;
            if(path != null && !path.isEmpty()){
                int X = Math.toIntExact(Math.round(path.get(path.size() - 1).getX() - 0.5));
                int Y = Math.toIntExact(Math.round(path.get(path.size() - 1).getY() - 0.5));
                if(map[X][Y] == 11 || map[X][Y] == 12 || map[X][Y] == 2){
                    x = Math.round(x*10.0)/10.0;
                    y = Math.round(y*10.0)/10.0;
                    x = (x*10)%2 == 0 ? Math.round((x + 0.1)*10.0)/10.0: x;
                    y = (y*10)%2 == 0 ? Math.round((y + 0.1)*10.0)/10.0: y;
                    for(int i = pathIndex - 1; i < path.size(); i++){
                        if(path.get(i).getX() == x && path.get(i).getY() == y){
                            pathIndex = i+1;
                            pointToExplore = path.get(pathIndex);
                            if(i == path.size() - 1){
                                waitToHide = true;
                            }
                            break;
                        }
                    }
                }
                else{
                    x = Math.round(x*10.0)/10.0;
                    y = Math.round(y*10.0)/10.0;
                    x = (x*10)%2 == 0 ? Math.round((x + 0.1)*10.0)/10.0: x;
                    y = (y*10)%2 == 0 ? Math.round((y + 0.1)*10.0)/10.0: y;
                    for(int o = pathIndex - 1; o < path.size(); o++){
                        if(path.get(o).getX() == x && path.get(o).getY() == y){
                            if(o == path.size() - 1){
                                int i = Math.toIntExact(Math.round(path.get(o).getX() - 0.5));
                                int j = Math.toIntExact(Math.round(path.get(o).getY() - 0.5));
                                if(i - 1 >= 0 && map[i - 1][j] == -1){
                                    stayAndObserve = true;
                                }
                                else if(i - 1 >= 0 && j - 1 >= 0 && map[i - 1][j - 1] == -1){
                                    stayAndObserve = true;
                                }
                                else if(i - 1 >= 0 && j + 1 < map[0].length && map[i - 1][j + 1] == -1){
                                    stayAndObserve = true;
                                }
                                else if(j - 1 >= 0 && map[i][j - 1] == -1){
                                    stayAndObserve = true;
                                }
                                else if(j + 1 < map[0].length && map[i][j + 1] == -1){
                                    stayAndObserve = true;
                                }
                                else if(i + 1 < map.length && j - 1 >= 0 && map[i + 1][j - 1] == -1){
                                    stayAndObserve = true;
                                }
                                else if(i + 1 < map.length && map[i + 1][j] == -1){
                                    stayAndObserve = true;
                                }
                                else if(i + 1 < map.length && j + 1 < map[0].length &&  map[i + 1][j + 1] == -1){
                                    stayAndObserve = true;
                                }
                                else{ 
                                    //I can hide
                                    if(!doors.isEmpty() || !windows.isEmpty() || !darkAreas.isEmpty()){
                                        IcanHide = true;
                                        AStar a = new AStar(hashMap, map, x, y, weightList, true, genes);
                                        path = a.getSolution();
                                        if(path.size() < 2){
                                            //System.out.println(3);
                                            stayAndObserve = true;
                                        }
                                        else{
                                            //System.out.println(4);
                                            pathIndex = 1;
                                            pointToExplore = path.get(1);
                                        }
                                    }
                                    //I cant hide
                                    else{
                                        Point p  = exploreWorld(x, y);
                                        //System.out.println(p.x + " " + p.y);
                                        AStar a = new AStar(hashMap, map, x, y, p.x + 0.5, p.y + 0.5, weightList, false, genes);
                                        path = a.getSolution();
                                        if(path.size() < 2){
                                            //System.out.println(5);
                                            stayAndObserve = true;
                                        }
                                        else{
                                            //System.out.println(6);
                                            pathIndex = 1;
                                            pointToExplore = path.get(1);
                                        }
                                    }
                                }
                            }
                            else{
                                pathIndex = o+1;
                                pointToExplore = path.get(pathIndex);
                            }
                            break;
                        }
                    }
                }
            }
            else{
                //I can hide
                if(!doors.isEmpty() || !windows.isEmpty() || !darkAreas.isEmpty()){
                    IcanHide = true;
                    AStar a = new AStar(hashMap, map, x, y, weightList, true, genes);
                    path = a.getSolution();
                    if(path.size() < 2){
                        //System.out.println(3);
                        stayAndObserve = true;
                    }
                    else{
                        //System.out.println(4);
                        pathIndex = 1;
                        pointToExplore = path.get(1);
                    }
                }
                //I cant hide
                else{
                    Point p  = exploreWorld(x, y);
                    //System.out.println(p.x + " " + p.y);
                    AStar a = new AStar(hashMap, map, x, y, p.x + 0.5, p.y + 0.5, weightList, false, genes);
                    path = a.getSolution();
                    if(path.size() < 2){
                        System.out.println(5);
                        stayAndObserve = true;
                    }
                    else{
                        System.out.println(6);
                        pathIndex = 1;
                        pointToExplore = path.get(1);
                    }
                }
            }
        }
        else{
            if(path != null && !path.isEmpty()){
                x = Math.round(x*10.0)/10.0;
                y = Math.round(y*10.0)/10.0;
                x = (x*10)%2 == 0 ? Math.round((x + 0.1)*10.0)/10.0: x;
                y = (y*10)%2 == 0 ? Math.round((y + 0.1)*10.0)/10.0: y;
                for(int o = pathIndex - 1; o < path.size(); o++){
                    if(path.get(o).getX() == x && path.get(o).getY() == y){
                        if(o == path.size() - 1){
                            int i = Math.toIntExact(Math.round(path.get(o).getX() - 0.5));
                            int j = Math.toIntExact(Math.round(path.get(o).getY() - 0.5));
                            if(x - 1 >= 0 && map[i - 1][j] == -1){
                                stayAndObserve = true;
                            }
                            else if(i - 1 >= 0 && j - 1 >= 0 && map[i - 1][j - 1] == -1){
                                stayAndObserve = true;
                            }
                            else if(i - 1 >= 0 && j + 1 < map[0].length && map[i - 1][j + 1] == -1){
                                stayAndObserve = true;
                            }
                            else if(j - 1 >= 0 && map[i][j - 1] == -1){
                                stayAndObserve = true;
                            }
                            else if(j + 1 < map[0].length && map[i][j + 1] == -1){
                                stayAndObserve = true;
                            }
                            else if(i + 1 < map.length && j - 1 >= 0 && map[i + 1][j - 1] == -1){
                                stayAndObserve = true;
                            }
                            else if(i + 1 < map.length && map[i + 1][j] == -1){
                                stayAndObserve = true;
                            }
                            else if(i + 1 < map.length && j + 1 < map[0].length &&  map[i + 1][j + 1] == -1){
                                stayAndObserve = true;
                            }
                            else{ 
                                Point p  = exploreWorld(x, y);
                                //System.out.println(p.x + " " + p.y);
                                AStar a = new AStar(hashMap, map, x, y, p.x + 0.5, p.y + 0.5, weightList, false, genes);
                                path = a.getSolution();
                                if(path.size() < 2){
                                    //System.out.println(7);
                                    stayAndObserve = true;
                                }
                                else{
                                    //System.out.println(8);
                                    pathIndex = 1;
                                    pointToExplore = path.get(1);
                                }
                            }
                        }
                        else{
                            pathIndex = o+1;
                            pointToExplore = path.get(pathIndex);
                        }
                        break;
                    }
                }
            }
            else{
                Point p  = exploreWorld(x, y);
                //System.out.println(p.x + " " + p.y);
                AStar a = new AStar(hashMap, map, x, y, p.x + 0.5, p.y + 0.5, weightList, false, genes);
                path = a.getSolution();
                if(path.size() < 2){
                    //System.out.println(7);
                    stayAndObserve = true;
                }
                else{
                    //System.out.println(8);
                    pathIndex = 1;
                    pointToExplore = path.get(1);
                }
            }
        }
    }
    
    private Point exploreWorld(double x, double y){
        ArrayList<Point> pointsToExplore = new ArrayList<>();
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                int index = map[i][j];
                if(index == 21 || index == 0 || index == 1 || index == 2){
                    if(i - 1 >= 0 && map[i - 1][j] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(i - 1 >= 0 && j - 1 >= 0 && map[i - 1][j - 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(i - 1 >= 0 && j + 1 < map[0].length && map[i - 1][j + 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(j - 1 >= 0 && map[i][j - 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(j + 1 < map[0].length && map[i][j + 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(i + 1 < map.length && j - 1 >= 0 && map[i + 1][j - 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(i + 1 < map.length && map[i + 1][j] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                    else if(i + 1 < map.length && j + 1 < map[0].length &&  map[i + 1][j + 1] == -1){
                        pointsToExplore.add(new Point(i, j));
                    }
                }
            }
        }
        ArrayList<Point> noRotationNeeded = new ArrayList<Point>();
        ArrayList<Point> smallRotationNeeded = new ArrayList<Point>();
        for(Point p: pointsToExplore){
            double pointAngle = 180-Math.toDegrees(Math.atan2(x- p.x, y-p.y));
            double[] rotationMaximum = PhysicsAgents.getBlindMaxAngleRad();
            rotationMaximum[0] = Math.toDegrees(rotationMaximum[0]);
            rotationMaximum[1] = Math.toDegrees(rotationMaximum[1]);
            if(pointAngle >= angle - rotationMaximum[1] && pointAngle <= angle + rotationMaximum[1]){
                noRotationNeeded.add(p);
            }
            else if(pointAngle >= angle - rotationMaximum[0] && pointAngle <= angle + rotationMaximum[0]){
                smallRotationNeeded.add(p);
            }
        }
        if(!noRotationNeeded.isEmpty()){
            /*System.out.println("No");
            for(Point p: noRotationNeeded){
                System.out.println(p.x + p.y);
            }*/
            System.out.println(11);
            if(noRotationNeeded.size() == 1){
                return noRotationNeeded.get(0);
            }
            else{
                return noRotationNeeded.get(Math.toIntExact(Math.round(Math.random()*(noRotationNeeded.size() - 1))));
            }
        }
        else if(!smallRotationNeeded.isEmpty()){
            /*System.out.println("small");
            for(Point p: smallRotationNeeded){
                System.out.println(p.x + " " + p.y);
            }*/
            System.out.println(12);
            if(smallRotationNeeded.size() == 1){
                return smallRotationNeeded.get(0);
            }
            else{
                return smallRotationNeeded.get(Math.toIntExact(Math.round(Math.random()*(smallRotationNeeded.size()-1))));
            }
        }
        else if(!pointsToExplore.isEmpty()){
            /*System.out.println("Yes");
            for(Point p: pointsToExplore){
                System.out.println(p.x + " " + p.y);
            }*/
            System.out.println(13);
            if(pointsToExplore.size() == 1){
                return pointsToExplore.get(0);
            }
            else{
                return pointsToExplore.get(Math.toIntExact(Math.round(Math.random()*(pointsToExplore.size() - 1))));
            }
        }
        else{
            stayAndObserve = true;
            return null;
        }
    }
    
    private void assertEnviroment(int[][] copyMap, int x, int y){
        if(copyMap[x][y] == 10 || copyMap[x][y] == -1 || weightList[x][y] > genes[5]){
            return;
        }
        else if(copyMap[x][y] == 21){
            goals.add(new Point(x, y));
        }
        else if(copyMap[x][y] == 11){
            doors.add(new Point(x, y));
        }
        else if(copyMap[x][y] == 12){
            windows.add(new Point(x, y));
        }
        /*else if(copyMap[x][y] == 13){
            guardTowers.add(new Point(x, y));
        }*/
        else if(copyMap[x][y] == 2){
            darkAreas.add(new Point(x, y));
        }
        copyMap[x][y] = -1;
        possibleMoves++;
        
        int X = x - 1;
        int Y = y;
        if(X >= 0){
            assertEnviroment(copyMap, X, Y);
        }
        X = x - 1;
        Y = y - 1;
        if(X >= 0 && Y >= 0){
            assertEnviroment(copyMap, X, Y);
        }
        X = x - 1;
        Y = y + 1;
        if(X >= 0 && Y < map[0].length){
            assertEnviroment(copyMap, X, Y);
        }
        X = x;
        Y = y - 1;
        if(Y >= 0){
            assertEnviroment(copyMap, X, Y);
        }
        X = x;
        Y = y + 1;
        if(Y < map[0].length){
            assertEnviroment(copyMap, X, Y);
        }
        X = x + 1;
        Y = y - 1;
        if(X < map.length && Y >= 0){
            assertEnviroment(copyMap, X, Y);
        }
        X = x + 1;
        Y = y;
        if(X < map.length){
            assertEnviroment(copyMap, X, Y);
        }
        X = x + 1;
        Y = y + 1;
        if(X < map.length && Y < map[0].length){
            assertEnviroment(copyMap, X, Y);
        }
    }
}
