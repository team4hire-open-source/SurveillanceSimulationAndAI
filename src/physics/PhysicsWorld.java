package physics;

import core.Core;
import environment.Vertice;

import java.util.ArrayList;


public class PhysicsWorld {
  
    public static void main(String[] args){
        //runTests();
    }
    
    /*private static void runTests(){
        map = new int[][]{{10, 10, 10},
                         {10, 0, 10},
                         {10, 10, 10}};
        System.out.println(isAgentInWall(1, 1));
        System.out.println(isAgentInWall(1.3, 1.3));
        System.out.println(isAgentInWall(1.4, 1.4));
        System.out.println(isAgentInWall(1.333, 1.333));
    }*/

  /*public static ArrayList<Guard> checkCollisionGuards(ArrayList<Guard> guards , int[][] map ){

    for (Guard g: guards) {
      if(isAgentInWall(g , map)){
        g.setCoordinates(new Vector(Math.random()*20, Math.random()*20));
      }
    }
    return guards;
  }

  public static ArrayList<Intruder> checkCollisionIntruders(ArrayList<Intruder> intruders , int[][] map ){

    for (Intruder i: intruders) {
      if(isAgentInWall(i , map)){
        i.setCoordinates(new Vector(Math.random()*20, Math.random()*20));
      }
    }
    return intruders;
  }*/

  public static boolean checkCollision(Core core, double radius, double agentXpos , double agentYpos){ //true if the agent is in the wall
    if(isAgentInWall(core, radius, agentXpos, agentYpos)){
      return true;
    }
    return false;
  }
  
  public static boolean isAgentOnTarget(Core core, double agentXpos , double agentYpos){
    int i = Math.toIntExact(Math.round(agentXpos - core.graphicDeviation));
    int j = Math.toIntExact(Math.round(agentYpos - core.graphicDeviation));
    if(core.map[i][j] == 21)
        {
            return true;
        }
    return false;
  }

    public static boolean isAgentInDark(Core core, double agentXpos , double agentYpos){
        int i = Math.toIntExact(Math.round(agentXpos - core.graphicDeviation));
        int j = Math.toIntExact(Math.round(agentYpos - core.graphicDeviation));
        if(core.map[i][j] == 2)
        {
            return true;
        }
        return false;
    }
  
  public static boolean isAgentInTower(Core core, double agentXpos , double agentYpos){
    int i = Math.toIntExact(Math.round(agentXpos - core.graphicDeviation));
    int j = Math.toIntExact(Math.round(agentYpos - core.graphicDeviation));
    if(core.map[i][j] == 13)
        {
            return true;
        }
    return false;
  }

  public static boolean isAgentInWall(Core core, double radius, double agentXpos , double agentYpos){
    //double agentXpos = a.getCoordinates().getxPos();
    //double agentYpos = a.getCoordinates().getyPos();
    
    //check currentCoordinates
    for (int i = Math.toIntExact(Math.round(agentYpos - radius - core.graphicDeviation)); i < Math.round(agentYpos + radius + 1 - core.graphicDeviation); i++) {
      for (int j = Math.toIntExact(Math.round(agentXpos - radius - core.graphicDeviation)); j < Math.round(agentXpos + radius + 1 - core.graphicDeviation); j++) {
          //System.out.println(i + " " + j);
          if(core.map[i][j] == 10 || core.map[i][j] == 11 || core.map[i][j] == 12 || core.map[i][j] == 13 ){
            /*System.out.println("i am in wall = " +map[i][j] );
            System.out.println("my pos = " + " x = " + agentXpos + "  y = " + agentYpos);
            System.out.println("Wall pos = " + "i = " + i + " j = " + j);
            System.out.println();*/
            return true;
        }
      }
    }

    return false;
  }
  
  public static double getDistance(Vector point1, Vector point2){

    double point1X = point1.getxPos();
    double point1Y = point1.getyPos();
    double point2X = point2.getxPos();
    double point2Y = point2.getyPos();

    double resultXcoord = point2X - point1X;
    double resultYcoord = point2Y - point1Y;

    double distance = Math.sqrt((resultXcoord * resultXcoord) + (resultYcoord * resultYcoord));

    return distance;
  }


  public static Vector getDirectionVec(Vector point1, Vector point2){

    double resultXcoord = point2.getxPos() - point1.getxPos();
    double resultYcoord = point2.getyPos() - point1.getyPos();

    Vector directionVec = new Vector(resultXcoord, resultYcoord);

    return directionVec;
  }

  public static void printMap (int[][] map){
    for (int i = 0; i < map.length ; i++) {
      for (int j = 0; j < map.length; j++) {
        System.out.print(map[i][j]);
      }
      System.out.println();
    }
  }


  public static ArrayList<Vertice> fromGridToCoordinates(int[][] map){

    double squareSizeInMeters = 1.0;
    double graphicDeviation = squareSizeInMeters/2.0;

    ArrayList<Vertice> coordinatesMap = new ArrayList<>();

    for (int i = 0; i < map.length  ; i++) {
      for (int j = 0; j < map[0].length ; j++) {

       if(map[i][j] != 0 && map[i][j] != 1 && map[i][j] != -1 && map[i][j] != -2 ){

         coordinatesMap.add(new Vertice(new Vector(j + graphicDeviation, i + graphicDeviation), map[i][j]));
       }

      }
    }

    return coordinatesMap;
  }
}
