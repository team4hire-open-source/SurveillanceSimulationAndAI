package physics;

import agents.Agent;

import java.util.ArrayList;

public class Vision {

  private int[][] seenTerrain; // 0 == can't see
  private ArrayList<Agent> intruders = new ArrayList<>();
  private ArrayList<Agent> guards = new ArrayList<>();

  public void addIntruders(Agent m){
    this.intruders.add(m);
  }

  public void addGuards(Agent e){
    this.guards.add(e);
  }

  public int[][] getSeenTerrain() {
    return seenTerrain;
  }

  public void setSeenTerrain(int[][] seenTerrain) {
    this.seenTerrain = seenTerrain;
  }

  public ArrayList<Agent> getIntruders() {
    return intruders;
  }

  public void setIntruders(ArrayList<Agent> intruders) {
    this.intruders = intruders;
  }

  public ArrayList<Agent> getGuards() {
    return guards;
  }

  public void setGuards(ArrayList<Agent> guards) {
    this.guards = guards;
  }
}
