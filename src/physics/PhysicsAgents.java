package physics;

import agents.guards.Guard;
import agents.intruders.Intruder;
import core.Core;
import physics.algorithms.SeenTerrain;

import java.util.ArrayList;

public class PhysicsAgents {

  public static Vector move(Core core, double radius, Vector currentPos, double velocity, Vector direction) {

    double gameTick = Core.gameTick / 1000; //in seconds
    direction = setVectorDirection(setRadiansDirection(direction), direction);
    double distance = velocity * gameTick;

    //calculate the end position
    double endXpos = currentPos.getxPos() + (distance * direction.getxPos());
    double endYpos = currentPos.getyPos() + (distance * direction.getyPos());

    if (!PhysicsWorld.checkCollision(core, radius, endXpos, endYpos)) {
      Vector endPos = new Vector(endXpos, endYpos);
      core.noise.noiseWalk(velocity, currentPos);

      return endPos;
    }
    return currentPos;
  }

  public static double endUpAt(Core core, Vector currentPos, Vector endPoint, Vector direction){
    double baseSpeed = 1.4;
    double neededSpeed;
    double time = 50;

    double xLength = Math.abs(currentPos.getxPos()-endPoint.getxPos());
    double yLength = Math.abs(currentPos.getyPos()-endPoint.getyPos());
    double realLength = Math.sqrt(xLength*xLength + yLength*yLength);

    neededSpeed = realLength/(core.gameTick/1000);

    if(neededSpeed > baseSpeed){
      return baseSpeed;
    }
    else {
      return neededSpeed;
    }
  }

  public static Vector setVectorDirection(double radians, Vector direction) {
    direction.setxPos(Math.cos(radians));
    direction.setyPos(Math.sin(radians));
    return direction;
  }

  public static double setRadiansDirection(Vector direction) {
    return Math.atan2(direction.getyPos(), direction.getxPos());
  }


  //IN CASE THE AGENT IN THE TOWER, THE CONTROLLER CHANGES THE aPos VALUES TEO METERS FORWARD!!!!!
  public static Vision iSee(Core core, Vector aPos, Vector direction, double range, double visionangle) {

    Vision vision = new Vision();

    SeenTerrain seenTerrain = new SeenTerrain();
    int x = Math.toIntExact(Math.round(aPos.getyPos() - core.graphicDeviation));
    int y = Math.toIntExact(Math.round(aPos.getxPos() - core.graphicDeviation));
    double directionAngle = 180 - Math.toDegrees(Math.atan2(aPos.getyPos() - direction.getyPos(), aPos.getxPos() - direction.getxPos())) - (visionangle / 2);
    int[][] seenMap = seenTerrain.getSeenMap(core, x, y, directionAngle, visionangle, range);
    vision.setSeenTerrain(seenMap);

    for (Intruder i : core.intruders) {

      Vector iPos = i.getCoordinates();

      if (canSee(aPos, iPos, range, seenMap, i.isHidden)) {
        vision.addIntruders(i);
      }
    }

    for (Guard g : core.guards) {

      Vector gPos = g.getCoordinates();

      if (canSee(aPos, gPos, range, seenMap, g.isHidden)) {
        vision.addGuards(g);
      }
    }

    return vision;
  }



  public static boolean canSee(Vector aPos1, Vector aPos2, double range, int[][] map, boolean isHidden) {
    //canSee method checks if an agent can see another one
    double distance = getDistance(aPos1, aPos2);

    if (distance > range) {
      return false;
    } else {

      int x = Math.toIntExact(Math.round(aPos2.getyPos() - 0.5));
      int y = Math.toIntExact(Math.round(aPos2.getxPos() - 0.5));

      if (map[x][y] == 0 || map[x][y] == 1 || map[x][y] == 13) {
        return true;
      } else if (map[x][y] == 2) {
        if (isHidden && distance <= 1) {
          return true;
        } else if (!isHidden && distance < range / 2) {
          return true;
        }
      }
      return false;
    }
  }


  public static double anglebetweenToVectors(Vector vec1, Vector vec2) {

    double vec1X = vec1.getxPos();
    double vec1Y = vec1.getyPos();

    double vec2X = vec2.getxPos();
    double vec2Y = vec2.getyPos();

    double angleToAgent;
    double w1 = Math.sqrt((vec1X * vec1X) + (vec1Y * vec1Y));
    double v1 = Math.sqrt((vec2X * vec2X) + (vec2Y * vec2Y));
    double uv = (vec1X * vec1Y) + (vec2X * vec2Y);
    angleToAgent = uv / (w1 * v1);

    return angleToAgent;

  }

  //return an arraylist of positions of the noises that the agent hears
  public static ArrayList<Vector> iHear(Core core, Vector pos) {

    ArrayList<Vector> noisesIhear = new ArrayList<>();

    for (NoiseModel n : core.noises) {
      int volume = n.getVolume();
      Vector noiseSource = n.getSource();
      if (getDistance(pos, noiseSource) <= volume) {
        Vector temp = getDirectionVec(pos, noiseSource);
        double rad = setRadiansDirection(temp);
        double deg = (180/Math.PI)*rad;
        deg = NormalDist.NormalDist(deg, 10);
        rad = (Math.PI/180)*deg;
        temp = setVectorDirection(rad, temp);
                noisesIhear.add(temp);
      }
    }

    if (noisesIhear.size() >= 1) {
      return noisesIhear;
    }
    return noisesIhear;
  }

  public static double getDistance(Vector point1, Vector point2) {

    double point1X = point1.getxPos();
    double point1Y = point1.getyPos();
    double point2X = point2.getxPos();
    double point2Y = point2.getyPos();

    double resultXcoord = point2X - point1X;
    double resultYcoord = point2Y - point1Y;

    double distance = Math.sqrt((resultXcoord * resultXcoord) + (resultYcoord * resultYcoord));

    return distance;
  }


  public static Vector getDirectionVec(Vector point1, Vector point2) {

    double resultXcoord = point2.getxPos() - point1.getxPos();
    double resultYcoord = point2.getyPos() - point1.getyPos();

    Vector directionVec = new Vector(resultXcoord, resultYcoord);

    return directionVec;
  }

  public static double[] getBlindMaxAngleRad() {
    double maxDir = (Math.PI)*(Core.gameTick/1000);
    double blindDir = (Math.PI/4)*(Core.gameTick/1000);
    double[] angles = new double[]{maxDir,blindDir};
    return angles;
  }
}