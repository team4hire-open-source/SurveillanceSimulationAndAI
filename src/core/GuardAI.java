package core;

public enum GuardAI {
    Random,
    Greedy,
    Heuristics,
    ExploreCatch,
    Patrolling
}
