package core;

import UI.TerrainGenerator.TerrainBuilder;
import UI.menu.MainMenu;
import UI.menu.SimulationMenu;
import UI.menu.TerrainSelectionMenu;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{
    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        loadMainMenu();
    }
    public static void main(String [] args){
        launch(args);
    }
    
    public static void loadMainMenu(){
        new MainMenu();
    }
    
    public static void loadSimulationMenu(int[][] map, boolean t){
        new SimulationMenu(map, t);
    }
    
    public static void loadTerrainSelectionMenu(){
        new TerrainSelectionMenu();
    }

    public static void loadTerrainDesignMenu(){
        new TerrainBuilder();
    }
}
