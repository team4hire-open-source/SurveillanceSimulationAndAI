package core;

import UI.data.Reader;
import agents.AgentController;
import agents.guards.HeuristicsGuardRefractored;
import agents.intruders.HeuristicsIntruderRefractored;
import static core.Core.millisecondsCounter;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import physics.Noise;

public class Simulation extends Core{
    
    private boolean simulationIsRunning;
    private int simulationsCountMax = 10;
    private int simulationsCount = 0;
    private int IntrudersWon = 0;
    private int GuardsWon = 0;
    private int Draw = 0;
    private double Mutation = 0.5;
    private ArrayList<HeuristicsIntruderRefractored> intruders = new ArrayList<>();
    private ArrayList<HeuristicsGuardRefractored> guards = new ArrayList<>();
    
    public static void main(String[] args){
        Simulation s = new Simulation();
        //Set the terrain, create manually using UI or go to Data/Terrains and choose one
        String mapName = "example";
        s.loadTerrain(mapName);
        if(s.map == null){
            System.out.println("Error while loading terrain!");
        }
        else{
            //Set guards, intruders and count
            s.startSimulation(defaultGuard, defaultIntruder, defaultGuardCount, defaultIntruderCount);
            System.out.println("Simulation has ended");
            System.out.println("Simulations runned: " + s.simulationsCountMax);
            System.out.println("Intruders have won: " + s.IntrudersWon);
            System.out.println("Guards have won: " + s.GuardsWon);
            System.out.println("Draw: " + s.Draw);
            s.observeAndMutate(s.IntrudersWon < s.GuardsWon);
        }
    }
    public Simulation(){
        super();
    }
    
    private void observeAndMutate(boolean guardsAreBetter){
        double[] genes;
        if(guardsAreBetter){
            genes = intruders.get(0).genes;
        }
        else{
            genes = intruders.get(0).genes;
        }
        double[] NewPopulation = new double[genes.length];
        for(int i = 0; i < NewPopulation.length; i++){
                    NewPopulation[i] = Math.round(NewPopulation[i] + Mutation*(Math.pow(-1,(Math.round(Math.random()))))*100.0)/100.0;
        }
        System.out.println("POPULATION:");
        for(int i = 0;  i < NewPopulation.length; i++)
        {
            System.out.print(NewPopulation[i] + " ");
        }
    }
    
    private void loadTerrain(String mapName){
        map = Reader.readTerrain(mapName);
        createPath();
    }
    
    @Override
     public void startSimulation(GuardAI Guard, IntruderAI Intruder, int guardCount, int intruderCount){
        simulationsCount++;
        this.Guard = Guard;
        this.Intruder = Intruder;
        this.guardCount = guardCount;
        this.intruderCount = intruderCount;
        createAgents();
        noise = new Noise(this, map);
        numberOfStartingIntruders = intruders.size();
        createSimulation();
    }
     
    public void clearWorld(){
        noises.clear();
        guards.clear();
        intruders.clear();
    }
     
    public void continueSimulation(){
        simulationIsRunning = false;
        clearWorld();
        if(simulationsCount + 1 <= simulationsCountMax){
            simulationsCount++;
            createAgents();
            noise = new Noise(this, map);
            numberOfStartingIntruders = intruders.size();
            createSimulation();
        }
    }
     
    @Override
    public void createAgents(){
        noises.clear();
        guards.clear();
        intruders.clear();
        for(int i = 0; i < guardCount; i++){
            guards.add(new HeuristicsGuardRefractored(this));
        }

        for(int i = 0; i < intruderCount; i++){
            intruders.add(new HeuristicsIntruderRefractored(this, true));
        }
    }
    
    @Override
    public void createSimulation(){
        simulationIsRunning = true;
        while(simulationIsRunning){
            millisecondsCounter+=gameTick;
            if(millisecondsCounter%1000 == 0)
            {
                secondsCounter++;
            }
            runWorld();
        }
    }
    
    private void runWorld(){
        noiseTick();
        // 0 - no one won this game tick
        // 1 - intruders won
        // 2 - draw
        // 3 - guards won
        int whoWon = AgentController.checkAllAgents(this);
        switch (whoWon) {
            case 1:
                System.out.println("INTRUDERS WON"); IntrudersWon++; continueSimulation();
                break;
            case 2:
                System.out.println("IT IS A DRAW"); Draw++; continueSimulation();
                break;
            case 3:
                System.out.println("GUARDS WON"); GuardsWon++; continueSimulation();
                break;
            default:
                break;
        }
        moveAgents();
    }
    
    private void noiseTick(){
        try {
            boolean done = false;
            while (!done){
                if ((millisecondsCounter - noises.element().getBirthday()) >= 100) {
                    noises.remove();
                }
                else {
                    done = true;
                }
            }
        }
        catch (NoSuchElementException e) {
            //System.out.println("No Noises");
        }
        noise.commitPoisson();
    }

    private void moveAgents(){
        for(int i = 0; i < guards.size(); i++){
            //1
            guards.get(i).move();
        }

        for(int i = 0; i < intruders.size(); i++){
            //2
            intruders.get(i).move();
        }
    }
}
