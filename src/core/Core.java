package core;

import UI.menu.SimulationMenu;
import UI.objects.UITerrain;
import agents.AgentController;
import agents.guards.GreedyGuard;
import agents.guards.Guard;
import agents.guards.ExploreCatch;
import agents.guards.HeuristicsGuardRefractored;
import agents.guards.Patrol.DriverPatrollingGuard;
import agents.guards.Patrol.PatrollingGuard;
import agents.intruders.GreedyIntruder;
import agents.intruders.HeuristicsIntruderRefractored;
import agents.intruders.Intruder;
import environment.Vertice;
import physics.NoiseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.util.Duration;
import physics.Noise;
import physics.Point;

/**
 * Control game tick and simulation
 * @author danyp
 */
public class Core {
    //AI types and default varibales
    public static final GuardAI defaultGuard = GuardAI.Heuristics;
    public static final int defaultGuardCount = 2;
    public static final IntruderAI defaultIntruder = IntruderAI.Heuristics;
    public static final int defaultIntruderCount = 2;
    
    //Enviroment variables
    public int[][] map; //2D representation of terrain
    public double squareSizeInMeters = 1.0; //square size in meters
    public double graphicDeviation = squareSizeInMeters/2.0; //get this from core or graphics
    public ArrayList<Vertice> coordinatesMap = new ArrayList<>(); //Vector representation of terrain
    public ArrayList<Guard> guards = new ArrayList<Guard>(); //Guards loaded in the world
    public ArrayList<Intruder> intruders = new ArrayList<Intruder>(); //Intruders loaded in the world
    public ArrayList<Point> intruderEntryPoints = new ArrayList<Point>();
    public Queue<NoiseModel> noises = new LinkedList<>(); //List of Noises
    public HashMap<Point2D, Vertice> pathMap = new HashMap<Point2D, Vertice>();
    public Noise noise;
    public int numberOfStartingIntruders; //number of intruderss
    public GuardAI Guard;
    public IntruderAI Intruder;
    public int guardCount;
    public int intruderCount;
    
    //Timeline variables
    public static long secondsCounter;
    public static long millisecondsCounter;
    public static Timeline timeline;
    public static double gameTick = 200; //milliseconds
    
    private boolean gameOver = false;
    
    private UITerrain terrain;
    private SimulationMenu menu;
    private boolean noMenu = false;

    private static DriverPatrollingGuard driver;
    private static boolean check = false;
    
    public Core(int[][] map, SimulationMenu menu){
        this.map = map;
        this.menu = menu;
        noMenu = false;
        createPath();
    }
    
    public Core(int[][] map){
        this.map = map;
        noMenu = true;
        createPath();
    }
    
    public Core(){
        noMenu = true;
    }
    
    public void startSimulation(GuardAI Guard, IntruderAI Intruder, int guardCount, int intruderCount){
        if(timeline == null || this.Guard != Guard || this.Intruder != Intruder || this.guardCount != guardCount || this.intruderCount != intruderCount){
            this.Guard = Guard;
            this.Intruder = Intruder;
            this.guardCount = guardCount;
            this.intruderCount = intruderCount;
            restartSimulation();
        }
    }
    
    public void restartSimulation(){
        if(timeline != null)
        {
            timeline.stop();
        }
        createAgents();
        numberOfStartingIntruders = intruders.size();
        if(!noMenu){
            menu.updateLegend(guards.size(), numberOfStartingIntruders, intruders.size());
            menu.setAI(Guard, Intruder, guards.size(), intruders.size());
        }
        createSimulation();
        if(!noMenu){
            menu.updateTimelineStatus(timeline.getStatus());
        }
    }
    
    public void createSimulation() {
        noise = new Noise(this, map);
        secondsCounter = 0;
        millisecondsCounter = 0;
        //create timeline
        timeline = new Timeline();
        //make it without ending
        timeline.setCycleCount(Animation.INDEFINITE);
        //setDuration to 0.2s
        Duration duration = Duration.millis(gameTick);
        //on each cycle call onFinishedMethod
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                millisecondsCounter+=gameTick;
                if(millisecondsCounter%1000 == 0)
                {
                    secondsCounter++;
                }
                runWorld();
            }
        };

        KeyFrame keyFrame = new KeyFrame(duration, onFinished);
        timeline.getKeyFrames().add(keyFrame);

        timeline.play();
    }
    
    private void runWorld(){
        noiseTick();
        // 0 - no one won this game tick
        // 1 - intruders won
        // 2 - draw
        // 3 - guards won
        int whoWon = AgentController.checkAllAgents(this);
        String s;
        switch (whoWon) {
            case 1:
                s = "Intruders won";
                someoneWon(s);
                break;
            case 2:
                s = "Draw";
                someoneWon(s);
                break;
            case 3:
                s = "Guards won"; someoneWon(s);
                break;
            default:
                break;
        }
        moveAgents();
    }

    private void someoneWon(String s){
        System.out.println(s);
        System.out.println(millisecondsCounter);
        timeline.stop();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                menu.simulationFinished(s);
            }
        });
    }
    
    private void noiseTick(){
        try {
            boolean done = false;
            while (!done){
                if ((millisecondsCounter - noises.element().getBirthday()) >= gameTick+1) {
                    noises.remove();
                }
                else {
                    done = true;
                }
            }
        }
        catch (NoSuchElementException e) {
            //System.out.println("No Noises");
        }
        noise.commitPoisson();
        terrain.drawNoise();
    }

    private void moveAgents(){
        if(check){
            driver.performMoves();
        }
        for(int i = 0; i < guards.size(); i++){
            //1
            guards.get(i).move();
        }

        for(int i = 0; i < intruders.size(); i++){
            //2
            intruders.get(i).move();
        }
    }
    
    public void clearWorld(){
        noises.clear();
        guards.clear();
        intruders.clear();
        if(timeline != null){
            timeline.stop();
            timeline = null;
        }
    }
    
    public void createAgents(){
        noises.clear();
        guards.clear();
        intruders.clear();
        if(Guard==GuardAI.Patrolling) { check = true; driver = new DriverPatrollingGuard(this);}
        for(int i = 0; i < guardCount; i++){
            switch(Guard){
                case Random: guards.add(new Guard(this)); break;
                case Greedy: guards.add(new GreedyGuard(this)); break;
                case Heuristics: guards.add(new HeuristicsGuardRefractored(this)); break;
                case ExploreCatch: guards.add(new ExploreCatch(this)); break;
                case Patrolling: guards.add(new PatrollingGuard(this)); break;
            }
        }

        for(int i = 0; i < intruderCount; i++){
            switch(Intruder){
                case Random: intruders.add(new Intruder(this, true)); break;
                case Greedy: intruders.add(new GreedyIntruder(this, true)); break;
                case Heuristics: intruders.add(new HeuristicsIntruderRefractored(this, true)); break;
            }
        }
        createPath();
        terrain.redrawAgents();
    }
    
    public void pauseSimulation(){
        if(timeline != null){
            if(Status.RUNNING == timeline.getStatus())
            {
                timeline.pause();
            }
            else{
                timeline.play();
            }
            if(!noMenu){
                menu.updateTimelineStatus(timeline.getStatus());
            }
        }
        else{
            
        }
    }
    
    public void createPath(){
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                int index = map[i][j];
                if(index == 21 || index == 0 || index == 1 || index == 2 || index == 20){
                    for(double I = 0; I < 1.0; I+=0.2){
                        for(double J = 0; J < 1.0; J+=0.2){
                            boolean flag = false;
                            double X = Math.round((i + I + 0.1)*10.0)/10.0;
                            double Y = Math.round((j + J + 0.1)*10.0)/10.0;
                            for (int m = Math.toIntExact(Math.round(X - (1.0/3.0) - 0.5)); m < Math.round(X + (1.0/3.0) + 1 - 0.5); m++) {
                                for (int n = Math.toIntExact(Math.round(Y - (1.0/3.0) - 0.5)); n < Math.round(Y + (1.0/3.0) + 1 - 0.5); n++) {
                                    if(m >= 0 && n >= 0 && m < map.length && n < map[0].length){
                                        if(map[m][n] == 10){
                                            flag = true;
                                        }
                                    }
                                }
                            }
                            if(!flag){
                                pathMap.put(new Point2D(X, Y), new Vertice(new Point2D(X, Y), new Point(i, j)));
                            }
                            //pathMap.add(new Vertice(new Point2D(i + I + 0.1, j + J + 0.1), new Point(i, j)));
                        }
                    }
                }
            }
        }
        for(Point2D p : pathMap.keySet()) {
            Vertice m = pathMap.get(p);
            boolean isTrueTarget = true;
            for(double I = -0.2; I <= 0.2; I+=0.2){
                 for(double J = -0.2; J <= 0.2; J+=0.2){
                     if(Math.round((p.getX() + I)*10.0)/10.0 != p.getX() || Math.round((p.getY() + J)*10.0)/10.0 != p.getY()){
                         Vertice mm = pathMap.get(new Point2D(Math.round((p.getX() + I)*10.0)/10.0, Math.round((p.getY() + J)*10.0)/10.0));
                         if(mm != null){
                             if(map[mm.origin.x][mm.origin.y] != 21){
                                isTrueTarget = false;
                             }
                             m.links.add(mm);
                         }
                         else{
                             isTrueTarget = false;
                         }
                    }
                }
            }
            m.isTrueTarget = isTrueTarget;
        }
    }
    
    public void displayEntryPoint(Point p){
        map[p.x][p.y] = 20;
        terrain.displayEntryPoint(p);
    }
    
    public UITerrain createTerrain(int PaneWidth, boolean t){
        terrain = new UITerrain(PaneWidth, PaneWidth, this, t);
        return terrain;
    }
}


