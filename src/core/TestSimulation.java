package core;

import agents.Agent;
import agents.guards.Guard;
import agents.intruders.Intruder;
import physics.PhysicsAgents;
import physics.Vector;
import physics.Vision;
import physics.algorithms.SeenTerrain;

import java.util.ArrayList;

public class TestSimulation extends Core{
    private int iSeeTestFails;
   
    public static void main(String[] args){
        new TestSimulation();
    }
    
    public TestSimulation(){
        super();
        iSeeTests();
    }
    
    private void runTests(){
         
    }
    
    private void iSeeTests(){
        System.out.println("--------------------------");
        System.out.println("Visual tests");
        System.out.println();
        System.out.println("Seeing constants test");
        Guard guardTest = new Guard(this, new Vector(6.5, 2.5));
        guards.add(guardTest);
        Intruder intruderTest = new Intruder(this, new Vector(7.5, 5.5));
        intruders.add(intruderTest);
        checkVision(guardTest, intruderTest);
    }
    
    private void checkVision(Guard g, Intruder intruder){
        iSeeTestMap();
        if(g.normalVisionRange != 6.0){
            iSeeTestFails++;
            System.out.println("Guard has wrong normal vision range");
        }
        if(intruder.normalVisionRange != 7.5){
            iSeeTestFails++;
            System.out.println("Intruder has wrong normal vision range");
        }

        Vector direction = new Vector(6.5, 5.5);
        g.setDirection(direction);
        g.setAngle(180-Math.toDegrees(Math.atan2(g.coordinates.getyPos()-direction.getyPos(), g.coordinates.getxPos()-direction.getxPos())));
        int x = Math.toIntExact(Math.round(g.coordinates.getyPos() - graphicDeviation));
        int y = Math.toIntExact(Math.round(g.coordinates.getxPos() - graphicDeviation));
        int[][] map = new SeenTerrain().getSeenMap(this, x, y, g.angle.getValue(), g.visionAngle, g.visionRange.doubleValue());
        /*for(int i = 0; i < map.length; i++){
            System.out.println("{{");
            for(int j = 0; j < map[0].length; j++){
                System.out.print(map[i][j] + ", ");
            }
            System.out.print("}");
        }*/

      System.out.println();
      System.out.println();

        Vision v = PhysicsAgents.iSee(this, g.coordinates, g.direction, g.visionRange.doubleValue(), g.visionAngle);

        int ix = Math.toIntExact(Math.round(intruder.coordinates.getyPos() - graphicDeviation));
        int iy = Math.toIntExact(Math.round(intruder.coordinates.getxPos() - graphicDeviation));

        int[][] map1 = v.getSeenTerrain();
        for(int i = 0; i < map1.length; i++){
          System.out.println("{{");
          for(int j = 0; j < map1[0].length; j++){
            if(i == ix && j == iy) {
              //System.out.print("intruder");
            }else{
              System.out.print(map1[i][j] + ", ");
            }
          }
          System.out.print("}");
        }

      System.out.println();
      System.out.println();

      ArrayList<Agent> intruders = v.getIntruders();
      System.out.println("size= " + intruders.size());
      for (Agent i: intruders) {
        System.out.println("my pos is: " + "x: " + i.getCoordinates().getxPos() + "  y: " + i.getCoordinates().getyPos());
      }
    }
    
    private void iSeeTestMap(){
        map = new int[][]{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                          {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                          {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                          {10, 10, 10, 0, 0, 0, 10, 0, 0, 0, 10, 10, 10},
                          {10, 10, 10, 10, 0, 0, 10, 0, 0, 10, 10, 10, 10},
                          {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}};
    }
    
    private void createExampleWorld(){
        map = new int[][]{{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                         {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                         {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10} ,
                         {10, 0, 0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 0, 10, 10, 0, 10},
                         {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 10},
                         {10, 0, 0, 10, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 10, 10, 0, 10, 0, 10},
                         {10, 0, 0, 10, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 10, 10, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 10, 0, 10},
                         {10, 0, 0, 0, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 0, 10},
                         {10, 0, 0, 0, 10, 10, 10, 10, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 10, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10, 0, 10},
                         {10, 0, 0, 10, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 10},
                         {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21, 10 },
                         {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}};
        createPath();
    }
}
